/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.api

import dev.maximilian.feather.civicrm.entities.CiviRulesRule
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import dev.maximilian.feather.civicrm.internal.civicrm.GeneralAPI
import dev.maximilian.feather.civicrm.internal.civicrm.IGetElement
import io.ktor.client.HttpClient
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface ICiviRulesRuleApi {
    public suspend fun getCiviRulesRules(): List<CiviRulesRule>

    public suspend fun getCiviRulesRule(id: Int): CiviRulesRule?

    public suspend fun createCiviRulesRule(
        name: String,
        label: String,
        triggerId: Int,
        description: String,
        helpText: String,
        trigger: String,
    ): CiviRulesRule

    public suspend fun deleteCiviRulesRule(id: Int)
}
internal class CiviRulesRuleApi(
    client: HttpClient,
    baseUrl: String,
    apiKey: String,
) : ICiviRulesRuleApi,
    IGetElement {

    private companion object : KLogging()
    private val generalApi = GeneralAPI(client, baseUrl, "CiviRulesRule", this)
    override suspend fun getCiviRulesRules(): List<CiviRulesRule> = generalApi.getBodyValues<CiviRulesRule>()

    override suspend fun getCiviRulesRule(id: Int): CiviRulesRule? = getCiviRulesRules().firstOrNull { it.id == id }

    override suspend fun createCiviRulesRule(
        name: String,
        label: String,
        triggerId: Int,
        description: String,
        helpText: String,
        trigger: String,
    ): CiviRulesRule {
        val s = mutableMapOf<String, JsonElement>()
        s["name"] = JsonPrimitive(name)
        s["label"] = JsonPrimitive(label)
        s["trigger_id"] = JsonPrimitive(triggerId)
        s["description"] = JsonPrimitive(description)
        s["help_text"] = JsonPrimitive(helpText)
        s["trigger"] = JsonPrimitive(trigger)
        logger.info { "body is params=${ParamStringBuilder.value(s)}" }

        // val response =
        generalApi.create(s) // .body<LegacyAPIAnswer<CiviRuleRule>>()
        val allResponses = getCiviRulesRules()
        val maxId = allResponses.maxOf { it.id }
        return getCiviRulesRules().find { it.id == maxId }!!

        // if (0 != response.is_error || null == response.values) {
        //    logger.warn { "CiviCRM::CiviRulesApi::createCiviRule failed with error: " + response.error_message }
        //    throw IllegalStateException("CiviCRM::CiviRulesApi::createCiviRule failed ${response.error_message}")
        // }
        // return response.values.single()
    }

    override suspend fun deleteCiviRulesRule(id: Int) {
        logger.info { "CiviCRM::CiviRuleRuleApi::deleteCiviRuleRule with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getCiviRulesRule(id)
}
