/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.CiviRulesRule
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import dev.maximilian.feather.civicrm.internal.retryWithExponentialBackoff
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.http.HttpStatusCode
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging
import java.net.http.HttpResponse
import java.time.Duration

@Serializable
internal data class LegacyAPIAnswer<ElementType>(
    val is_error: Int,
    val error_message: String? = null,
    val count: Int? = null,
    val values: List<ElementType>? = null,
)

public interface ICiviRulesApi {
    public suspend fun getCiviRules(): List<CiviRulesRule>

    public suspend fun getCiviRule(id: Int): CiviRulesRule?

    public suspend fun createCiviRule(
        name: String,
        label: String,
        triggerId: Int,
        description: String,
        helpText: String,
        trigger: String,
    ): CiviRulesRule

    public suspend fun deleteCiviRule(id: Int)
}
public class CiviRulesApi(
    private val client: HttpClient,
    private val baseUrl: String,
    private val apiKey: String,
) : ICiviRulesApi,
    IGetElement {
    private companion object : KLogging()

    // private val generalAPI = GeneralAPI(client, baseUrl, "CiviRuleAction", this)
    internal fun url(
        baseUrl: String,
        action: GeneralAPI.Action,
    ) = "$baseUrl/civicrm/ajax/rest?entity=CiviRuleRule&action=$action&json=%7B%22sequential%22%3A1%7D&api_key=$apiKey"

    public override suspend fun getCiviRules(): List<CiviRulesRule> = getBodyValues<CiviRulesRule>()

    public override suspend fun getCiviRule(id: Int): CiviRulesRule? = getSingleValue<CiviRulesRule>(id)

    private suspend fun get(): io.ktor.client.statement.HttpResponse =
        client.get(url(baseUrl, GeneralAPI.Action.GET) + "&limit=10000000", {
            /* needed so that this request is identified as web-service request and thus CSRF checks are not needed
             * see isWebServiceRequest in CRM/Utils/REST.php of CiviCRM repo */
            header("X-Requested-With", "XMLHttpRequest")
        })

    private suspend fun get(s: Set<Triple<String, String, Any>>): io.ktor.client.statement.HttpResponse =
        client.get(
            url(baseUrl, GeneralAPI.Action.GET) + "&params=${ParamStringBuilder.where(s, "")}",
            {
                header("X-Requested-With", "XMLHttpRequest")
            },
        )

    private suspend inline fun <reified T> getBodyValuesPriv(): GeneralAPIAnswer<T> {
        val response = get().body<LegacyAPIAnswer<T>>()
        if (0 != response.is_error || null == response.values) {
            logger.warn { "CiviCRM::CiviRulesApi::get failed with error: " + response.error_message }
            throw IllegalStateException("CiviCRM::CiviRulesApi::get failed")
        }
        return GeneralAPIAnswer(response.values)
    }

    private suspend inline fun <reified T> getSingleValuePriv(id: Int): GeneralAPIAnswer<T> {
        val response = get(setOf(Triple("id", "=", id))).body<LegacyAPIAnswer<T>>()
        if (0 != response.is_error || null == response.values) {
            logger.warn { "CiviCRM::CiviRulesApi::get failed with error: " + response.error_message }
            throw IllegalStateException("CiviCRM::CiviRulesApi::get failed")
        }
        return GeneralAPIAnswer(response.values)
    }

    internal suspend inline fun <reified T> getBodyValues(): List<T> = getBodyValuesPriv<T>().values

    internal suspend inline fun <reified T> getSingleValue(id: Int): T? = getSingleValuePriv<T>(id).values.singleOrNull()

    public override suspend fun createCiviRule(
        name: String,
        label: String,
        triggerId: Int,
        description: String,
        helpText: String,
        trigger: String,
    ): CiviRulesRule {
        val s = mutableMapOf<String, JsonElement>()
        s["name"] = JsonPrimitive(name)
        s["label"] = JsonPrimitive(label)
        s["trigger_id"] = JsonPrimitive(triggerId)
        s["description"] = JsonPrimitive(description)
        s["help_text"] = JsonPrimitive(helpText)
        s["trigger"] = JsonPrimitive(trigger)
        logger.info { "body is params=${ParamStringBuilder.value(s)}" }

        val response = createInternal(s).body<LegacyAPIAnswer<CiviRulesRule>>()
        if (0 != response.is_error || null == response.values) {
            logger.warn { "CiviCRM::CiviRulesApi::createCiviRule failed with error: " + response.error_message }
            throw IllegalStateException("CiviCRM::CiviRulesApi::createCiviRule failed")
        }
        return response.values.single()
    }

    private suspend fun createInternal(s: Map<String, JsonElement>): io.ktor.client.statement.HttpResponse {
        try {
            return client.post(url(baseUrl, GeneralAPI.Action.CREATE) + "&params=${ParamStringBuilder.value(s)}")
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.UnprocessableEntity) {
                throw IllegalArgumentException(e.response.body<GeneralAPI.DrupalErrorAnswer>().message)
            } else {
                throw e
            }
        }
    }
    public override suspend fun deleteCiviRule(id: Int) {
        logger.info { "CiviCRM::CiviRulesApi::deleteCiviRule with id $id" }
        deleteWithLoop(id)
    }

    private suspend fun deleteWithLoop(
        id: Int,
        suffix: String = "",
    ) {
        delete(id, suffix)

        val waitingFunction: suspend () -> Unit = {
            require(getElement(id) == null) { "Deletion exceeds time limit" }
        }
        waitingFunction.retryWithExponentialBackoff(6, Duration.ofSeconds(180))
    }

    internal suspend fun delete(
        id: Int,
        suffix: String = "",
    ) {
        client.post(
            url(baseUrl, GeneralAPI.Action.DELETE) +
                "&params=${ParamStringBuilder.where(setOf(Triple("id", "=", id)), suffix)}",
        )
    }

    override suspend fun getElement(id: Int): Any? = getCiviRule(id)
}
