package dev.maximilian.feather.civicrm.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class OptionValue(
    val id: Int,
    @SerialName("option_group_id")
    val optionGroupId: Int,
    val label: String?,
    val name: String?,
    val value: String?,
    // val grouping: String?,
    // @SerialName("is_default")
    // val isDefault: Boolean?,
    // @SerialName("is_reserved")
    // val isReserved: Boolean?,
    val weight: Int?,
    val description: String?,
    // @SerialName("is_optgroup")
    // val isOptGroup: Boolean?,
    @SerialName("is_active")
    val isActive: Boolean?,
    // @SerialName("is_locked")
    // val isLocked: Boolean?,
    // @SerialName("component_id")
    // val componentId: Int?,
    // @SerialName("domain_id")
    // val domainId: Int?,
    // @SerialName("visibility_id")
    // val visibilityId: Int?,
    // val icon: String?,
    // val color: String?,
)
