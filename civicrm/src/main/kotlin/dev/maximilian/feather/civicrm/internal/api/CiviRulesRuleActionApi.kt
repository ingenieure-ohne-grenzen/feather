/*
 *    Copyright [2024] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.api

import dev.maximilian.feather.civicrm.entities.CiviRulesRuleAction
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import dev.maximilian.feather.civicrm.internal.civicrm.GeneralAPI
import dev.maximilian.feather.civicrm.internal.civicrm.IGetElement
import io.ktor.client.HttpClient
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface ICiviRulesRuleActionApi {
    public suspend fun getCiviRulesRuleActions(): List<CiviRulesRuleAction>

    public suspend fun getCiviRulesRuleAction(id: Int): CiviRulesRuleAction?

    public suspend fun createCiviRulesRuleAction(
        ruleId: Int,
        actionId: Int,
        actionParameters: String,
    ): CiviRulesRuleAction
    public suspend fun buildActionParamStringToAddContactToGroup(
        groupId: Int,
    ): String
    public suspend fun deleteCiviRulesRuleAction(id: Int)
}
public class CiviRuleRuleActionApi(
    client: HttpClient,
    baseUrl: String,
    apiKey: String,
) : ICiviRulesRuleActionApi,
    IGetElement {
    private companion object : KLogging()
    private val generalApi = GeneralAPI(client, baseUrl, "CiviRulesRuleAction", this)

    public override suspend fun getCiviRulesRuleActions(): List<CiviRulesRuleAction> = generalApi.getBodyValues<CiviRulesRuleAction>()

    public override suspend fun getCiviRulesRuleAction(id: Int): CiviRulesRuleAction? = getCiviRulesRuleActions().firstOrNull { it.id == id }

    public override suspend fun buildActionParamStringToAddContactToGroup(
        groupId: Int,
    ): String {
        val len = groupId.toString().length
        return """a:2:{s:8:"group_id";s:$len:"$groupId";s:9:"group_ids";b:0;}"""
    }
    public override suspend fun createCiviRulesRuleAction(
        ruleId: Int,
        actionId: Int,
        actionParameters: String,
    ): CiviRulesRuleAction {
        val s = mutableMapOf<String, JsonElement>()
        s["rule_id"] = JsonPrimitive(ruleId)
        s["action_id"] = JsonPrimitive(actionId)
        s["action_params"] = JsonPrimitive(actionParameters)
        s["is_active"] = JsonPrimitive(1)
        logger.info { "body is params=${ParamStringBuilder.value(s)}" }

        // val response =
        generalApi.create(s) // .body<LegacyAPIAnswer<CiviRuleRule>>()
        val allResponses = getCiviRulesRuleActions()
        val maxId = allResponses.maxOf { it.id }
        return getCiviRulesRuleActions().find { it.id == maxId }!!

        // if (0 != response.is_error || null == response.values) {
        //    logger.warn { "CiviCRM::CiviRulesApi::createCiviRule failed with error: " + response.error_message }
        //    throw IllegalStateException("CiviCRM::CiviRulesApi::createCiviRule failed ${response.error_message}")
        // }
        // return response.values.single()
    }

    public override suspend fun deleteCiviRulesRuleAction(id: Int) {
        logger.info { "CiviCRM::CiviRuleRuleActionApi::deleteCiviRuleRuleAction with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getCiviRulesRuleAction(id)
}
