/*
 *    Copyright [2024] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.api
import dev.maximilian.feather.civicrm.entities.CiviRulesRuleCondition
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import dev.maximilian.feather.civicrm.internal.civicrm.GeneralAPI
import dev.maximilian.feather.civicrm.internal.civicrm.IGetElement
import io.ktor.client.HttpClient
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface ICiviRulesRuleConditionApi {
    public suspend fun getCiviRulesRuleConditions(): List<CiviRulesRuleCondition>

    public suspend fun getCiviRulesRuleCondition(id: Int): CiviRulesRuleCondition?

    public suspend fun createCiviRulesRuleCondition(
        ruleId: Int,
        conditionId: Int,
        conditionLink: String?,
        conditionParameters: String,
    ): CiviRulesRuleCondition
    public suspend fun buildIsInOneOfSelectedGroupCondition(groupId: Int, checkGroupTree: Boolean): String
    public suspend fun buildIsNotInOneOfSelectedGroupCondition(inGroup: List<Int>, checkGroupTree: Boolean): String
    public suspend fun buildIsOneOfTypeCondition(types: List<String>): String
    public suspend fun deleteCiviRulesRuleCondition(id: Int)
    public suspend fun updateCiviRulesRuleCondition(conditionId: Int, conditionParameters: String): CiviRulesRuleCondition?
}
public class CiviRulesRuleConditionApi(
    client: HttpClient,
    baseUrl: String,
    apiKey: String,
) : ICiviRulesRuleConditionApi,
    IGetElement {
    private companion object : KLogging()
    private val generalApi = GeneralAPI(client, baseUrl, "CiviRulesRuleCondition", this)

    public override suspend fun getCiviRulesRuleConditions(): List<CiviRulesRuleCondition> = generalApi.getBodyValues<CiviRulesRuleCondition>()

    public override suspend fun getCiviRulesRuleCondition(id: Int): CiviRulesRuleCondition? = getCiviRulesRuleConditions().firstOrNull { it.id == id }

    public override suspend fun buildIsInOneOfSelectedGroupCondition(groupId: Int, checkGroupTree: Boolean): String {
        val groupIdLen = groupId.toString().length
        var checkTree = "0"
        if (checkGroupTree) {
            checkTree = "1"
        }
        return "a:3:{s:9:\"group_ids\";a:1:{i:0;s:$groupIdLen:\"$groupId\";}s:8:\"operator\";s:9:\"in one of\";s:16:\"check_group_tree\";s:1:\"$checkTree\";}"
    }

    public override suspend fun buildIsNotInOneOfSelectedGroupCondition(inGroup: List<Int>, checkGroupTree: Boolean): String {
        val inGroupAsString = inGroup.map { it.toString() }
        var checkFlag = "0"
        if (checkGroupTree) {
            checkFlag = "1"
        }
        var resultString = "a:3:{"
        resultString += buildString("group_ids")
        resultString += buildStringArray(inGroupAsString)
        resultString += buildString("operator")
        resultString += buildString("not in")
        resultString += buildString("check_group_tree")
        resultString += "s:1:\"${checkFlag}\";}"
        return resultString
    }
    private fun buildArrayStringElement(arrayIndex: Int, s: String): String = "i:$arrayIndex;${buildString(s)}"
    private fun buildString(s: String): String = "s:${s.length}:\"$s\";"
    private fun buildStringArray(stringList: List<String>): String {
        var r = "a:${stringList.count()}:{"
        var index = 0
        stringList.forEach { element ->
            r += buildArrayStringElement(index, element)
            index++
        }
        r += "}"
        return r
    }

    public override suspend fun buildIsOneOfTypeCondition(types: List<String>): String {
        var subtypeArray = ""
        var index = 0
        types.forEach {
            subtypeArray += "i:$index;s:${it.length}:\"$it\";"
            index++
        }
        return "a:2:{s:13:\"subtype_names\";a:${types.count()}:{$subtypeArray}s:8:\"operator\";s:9:\"in one of\";}"
    }
    public override suspend fun createCiviRulesRuleCondition(
        ruleId: Int,
        conditionId: Int,
        conditionLink: String?,
        conditionParameters: String,
    ): CiviRulesRuleCondition {
        val s = mutableMapOf<String, JsonElement>()
        s["rule_id"] = JsonPrimitive(ruleId)
        s["condition_id"] = JsonPrimitive(conditionId)
        conditionLink?.let {
            s["condition_link"] = JsonPrimitive(conditionLink)
        }
        s["condition_params"] = JsonPrimitive(conditionParameters)
        s["is_active"] = JsonPrimitive(1)
        logger.info { "body is params=${ParamStringBuilder.value(s)}" }

        generalApi.create(s)
        val allResponses = getCiviRulesRuleConditions()
        val maxId = allResponses.maxOf { it.id }
        return getCiviRulesRuleConditions().find { it.id == maxId }!!

        // if (0 != response.is_error || null == response.values) {
        //    logger.warn { "CiviCRM::CiviRulesApi::createCiviRule failed with error: " + response.error_message }
        //    throw IllegalStateException("CiviCRM::CiviRulesApi::createCiviRule failed ${response.error_message}")
        // }
        // return response.values.single()
    }

    public override suspend fun deleteCiviRulesRuleCondition(id: Int) {
        logger.info { "CiviCRM::CiviRuleRuleConditionApi::deleteCiviRuleRuleCondition with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getCiviRulesRuleCondition(id)

    override suspend fun updateCiviRulesRuleCondition(
        conditionId: Int,
        conditionParameters: String,
    ): CiviRulesRuleCondition? {
        generalApi.update(conditionId, mapOf("condition_params" to JsonPrimitive(conditionParameters)))
        return getCiviRulesRuleCondition(conditionId)
    }
}
