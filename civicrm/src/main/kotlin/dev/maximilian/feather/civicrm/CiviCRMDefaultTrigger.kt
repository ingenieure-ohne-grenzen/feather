/*
 *    Copyright [2024] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

public class CiviCRMDefaultTrigger {
    public companion object {
        public const val NEW_CONTACT_TRIGGER: String = "new_contact"
        public const val CONTACT_IS_REMOVED_FROM_GROUP: String = "deleted_group_contact"
        public const val CONTACT_IS_ADDED_TO_GROUP: String = "new_group_contact"
    }
}
