package dev.maximilian.feather.civicrm.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class CustomField(
    val id: Int,
    @SerialName("custom_group_id")
    val customGroupId: Int,
    val name: String,
    @SerialName("data_type")
    val dataType: String,
    @SerialName("default_value")
    val defaultValue: String,
    val label: String,
    @SerialName("html_type")
    val htmlType: String,
)
