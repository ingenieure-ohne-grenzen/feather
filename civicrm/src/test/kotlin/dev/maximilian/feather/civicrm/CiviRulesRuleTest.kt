/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.TestInstance
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CiviRulesRuleTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Get civicrm rules`() {
        runBlocking {
            val response = civicrm.getCiviRulesRules()
            assertTrue(response.isEmpty(), "CiviRules should be empty by default")
        }
    }

    @Test
    fun `Create civicrm rule rule`() {
        runBlocking {
            val allTrigers = civicrm.getCiviRulesTriggers()
            val contactTrigger = allTrigers.first { it.name == "new_contact" }
            val allOldRules = civicrm.getCiviRulesRules()
            val response = civicrm.createCiviRulesRule(
                "testrule",
                "testlabel",
                contactTrigger.id,
                "testdescription",
                "helptext",
                "Contact of any type is added",
            )
            val allRules = civicrm.getCiviRulesRules()
            assertEquals(allOldRules.count() + 1, allRules.count())
            assertEquals("testrule", allRules.last().name)
            assertEquals("testlabel", allRules.last().label)
            assertEquals("testdescription", allRules.last().description)
            assertTrue(allRules.last().triggerId > 0)
            assertEquals("helptext", allRules.last().helpText)

            civicrm.deleteCiviRulesRule(response.id)
        }
    }

    @Test
    fun `Delete civicrm rule rule`() {
        runBlocking {
            val allTriggers = civicrm.getCiviRulesTriggers()
            val contactTrigger = allTriggers.first { it.name == "new_contact" }
            val allOldRules = civicrm.getCiviRulesRules()
            val response = civicrm.createCiviRulesRule(
                "testrule",
                "testlabel",
                contactTrigger.id,
                "testdescription",
                "helptext",
                "testtrigger",
            )
            civicrm.deleteCiviRulesRule(response.id)
            val allRules = civicrm.getCiviRulesRules()
            assertEquals(allOldRules.sortedBy { it.id }, allRules.sortedBy { it.id })
        }
    }

    suspend fun createTestGroup(): Group {
        val testname = "testgroup-rule-${Random().nextInt(1000)}"
        val testtitle = "testtitle-rule-${Random().nextInt(1000)}"
        val group = Group(0, testname, testtitle, "testdescription for ruletest")
        val response = civicrm.createGroup(group)
        return response
    }
}
