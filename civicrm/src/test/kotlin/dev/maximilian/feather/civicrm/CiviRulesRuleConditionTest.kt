/*
 *    Copyright [2024] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CiviRulesRuleConditionTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Get civicrm rule rule conditions`() {
        runBlocking {
            val response = civicrm.getCiviRulesRuleConditions()
            assertTrue(response.isEmpty(), "Set of standard rule conditions should be empty")
        }
    }

    @Test
    fun `Create civicrm rule rule condition`() {
        runBlocking {
            val allTriggers = civicrm.getCiviRulesTriggers()
            val contactTrigger = allTriggers.first { it.name == "new_contact" }
            val allOldConditions = civicrm.getCiviRulesRuleConditions()
            val rule = civicrm.createCiviRulesRule(
                "testconditionrule",
                "testconditionlabel",
                contactTrigger.id,
                "testconditiondescription",
                "condition helptext",
                "Contact of any type is added",
            )

            val g = civicrm.createGroup(Group(0, "ruletestconditiongroup", "Rule Testcondition Group"))
            val conditionString = civicrm.buildIsInOneOfSelectedGroupCondition(g.id, false)
            val response = civicrm.createCiviRulesRuleCondition(rule.id, 82, null, conditionString)
            val allConditions = civicrm.getCiviRulesRuleConditions()
            assertEquals(allOldConditions.count() + 1, allConditions.count())
            val groupIdLen = g.id.toString().length
            assertEquals("a:3:{s:9:\"group_ids\";a:1:{i:0;s:$groupIdLen:\"${g.id}\";}s:8:\"operator\";s:9:\"in one of\";s:16:\"check_group_tree\";s:1:\"0\";}", allConditions.last().conditionParams)
            assertEquals(82, allConditions.last().conditionId)
            assertEquals(rule.id, allConditions.last().ruleId)

            civicrm.deleteCiviRulesRuleCondition(response.id)
            civicrm.deleteCiviRulesRule(rule.id)
            civicrm.deleteGroup(g.id)
        }
    }

    @Test
    fun `Delete civicrm rule rule condition`() {
        runBlocking {
            val allTriggers = civicrm.getCiviRulesTriggers()
            val contactTrigger = allTriggers.first { it.name == "new_contact" }
            val allOldConditions = civicrm.getCiviRulesRuleConditions()
            val rule = civicrm.createCiviRulesRule(
                "testconditionrule",
                "testconditionlabel",
                contactTrigger.id,
                "testconditiondescription",
                "condition helptext",
                "Contact of any type is added",
            )

            val g = civicrm.createGroup(Group(0, "ruletestconditiongroup", "Rule Testcondition Group"))
            val conditionString = civicrm.buildIsInOneOfSelectedGroupCondition(g.id, false)
            val response = civicrm.createCiviRulesRuleCondition(rule.id, 82, null, conditionString)

            civicrm.deleteCiviRulesRuleCondition(response.id)
            civicrm.deleteCiviRulesRule(rule.id)
            civicrm.deleteGroup(g.id)
            val allConditions = civicrm.getCiviRulesRuleConditions()
            assertEquals(allOldConditions.sortedBy { it.id }, allConditions.sortedBy { it.id })
        }
    }
}
