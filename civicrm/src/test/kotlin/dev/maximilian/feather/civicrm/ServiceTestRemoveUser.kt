/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.Group
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.helper.CredentialScenario
import dev.maximilian.feather.testutils.ServiceConfig
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.random.Random

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServiceTestRemoveUser {

    private val civiService = CiviCRMTestSetup().civiService

    @Test
    fun `CiviCRMService userRemoved from group deletes on last event`() {
        val randomNumber = Random.nextInt()
        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "userdelete")

        runBlocking {
            ServiceTest.createContactTypes(civiService.civicrm)
            val civiGroup = civiService.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")

            val oldGroupContacts = civiService.civicrm.getGroupContacts()
            val oldContacts = civiService.civicrm.getContacts()
            val oldEmails = civiService.civicrm.getEmails()

            civiService.userAddedToGroup(credentialScenario.admin, credentialScenario.group, credentialScenario.testUser.id)
            ServiceConfig.CREDENTIAL_PROVIDER.updateGroup(credentialScenario.group.copy(userMembers = emptySet()))
            credentialScenario.updateUser()
            civiService.userRemovedFromGroup(credentialScenario.testUser.id, credentialScenario.group.id)

            val groupContacts = civiService.civicrm.getGroupContacts()
            val contacts = civiService.civicrm.getContacts()
            val emails = civiService.civicrm.getEmails()

            credentialScenario.deleteScenario()
            civiService.civicrm.deleteGroup(civiGroup.id)

            assertEquals(oldContacts.count(), contacts.count(), "Contact number was changed")
            assertEquals(oldContacts, contacts, "Contacts was changed")
            assertEquals(oldGroupContacts, groupContacts, "GroupContacts was changed")
            assertEquals(oldEmails, emails, "Emails was changed")
        }
    }

    @Test
    fun `CiviCRMService user removed on some crm group with other groups still keeps contact`() {
        val randomNumber = Random.nextInt()
        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "remove")
        val group =
            Group(
                0,
                "rg-test-remove2-$randomNumber-crm",
                "CRM Nutzer von RG Test remove $randomNumber",
                setOf(credentialScenario.testUser.id),
                emptySet(),
                emptySet(),
                emptySet(),
                emptySet(),
                emptySet(),
            )
        val groupCreated2 = ServiceConfig.CREDENTIAL_PROVIDER.createGroup(group)
        credentialScenario.updateUser()

        runBlocking {
            ServiceTest.createContactTypes(civiService.civicrm)
            val civiGroup = civiService.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")
            civiService.civicrm.createGroup(groupCreated2.name, "CRM Nutzer von RG Test2 $randomNumber")

            civiService.userAddedToGroup(credentialScenario.admin, credentialScenario.group, credentialScenario.testUser.id)
            civiService.userAddedToGroup(credentialScenario.admin, groupCreated2, credentialScenario.testUser.id)

            val oldGroupContacts = civiService.civicrm.getGroupContacts()
            val oldContacts = civiService.civicrm.getContacts()
            val oldEmails = civiService.civicrm.getEmails()

            ServiceConfig.CREDENTIAL_PROVIDER.updateGroup(groupCreated2.copy(userMembers = emptySet()))
            credentialScenario.updateUser()
            civiService.userRemovedFromGroup(credentialScenario.testUser.id, groupCreated2.id)

            val groupContacts = civiService.civicrm.getGroupContacts()
            val contacts = civiService.civicrm.getContacts()
            val emails = civiService.civicrm.getEmails()

            credentialScenario.deleteScenario()
            civiService.civicrm.deleteGroup(civiGroup.id)

            assertEquals(oldContacts.count(), contacts.count(), "Contact number was changed")
            assertEquals(oldContacts, contacts, "Contacts was changed")
            assertEquals(oldEmails, emails, "Emails was changed")

            assertEquals(oldGroupContacts.count() - 1, groupContacts.count(), "GroupContact was not added")
            civiService.civicrm.deleteGroupContact(groupContacts.last().id)

            assertEquals("Individual", contacts.last().contactType, "Contact.ContactTyp is not defined properly")
            assertEquals("test$randomNumber", contacts.last().lastName, "Contact.LastName is not defined properly")
            assertEquals("civi$randomNumber", contacts.last().firstName, "Contact.FirstName is not defined properly")

            assertEquals(
                contacts.last().id,
                groupContacts.last().contactId,
                "GroupContact.ContactID is not defined properly",
            )
            assertEquals(civiGroup.id, groupContacts.last().groupId, "GroupContact.GroupID is not defined properly")

            assertEquals(contacts.last().id, emails.last().contactId, "Email.ContactID is not defined properly")
            assertEquals("civi$randomNumber@example.org", emails.last().email, "Email.Email is not defined properly")
        }
    }
}
