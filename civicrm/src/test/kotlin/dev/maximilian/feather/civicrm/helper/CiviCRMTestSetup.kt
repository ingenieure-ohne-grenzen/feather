/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.helper

import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.CiviCRMService
import dev.maximilian.feather.civicrm.CiviCRMSettings
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.getEnv
import kotlinx.coroutines.runBlocking
import redis.clients.jedis.JedisPool

class CiviCRMTestSetup {
    companion object {
        internal val host: String = getEnv("CIVICRM_HOST", "http://localhost:8083")
        internal val apikey = getEnv("CIVICRM_APIKEY", "WuM5g2nuG8kFHXLID74p") // apiuser
        internal val civiCRMSettings =
            CiviCRMSettings(
                "API",
                "User",
                "Administrators",
                host,
                apikey,
                setOf("geschaeftsstelle", "ethikpruefer", "vorstand"),
                setOf("-crm"),
                "geschaeftsstelle",
                "Employee",
                "IogMember",
            )

        private val pool: JedisPool by lazy { ServiceConfig.JEDIS_POOL }
        val backgroundJobManager = BackgroundJobManager(pool)
    }

    val civiService = CiviCRMService(ServiceConfig.CREDENTIAL_PROVIDER, civiCRMSettings, backgroundJobManager)

    fun reset() {
        runBlocking {
            civiService.civicrm.getEmails().filter { it.id > CiviCRMConstants.HIGHEST_ID_OF_DEFAULT_EMAILS }
                .forEach { civiService.civicrm.deleteEmail(it.id) }
            civiService.civicrm.getGroupContacts().forEach { civiService.civicrm.deleteGroupContact(it.id) }
            civiService.civicrm.getContacts().filter { it.id > CiviCRMConstants.HIGHEST_ID_OF_DEFAULT_CONTACTS }
                .forEach { civiService.civicrm.deleteContact(it.id) }
            civiService.civicrm.getBlockedContacts().filter { it.id > CiviCRMConstants.HIGHEST_ID_OF_DEFAULT_CONTACTS }
                .forEach { civiService.civicrm.deleteContact(it.id) }
            val contactTypes = civiService.civicrm.getContactTypes()
            contactTypes.forEach {
                if ((it.name != CiviCRMConstants.ContactTypeDefaults.Organization.protocolName) &&
                    (it.name != CiviCRMConstants.ContactTypeDefaults.Individual.protocolName) &&
                    (it.name != CiviCRMConstants.ContactTypeDefaults.Household.protocolName)
                ) {
                    civiService.civicrm.deleteContactType(it.id)
                }
            }
            civiService.civicrm.getGroups().filter { it.id > 1 }.forEach { civiService.civicrm.deleteGroup(it.id) }
        }
    }
}
