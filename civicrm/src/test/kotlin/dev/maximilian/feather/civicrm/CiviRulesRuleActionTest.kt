/*
 *    Copyright [2024] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CiviRulesRuleActionTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Get civicrm rule rule action`() {
        runBlocking {
            val response = civicrm.getCiviRulesRuleActions()
            assertTrue(response.isEmpty())
        }
    }

    @Test
    fun `Create civicrm rule rule action`() {
        runBlocking {
            val allTriggers = civicrm.getCiviRulesTriggers()
            val contactTrigger = allTriggers.first { it.name == "new_contact" }
            val allOldActions = civicrm.getCiviRulesRuleActions()
            val rule = civicrm.createCiviRulesRule(
                "testrule",
                "testlabel",
                contactTrigger.id,
                "testdescription",
                "helptext",
                "Contact of any type is added",
            )

            val g = civicrm.createGroup(Group(0, "ruletestgroup", "Rule TestGroup"))
            val actionParameters = civicrm.buildActionParamStringToAddContactToGroup(g.id)
            val response = civicrm.createCiviRulesRuleAction(rule.id, 23, actionParameters)
            val allActions = civicrm.getCiviRulesRuleActions()
            assertEquals(allOldActions.count() + 1, allActions.count())
            val groupIdLen = g.id.toString().length
            assertEquals("""a:2:{s:8:"group_id";s:$groupIdLen:"${g.id}";s:9:"group_ids";b:0;}""", allActions.last().actionParams)
            assertEquals(23, allActions.last().actionId)
            assertEquals(rule.id, allActions.last().ruleId)

            civicrm.deleteCiviRulesRule(rule.id)
            civicrm.deleteCiviRulesRuleAction(response.id)
            civicrm.deleteGroup(g.id)
        }
    }

    @Test
    fun `Delete civicrm rule action`() {
        runBlocking {
            val allTrigers = civicrm.getCiviRulesTriggers()
            val contactTrigger = allTrigers.first { it.name == "new_contact" }
            val allOldActions = civicrm.getCiviRulesRuleActions()
            val rule = civicrm.createCiviRulesRule(
                "testrule",
                "testlabel",
                contactTrigger.id,
                "testdescription",
                "helptext",
                "Contact of any type is added",
            )
            val g = civicrm.createGroup(Group(0, "ruletestgroup2", "Rule TestGroup2"))
            val actionParameters = civicrm.buildActionParamStringToAddContactToGroup(g.id)
            val response = civicrm.createCiviRulesRuleAction(rule.id, 23, actionParameters)
            civicrm.deleteCiviRulesRuleAction(response.id)
            civicrm.deleteCiviRulesRule(rule.id)
            val allActions = civicrm.getCiviRulesRuleActions()
            assertEquals(allOldActions.sortedBy { it.id }, allActions.sortedBy { it.id })
            civicrm.deleteGroup(g.id)
        }
    }
}
