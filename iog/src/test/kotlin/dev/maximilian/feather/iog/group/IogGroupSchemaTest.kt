/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.group

import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.testutils.ServiceConfig
import kotlin.test.Test
import kotlin.test.assertEquals

class IogGroupSchemaTest {
    private val credentials = ServiceConfig.CREDENTIAL_PROVIDER

    @Test
    fun `iogschema can identify idn-iog05 as project`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("idn-iog05")
        assertEquals(Pair(IogGroupSchema.PatternTypes.MemberAdminInterested, GroupKind.PROJECT), res)
    }

    @Test
    fun `iogschema can identify idn-iog05-admin as project`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("idn-iog05-admin")
        assertEquals(Pair(IogGroupSchema.PatternTypes.MemberAdminInterested, GroupKind.PROJECT), res)
    }

    @Test
    fun `iogschema can identify ken-zisternen as project`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("ken-zisternen")
        assertEquals(Pair(IogGroupSchema.PatternTypes.MemberAdminInterested, GroupKind.PROJECT), res)
    }

    @Test
    fun `iogschema can identify ken-zisternen-admin as project`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("ken-zisternen-admin")
        assertEquals(Pair(IogGroupSchema.PatternTypes.MemberAdminInterested, GroupKind.PROJECT), res)
    }

    @Test
    fun `iogschema can identify rg-ilmenau as Regionalgroup`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("rg-ilmenau")
        assertEquals(Pair(IogGroupSchema.PatternTypes.MemberCrmInterested, GroupKind.REGIONAL_GROUP), res)
    }

    @Test
    fun `iogschema can identify rg-ilmenau-admin as REGIONAL_GROUP`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("rg-ilmenau-admin")
        assertEquals(Pair(IogGroupSchema.PatternTypes.MemberCrmInterested, GroupKind.REGIONAL_GROUP), res)
    }

    @Test
    fun `iogschema can identify gs-gf as no project`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("gs-gf")
        assertEquals(Pair(IogGroupSchema.PatternTypes.Other, GroupKind.LDAP_ONLY), res)
    }

    @Test
    fun `iogschema can identify oms as no project`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("oms")
        assertEquals(Pair(IogGroupSchema.PatternTypes.SimpleMemberAdmin, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET), res)
    }

    @Test
    fun `iogschema can identify oms-admin as NO_SPECIALITY_MEMBER_ADMIN_SECRET`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("oms-admin")
        assertEquals(Pair(IogGroupSchema.PatternTypes.SimpleMemberAdmin, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET), res)
    }

    @Test
    fun `iogschema can identify progr-landgemei-wissen as NO_SPECIALITY_MEMBER_ADMIN_PUBLIC`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("progr-landgemei-wissen")
        assertEquals(Pair(IogGroupSchema.PatternTypes.SimpleMemberAdmin, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC), res)
    }

    @Test
    fun `iogschema can identify ap-sprecherinnen as COLLABORATION`() {
        val a = IogGroupSchema(credentials)
        val res = a.identifyNodePatternType("ap-sprecherinnen")
        assertEquals(Pair(IogGroupSchema.PatternTypes.SimpleMemberAdmin, GroupKind.COLLABORATION), res)
    }
}
