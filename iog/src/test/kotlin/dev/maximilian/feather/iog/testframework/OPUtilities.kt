/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.OPGroupNames
import dev.maximilian.feather.iog.internal.settings.OPPName
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.mockserver.GroupSyncMock
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.IogServiceApiConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectDescription
import dev.maximilian.feather.openproject.OpenProjectMembership
import dev.maximilian.feather.openproject.OpenProjectProject
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import java.util.UUID

internal class OPUtilities(private val op: IOpenProject, backgroundJobManager: BackgroundJobManager, private val groupSyncMock: GroupSyncMock) {
    private val onc = OPNameConfig(mutableMapOf())
    private val opc = OpenProjectPCreator(op, onc, UUID.randomUUID(), backgroundJobManager)

    internal fun createOPProjectWithMemberships(
        identifier: String,
        name: String,
        groupList: List<Group>,
        parentProjectName: String,
        groupKind: GroupKind,
    ) {
        runBlocking { op.getProjectByIdentifierOrName(identifier) }?.let {
            runBlocking { op.deleteProject(it) }
        }

        runBlocking {
            // OP v14.x seems to have problems when projects are created in parallel
            /*
            val job1 = async { op.getProjectByIdentifierOrName(parentProjectName) }
            val job2 = async { op.getProjectByIdentifierOrName(identifier)?.let { op.deleteProject(it) } }
            val rolesJob = async { op.getRoles() }

            awaitAll(job1, job2, rolesJob)

            val parentProjectId = job1.await();
            val roles = rolesJob.await();*/

            val parentProjectId = op.getProjectByIdentifierOrName(parentProjectName)
            /*op.getProjectByIdentifierOrName(identifier)?.let { op.deleteProject(it) }*/

            val roles = op.getRoles()

            val projectCreated =
                op.createProject(
                    OpenProjectProject(
                        3,
                        name,
                        identifier,
                        OpenProjectDescription("", "Description of $name", "op/${OPPName.OTHER}}/$parentProjectName/$identifier"),
                        parentProjectId,
                    ),
                )

            val no =
                when (groupKind) {
                    GroupKind.REGIONAL_GROUP -> IogServiceApiConstants.GROUPS_PER_MEMBER_CRM_INTERESTED_PATTERN
                    GroupKind.PROJECT, GroupKind.BILA_GROUP, GroupKind.PR_FR_GROUP -> IogServiceApiConstants.GROUPS_PER_MEMBER_ADMIN_INTERESTED_PATTERN
                    else -> IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN
                }
            val rolesToBeAssigned =
                opc.getRolesToBeAssigned(
                    groupKind,
                    no,
                    roles,
                )

            rolesToBeAssigned.forEachIndexed { index, openProjectRole ->
                openProjectRole?.let {
                    runBlocking {
                        opc.reassignRole(groupList[index], projectCreated, openProjectRole.name, null, groupSyncMock)
                    }
                } ?: opc.createGroups(listOf(groupList[index]), groupSyncMock)
            }

            // OP v14.x seems to have problems when projects are created in parallel
            /*opc.getPublicRoles(groupKind, roles).map {
                async { op.createMembership(OpenProjectMembership(0, projectCreated, it.group, listOf(it.role))) }
            }.awaitAll()*/
            opc.getPublicRoles(groupKind, roles).map {
                op.createMembership(OpenProjectMembership(0, projectCreated, it.group, listOf(it.role)))
            }
        }
    }

    internal fun createStandardProjects(): Pair<OpenProjectProject, OpenProjectProject> {
        // OP v14.x seems to have problems when projects are created in parallel
        /*val mainProject = createOPProjectIfNotExisting(OPPName.OTHER, "IOG", null)
        val rgIntern = createOPProjectIfNotExisting(OPPName.REGIONAL_GROUP, "RG Intern", mainProject)
        createOPProjectIfNotExisting(OPPName.PROJECTS, "Projekte", mainProject)
        createOPProjectIfNotExisting(OPPName.BILA, "BiLa", mainProject)
        createOPProjectIfNotExisting(OPPName.PR_FR, "PR-FR", mainProject)
        createOPProjectIfNotExisting(OPPName.COLLABORATION, "Projektübergreifende Zusammenarbeit", mainProject)
        val kgIntern = createOPProjectIfNotExisting(OPPName.COMMITTEE, "Ausschüsse und KGs", mainProject)

        coroutineScope {
            val job1 =
                async {
                    if (op.getProjectByIdentifierOrName("vorstand") == null) {
                        opc.copyProjectAndAssignRoles(
                            "Vorstand",
                            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                            IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                            LdapNames.ASSOCIATION_BOARD,
                            listOf(
                                op.getGroupByName(LdapNames.ASSOCIATION_BOARD)!!,
                                op.getGroupByName(LdapNames.ASSOCIATION_BOARD + IogPluginConstants.ADMIN_SUFFIX)!!,
                            ),
                        )
                    }
                }

            val job2 =
                async {
                    if (op.getProjectByIdentifierOrName("oms") == null) {
                        opc.copyProjectAndAssignRoles(
                            "Ordentliche Mitglieder",
                            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                            IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                            LdapNames.OMS,
                            listOf(
                                op.getGroupByName(LdapNames.OMS)!!,
                                op.getGroupByName(LdapNames.OMS + IogPluginConstants.ADMIN_SUFFIX)!!,
                            ),
                        )
                    }
                }

            val job3 =
                async {
                    val gsProject =
                        op.getProjectByIdentifierOrName("geschaeftsstelle") ?: opc.copyProjectAndAssignRoles(
                            "Geschäftsstelle",
                            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                            IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                            LdapNames.CENTRAL_OFFICE,
                            listOf(
                                op.getGroupByName(LdapNames.CENTRAL_OFFICE)!!,
                                op.getGroupByName(LdapNames.CENTRAL_OFFICE + IogPluginConstants.ADMIN_SUFFIX)!!,
                            ),
                        )
                    createOPProjectIfNotExisting("proko", "PROKO", gsProject)
                }

            val job4 =
                async {
                    if (op.getProjectByIdentifierOrName("ap-sprecherinnen") == null) {
                        opc.copyProjectAndAssignRoles(
                            "AP SprecherInnen",
                            GroupKind.COLLABORATION,
                            IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                            LdapNames.AP_SPRECHERINNEN,
                            listOf(
                                op.getGroupByName(LdapNames.AP_SPRECHERINNEN)!!,
                                op.getGroupByName(LdapNames.AP_SPRECHERINNEN + IogPluginConstants.ADMIN_SUFFIX)!!,
                            ),
                        )
                    }
                }

            listOf(job1, job2, job3, job4)
        }.awaitAll()*/

        val mainProject = createOPProjectIfNotExisting(OPPName.OTHER, "IOG", null)
        val rgIntern = createOPProjectIfNotExisting(OPPName.REGIONAL_GROUP, "RG Intern", mainProject)
        createOPProjectIfNotExisting(OPPName.PROJECTS, "Projekte", mainProject)
        createOPProjectIfNotExisting(OPPName.BILA, "BiLa", mainProject)
        createOPProjectIfNotExisting(OPPName.PR_FR, "PR-FR", mainProject)
        createOPProjectIfNotExisting(OPPName.COLLABORATION, "Projektübergreifende Zusammenarbeit", mainProject)
        val kgIntern = createOPProjectIfNotExisting(OPPName.COMMITTEE, "Ausschüsse und KGs", mainProject)

        if (runBlocking { op.getProjectByIdentifierOrName("vorstand") } == null) {
            runBlocking {
                opc.copyProjectAndAssignRoles(
                    "Vorstand",
                    GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                    IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                    LdapNames.ASSOCIATION_BOARD,
                    listOf(
                        op.getGroupByName(LdapNames.ASSOCIATION_BOARD)!!,
                        op.getGroupByName(LdapNames.ASSOCIATION_BOARD + IogPluginConstants.ADMIN_SUFFIX)!!,
                    ),
                )
            }
        }

        if (runBlocking { op.getProjectByIdentifierOrName("oms") } == null) {
            runBlocking {
                opc.copyProjectAndAssignRoles(
                    "Ordentliche Mitglieder",
                    GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                    IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                    LdapNames.OMS,
                    listOf(
                        op.getGroupByName(LdapNames.OMS)!!,
                        op.getGroupByName(LdapNames.OMS + IogPluginConstants.ADMIN_SUFFIX)!!,
                    ),
                )
            }
        }

        val gsProject = runBlocking { op.getProjectByIdentifierOrName("geschaeftsstelle") }
            ?: runBlocking {
                opc.copyProjectAndAssignRoles(
                    "Geschäftsstelle",
                    GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                    IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                    LdapNames.CENTRAL_OFFICE,
                    listOf(
                        op.getGroupByName(LdapNames.CENTRAL_OFFICE)!!,
                        op.getGroupByName(LdapNames.CENTRAL_OFFICE + IogPluginConstants.ADMIN_SUFFIX)!!,
                    ),
                )
            }
        createOPProjectIfNotExisting("proko", "PROKO", gsProject)

        if (runBlocking { op.getProjectByIdentifierOrName("ap-sprecherinnen") } == null) {
            runBlocking {
                opc.copyProjectAndAssignRoles(
                    "AP SprecherInnen",
                    GroupKind.COLLABORATION,
                    IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                    LdapNames.AP_SPRECHERINNEN,
                    listOf(
                        op.getGroupByName(LdapNames.AP_SPRECHERINNEN)!!,
                        op.getGroupByName(LdapNames.AP_SPRECHERINNEN + IogPluginConstants.ADMIN_SUFFIX)!!,
                    ),
                )
            }
        }

        return Pair(rgIntern, kgIntern)
    }

    private fun createOPProjectIfNotExisting(
        identifier: String,
        name: String,
        parentProject: OpenProjectProject?,
    ): OpenProjectProject {
        val project = runBlocking { op.getProjectByIdentifierOrName(identifier) }
        if (project == null) {
            val projectNew =
                OpenProjectProject(
                    2,
                    name,
                    identifier,
                    OpenProjectDescription("", identifier, "op/${OPPName.OTHER}/$identifier"),
                    parentProject,
                )
            return runBlocking { op.createProject(projectNew) }
        }
        return project
    }

    internal suspend fun createOPGroups(groups: List<Group>) {
        val missingGroups =
            groups.asFlow().filter {
                op.getGroupByName(onc.credentialGroupToOpenProjectGroup(it.name)) == null
            }.toList()

        if (missingGroups.isNotEmpty()) {
            opc.createGroups(missingGroups, groupSyncMock)
            // opc.createLdapGroupSynchronisations(groups, opGroups)
        }
    }

    fun removeOPProjectWithGroups(
        projectId: String,
        groups: List<String>,
    ) {
        /*runBlocking {
            val job1 =
                async {
                    op.getProjectByIdentifierOrName(projectId)?.also {
                        op.deleteProject(it)
                    }
                }

            (groups.map { g -> async { op.getGroupByName(g)?.let { op.deleteGroup(it) } } } + job1).awaitAll()
        }*/
        runBlocking { op.getProjectByIdentifierOrName(projectId) }?.also {
            runBlocking { op.deleteProject(it) }
        }

        groups.forEach { g -> runBlocking { op.getGroupByName(g) }?.let { runBlocking { op.deleteGroup(it) } } }
    }

    fun reset() {
        runBlocking {
            op.getProjectByIdentifierOrName("iog")?.let { op.deleteProject(it) }
        }
    }

    fun removeGeneralTrialMembers() {
        runBlocking {
            op.getGroupByName(OPGroupNames.TRIAL_MEMBERS)?.let { op.deleteGroup(it) }
        }
    }
}
