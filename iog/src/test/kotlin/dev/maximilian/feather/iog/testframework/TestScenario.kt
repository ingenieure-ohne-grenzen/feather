/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.civicrm.CiviCRMService
import dev.maximilian.feather.iog.IogPlugin
import dev.maximilian.feather.iog.mockserver.GroupSyncMock
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.openproject.IOpenProject
import io.javalin.Javalin

internal data class TestScenario(
    val basePath: String,
    val app: Javalin,
    val services: MutableList<IControllableService>,
    val nextcloud: Nextcloud?,
    val nextcloudService: NextcloudService?,
    val openProject: IOpenProject?,
    val openProjectService: OpenProjectService?,
    val iogPlugin: IogPlugin?,
    val opUtilities: OPUtilities,
    val ncUtilities: NCUtilities,
    val groupSyncMock: GroupSyncMock,
    val civiCRM: CiviCRMService?,
    val civiTestUtilities: CiviUtilities?,
)
