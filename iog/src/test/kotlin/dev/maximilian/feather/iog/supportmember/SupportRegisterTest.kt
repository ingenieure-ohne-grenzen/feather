/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package dev.maximilian.feather.iog.supportmember

import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB
import dev.maximilian.feather.iog.internal.tools.DatabaseValidation
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class SupportRegisterTest {
    companion object {
        private val db = ApiTestUtilities.db
        private val accountController = ServiceConfig.ACCOUNT_CONTROLLER
        private val databaseValidation = DatabaseValidation()
    }

    @Test
    fun `Test store and getExternalIDbyLDAPTest`() {
        val supportMemberRegister = SupportMemberDB(db, accountController)
        supportMemberRegister.deleteAll()
        val users = (0 until 3).map { accountController.createUser(TestUser.generateTestUser()) }

        supportMemberRegister.storeHash(221, UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString())
        supportMemberRegister.storeHash(777, UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString())
        supportMemberRegister.storeHash(444, UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString())

        supportMemberRegister.storeLdapAndExternalID(users[0].id, 221)
        supportMemberRegister.storeLdapAndExternalID(users[1].id, 777)
        supportMemberRegister.storeLdapAndExternalID(users[2].id, 444)
        val id = supportMemberRegister.getExternalIDbyLDAP(users[1].id)
        assertEquals(id, 777)
    }

    @Test
    fun `Test deleteAll`() {
        val supportMemberRegister = SupportMemberDB(db, accountController)
        val user = accountController.createUser(TestUser.generateTestUser())
        supportMemberRegister.storeHash(777, UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString())
        supportMemberRegister.storeLdapAndExternalID(user.id, 777)
        val c1 = supportMemberRegister.countRegisteredDKPUser()
        assertEquals(1, c1)
        supportMemberRegister.deleteAll()

        val c2 = supportMemberRegister.countRegisteredDKPUser()
        assertEquals(0, c2)
    }

    @Test
    fun `Test notFoundMemberDeliversNull`() {
        val supportMemberRegister = SupportMemberDB(db, accountController)
        supportMemberRegister.deleteAll()
        assertNull(supportMemberRegister.getExternalIDbyLDAP(-42))
    }

    @Test
    fun `Test registerBirthday And Compare`() {
        val supportMemberRegister = SupportMemberDB(db, accountController)
        supportMemberRegister.deleteAll()
        val hash = databaseValidation.makeHash("HaraldTest01.01.1995")
        supportMemberRegister.storeHash(
            444,
            databaseValidation.makeHash("Donald05.02.1995"),
            databaseValidation.makeHash("ewe@example.org"),
            databaseValidation.makeHash("zzt@example.org"),
        )
        supportMemberRegister.storeHash(
            777,
            hash,
            databaseValidation.makeHash("tester@example.org"),
            databaseValidation.makeHash("cheese@example.org"),
        )
        supportMemberRegister.storeHash(
            999,
            databaseValidation.makeHash("Renate01.01.1994"),
            databaseValidation.makeHash("qq@example.org"),
            databaseValidation.makeHash("tt@example.org"),
        )
        val q = supportMemberRegister.getExternalIDbyNameHash(hash)
        assertEquals(q, 777)
    }

    @Test
    fun `Test getExternalIDPerMail`() {
        val supportMemberRegister = SupportMemberDB(db, accountController)
        supportMemberRegister.deleteAll()
        val hash = databaseValidation.makeHash("tester@t-online.de")
        supportMemberRegister.storeHash(
            444,
            databaseValidation.makeHash("Donald05.02.1995"),
            databaseValidation.makeHash("ewe@example.org"),
            databaseValidation.makeHash("zzt@example.org"),
        )
        supportMemberRegister.storeHash(
            777,
            databaseValidation.makeHash("HaraldTest01.01.1995"),
            hash,
            databaseValidation.makeHash("cheese@example.org"),
        )
        supportMemberRegister.storeHash(
            999,
            databaseValidation.makeHash("Renate01.01.1994"),
            databaseValidation.makeHash("qq@example.org"),
            databaseValidation.makeHash("tt@example.org"),
        )

        val c = supportMemberRegister.countSupportMember()
        assertEquals(c, 3)
        val q = supportMemberRegister.getExternalIDbyMail(hash)
        assertEquals(q, 777)
    }

    @Test
    fun `Test getExternalIDByAlternativeMail`() {
        val supportMemberRegister = SupportMemberDB(db, accountController)
        supportMemberRegister.deleteAll()
        val hash = databaseValidation.makeHash("tester@t-online.de")
        supportMemberRegister.storeHash(
            444,
            databaseValidation.makeHash("Donald05.02.1995"),
            databaseValidation.makeHash("ewe@example.org"),
            databaseValidation.makeHash("zzt@example.org"),
        )
        supportMemberRegister.storeHash(
            777,
            databaseValidation.makeHash("HaraldTest01.01.1995"),
            databaseValidation.makeHash("cheese@example.org"),
            hash,
        )
        supportMemberRegister.storeHash(
            999,
            databaseValidation.makeHash("Renate01.01.1994"),
            databaseValidation.makeHash("qq@example.org"),
            databaseValidation.makeHash("tt@example.org"),
        )
        val q = supportMemberRegister.getExternalIDbyMail(hash)
        assertEquals(777, q)
    }

    @Test
    fun `Test read and write metadata`() {
        val supportMemberRegister = SupportMemberDB(db, accountController)
        supportMemberRegister.deleteAll()
        val key = SupportMemberDB.MetadataKey.values()[0]
        val value = "Test"
        supportMemberRegister.setMetadata(key, value)
        assertEquals(value, supportMemberRegister.getMetadata(key))

        val newValue = "Test 2"
        supportMemberRegister.setMetadata(key, newValue)
        assertEquals(newValue, supportMemberRegister.getMetadata(key))
    }
}
