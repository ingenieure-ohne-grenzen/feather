/*
 *    Copyright [2024] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.civicrm

import dev.maximilian.feather.civicrm.CiviCRMDefaultActions
import dev.maximilian.feather.civicrm.CiviCRMDefaultConditions
import dev.maximilian.feather.civicrm.CiviCRMDefaultTrigger
import dev.maximilian.feather.civicrm.internal.ICiviCRM
import dev.maximilian.feather.iog.internal.civicrm.UpdateCRMWithRules.Companion.logger
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants

class GeneralCRMRules(private val civicrm: ICiviCRM) {
    companion object {
        public const val ADD_CONTACT_RULE_NAME = "hinzufügen_zu_alle_crm-nutzer"
        public const val REMOVE_CONTACT_RULE_NAME = "entfernen_aus_alle_crm-nutzer"
    }
    suspend fun updateGeneralCrmRule(personContactsId: Int) {
        val g = civicrm.getGroups()
        val centralOfficeGroupId = g.find { it.name == LdapNames.CENTRAL_OFFICE }?.id
        val ethicGroupId = g.find { it.name == LdapNames.ETHIC_VALIDATION_GROUP }?.id
        val associationBoardId = g.find { it.name == LdapNames.ASSOCIATION_BOARD }?.id

        val defaultList = listOf(
            centralOfficeGroupId,
            ethicGroupId!!,
            associationBoardId,
            personContactsId,
        )

        val groupIdList = mutableListOf<Int>()
        groupIdList.addAll(defaultList.mapNotNull { it })
        g.filter { it.name.endsWith(IogPluginConstants.CRM_SUFFIX) }.forEach { groupIdList.add(it.id) }
        civicrm.getCiviRulesRules().find { it.name == REMOVE_CONTACT_RULE_NAME }?.let { rule ->

            civicrm.getCiviRulesRuleConditions().find { it.ruleId == rule.id && it.conditionParams?.contains("not in") ?: false }?.let { condition ->
                civicrm.updateCiviRulesRuleCondition(
                    condition.id,
                    civicrm.buildIsNotInOneOfSelectedGroupCondition(groupIdList, false),
                )
            }
        } ?: logger.warn { "GeneralCRMRules::updateGeneralRule could not find general CIVICRM Add rule with name $REMOVE_CONTACT_RULE_NAME to update" }
    }

    suspend fun installGeneralCiviRulesDelete(createSchemeResult: CiviGroupSchemeCreationResult) {
        val contactDeletedTriggerId =
            civicrm.getCiviRulesTriggers().find { it.name == CiviCRMDefaultTrigger.CONTACT_IS_REMOVED_FROM_GROUP }!!.id

        val rule = civicrm.createCiviRulesRule(
            REMOVE_CONTACT_RULE_NAME,
            "Entfernen aus Alle CRM-Nutzer",
            contactDeletedTriggerId,
            "Stellt sicher, dass CRM-Nutzer auch explizit aus der Gruppe \"Alle CRM-Nutzer\" entfernt werden.",
            "Remove from all CRM-Users",
            "Contact is removed from Group CiviCrm",
        )

        val conditionId =
            civicrm.getCiviRuleConditions().find { it.name == CiviCRMDefaultConditions.CONTACT_IN_GROUP }!!.id

        civicrm.createCiviRulesRuleCondition(
            rule.id,
            conditionId,
            null,
            civicrm.buildIsNotInOneOfSelectedGroupCondition(
                listOf(
                    createSchemeResult.centralOfficeGroupId,
                    createSchemeResult.ethicGroupId!!,
                    createSchemeResult.associationBoardId,
                    createSchemeResult.personContactsId,
                ),
                true,
            ),
        )

        civicrm.createCiviRulesRuleCondition(
            rule.id,
            conditionId,
            "AND",
            civicrm.buildIsInOneOfSelectedGroupCondition(createSchemeResult.userGroupId, false),
        )
        val addAction = civicrm.getCiviRuleActions().find { it.name == CiviCRMDefaultActions.REMOVE_CONTACT_FROM_GROUP_ACTION }
        if (addAction == null) {
            logger.error { "UpdateCRMWithRules::installGeneralCiviRulesAdd Cannot find add contact to group action" }
        } else {
            civicrm.createCiviRulesRuleAction(rule.id, addAction.id, civicrm.buildActionParamStringToAddContactToGroup(createSchemeResult.userGroupId))
        }
    }
    suspend fun installGeneralCiviRulesAdd(createSchemeResult: CiviGroupSchemeCreationResult) {
        val contactAddedToGroupTriggerId =
            civicrm.getCiviRulesTriggers().find { it.name == CiviCRMDefaultTrigger.CONTACT_IS_ADDED_TO_GROUP }!!.id

        val rule = civicrm.createCiviRulesRule(
            ADD_CONTACT_RULE_NAME,
            "Hinzufügen zu Alle CRM-Nutzer",
            contactAddedToGroupTriggerId,
            "Stellt sicher, dass alle CRM-Nutzer auch explizit zur Gruppe \"Alle CRM-Nutzer\" hinzugefügt werden.",
            "Remove from all CRM-Users",
            "Contact is removed from Group CiviCrm",
        )
        val conditionId =
            civicrm.getCiviRuleConditions().find { it.name == CiviCRMDefaultConditions.CONTACT_IN_GROUP }!!.id

        civicrm.createCiviRulesRuleCondition(
            rule.id,
            conditionId,
            null,
            civicrm.buildIsInOneOfSelectedGroupCondition(createSchemeResult.userGroupId, true),
        )

        civicrm.createCiviRulesRuleCondition(
            rule.id,
            conditionId,
            "AND",
            civicrm.buildIsInOneOfSelectedGroupCondition(createSchemeResult.userGroupId, false),
        )
        val addAction = civicrm.getCiviRuleActions().find { it.name == CiviCRMDefaultActions.ADD_CONTACT_TO_GROUP_ACTION }
        if (addAction == null) {
            logger.error { "UpdateCRMWithRules::installGeneralCiviRulesAdd Cannot find add contact to group action" }
        } else {
            civicrm.createCiviRulesRuleAction(rule.id, addAction.id, civicrm.buildActionParamStringToAddContactToGroup(createSchemeResult.userGroupId))
        }
    }
}
