/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.trials

import dev.maximilian.feather.account.AccountController
import mu.KotlinLogging
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.upsert
import java.time.Instant

internal class TrialMemberDB(val db: Database, accountController: AccountController) {
    private val logger = KotlinLogging.logger { }
    private val trialTable = TrialTable(accountController)
    private val openRequestTable = OpenRequestTable(accountController)

    init {
        logger.info { "TrialMemberDB::init create DB " }
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(trialTable)
            SchemaUtils.createMissingTablesAndColumns(openRequestTable)
        }
        logger.info { "TrialMemberDB::init finished" }
    }

    class TrialTable(accountController: AccountController) : Table() {
        val ldapID: Column<EntityID<Int>> = reference("ldapID", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)
        val startOfTrial = timestamp("startOfTrial")
        val extensions = integer("extensions")
        override val primaryKey: PrimaryKey = PrimaryKey(ldapID)
    }

    class OpenRequestTable(accountController: AccountController) : UUIDTable("requests") {
        val requesterId: Column<EntityID<Int>> =
            reference("requesterId", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)
        val started: Column<Instant> = timestamp("started")
    }

    fun startFirstTrial(
        ldapID: Int,
        start: Instant,
    ) = transaction(db) {
        trialTable.insert {
            it[trialTable.ldapID] = ldapID
            it[startOfTrial] = start
            it[extensions] = 0
        }
    }

    fun updateTrials(
        ldapID: Int,
        no: Int,
        started: Instant,
    ) = transaction(db) {
        trialTable.deleteWhere { trialTable.ldapID eq ldapID }
        trialTable.insert {
            it[trialTable.ldapID] = ldapID
            it[startOfTrial] = started
            it[extensions] = no
        }
    }

    fun deleteAll() =
        transaction(db) {
            trialTable.deleteAll()
            openRequestTable.deleteAll()
        }

    fun getAllRequests(): List<RequestEntry> =
        transaction(db) {
            openRequestTable.selectAll().orderBy(openRequestTable.started).map { rowToRequestState(it) }
        }

    fun getTrialState(id: Int): TrialEntry? =
        transaction(db) {
            trialTable.select { trialTable.ldapID eq id }.singleOrNull()?.let { rowToTrialState(it) }
        }

    fun getAllTrialStates(): List<TrialEntry> =
        transaction(db) {
            trialTable.selectAll().map { rowToTrialState(it) }
        }

    fun getRequestState(id: Int): RequestEntry? =
        transaction(db) {
            openRequestTable.select { openRequestTable.requesterId eq id }.singleOrNull()?.let { rowToRequestState(it) }
        }

    fun createRequest(
        ldapID: Int,
        start: Instant,
    ) = transaction(db) {
        openRequestTable.upsert {
            it[requesterId] = ldapID
            it[started] = start
        }
    }

    fun removeRequest(ldapID: Int) =
        transaction(db) {
            openRequestTable.deleteWhere { openRequestTable.requesterId eq ldapID }
        }

    fun removeTrial(ldapID: Int) =
        transaction(db) {
            trialTable.deleteWhere { trialTable.ldapID eq ldapID }
        }

    private fun rowToTrialState(it: ResultRow): TrialEntry =
        TrialEntry(
            ldapID = it[trialTable.ldapID].value,
            Extensions = it[trialTable.extensions],
            Started = it[trialTable.startOfTrial],
        )

    private fun rowToRequestState(it: ResultRow): RequestEntry =
        RequestEntry(
            requestUserID = it[openRequestTable.requesterId].value,
            started = it[openRequestTable.started],
        )
}
