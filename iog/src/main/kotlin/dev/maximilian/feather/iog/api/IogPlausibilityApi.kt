/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.api

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.iog.internal.group.GroupPlausibility
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.requireAdminPermission
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.bodyAsClass
import io.javalin.openapi.HttpMethod
import io.javalin.openapi.OpenApi
import io.javalin.openapi.OpenApiContent
import io.javalin.openapi.OpenApiRequestBody
import io.javalin.openapi.OpenApiResponse
import mu.KotlinLogging

internal data class GroupPlausibilityConfig(
    val autoRepair: AutoRepair = AutoRepair.JUST_CHECK,
    val autoRepairGroupIds: Set<Int>? = null,
    val checkOpenProject: Boolean = true,
    val checkNextcloud: Boolean = true,
)

internal enum class AutoRepair {
    JUST_CHECK,
    AUTOREPAIR_ALL,
    AUTOREPAIR_SELECTED,
    RESYNC_ONLY,
}

internal class GroupPlausibilityApi(
    app: Javalin,
    val credentialProvider: ICredentialProvider,
    val backgroundJobManager: BackgroundJobManager,
    private val iogSchema: IogGroupSchema,
    private val openProjectService: OpenProjectService,
    private val syncEvent: GroupSynchronizationEvent,
    private val onc: OPNameConfig,
    private val nc: Nextcloud,
    private val nextcloudPublicURL: String,
) {
    init {
        app.routes {
            path("bindings/iog/plausibility") {
                post("/checkgroups", ::handleCreateGroupCheckJob, Permission.ADMIN)
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    @OpenApi(
        summary = "Check group plausibility",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("201"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/plausibility",
        methods = [HttpMethod.POST],
    )
    private fun handleCreateGroupCheckJob(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        try {
            creator.requireAdminPermission("createGroupPlausibilityCheck")
        } catch (e: Exception) {
            logger.warn { "IogPlausibilityApi::handleCreateGroupCheckJob() permission denied. createGroupPlausibilityCheck required" }
            throw ForbiddenResponse()
        }

        val config: GroupPlausibilityConfig
        try {
            config = ctx.bodyAsClass<GroupPlausibilityConfig>()
        } catch (e: Exception) {
            logger.warn { "IogPlausibilityApi::handleCreateGroupCheckJob() malformed entity" }
            throw BadRequestResponse()
        }

        logger.info { "About to check the group plausibility by admin ${creator.displayName}" }

        val groupPlausibility =
            GroupPlausibility(
                credentialProvider,
                backgroundJobManager,
                iogSchema,
                openProjectService,
                syncEvent,
                onc,
                nc,
                nextcloudPublicURL,
            )

        val newJob =
            backgroundJobManager.runBackgroundJob(
                groupPlausibility::checkGroupPlausibility,
                config,
            )

        ctx.status(201)
        ctx.json(newJob)
    }
}
