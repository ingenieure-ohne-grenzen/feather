/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminInterestedPattern
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminTrialPattern
import dev.maximilian.feather.iog.internal.groupPattern.MemberCrmInterestedPattern
import dev.maximilian.feather.iog.internal.groupPattern.SimpleMemberAdminCRM
import dev.maximilian.feather.iog.internal.groupPattern.SimpleMemberAdminPattern
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants

internal class IogGroupSchema(private val credentials: ICredentialProvider) {
    companion object {
        fun groupPrefixAndSuffix(groupKind: GroupKind): Pair<String?, String?> {
            when (groupKind) {
                GroupKind.REGIONAL_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.RG_PREFIX.split("-").first(),
                    null,
                )
                GroupKind.COMPETENCE_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.KG_PREFIX.split("-").first(),
                    null,
                )
                GroupKind.COMMITTEE -> return Pair<String?, String?>(
                    IogPluginConstants.AS_PREFIX.split("-").first(),
                    null,
                )
                GroupKind.BILA_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.RG_PREFIX.split("-").first(),
                    IogPluginConstants.BILA_SUFFIX.split("-").last(),
                )
                GroupKind.PR_FR_GROUP -> return Pair<String?, String?>(
                    IogPluginConstants.RG_PREFIX.split("-").first(),
                    IogPluginConstants.PR_FR_SUFFIX.split("-").last(),
                )
                GroupKind.PROJECT -> return Pair<String?, String?>(null, null)
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED -> return Pair<String?, String?>(null, null)
                GroupKind.COLLABORATION,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                -> return Pair<String?, String?>(null, null)
                GroupKind.LDAP_ONLY -> return Pair<String?, String?>(null, null)
                // else -> throw IllegalArgumentException("invalid group kind")
            }
        }

        val possibleSubGroupSuffixes =
            listOf(
                IogPluginConstants.INTERESTED_SUFFIX,
                IogPluginConstants.MEMBER_SUFFIX,
                IogPluginConstants.TRIAL_SUFFIX,
                IogPluginConstants.ADMIN_SUFFIX,
                IogPluginConstants.CRM_SUFFIX,
            )
        val possibleKindPrefixes =
            listOf(
                IogPluginConstants.RG_PREFIX,
                IogPluginConstants.AS_PREFIX,
                IogPluginConstants.KG_PREFIX,
            )
    }

    enum class PatternTypes { SimpleMemberAdmin, SimpleMemberAdminCRM, MemberCrmInterested, MemberAdminInterested, MemberAdminTrial, Other }

    internal val groupKindPatternRelation =
        setOf(
            Pair(GroupKind.REGIONAL_GROUP, PatternTypes.MemberCrmInterested),
            Pair(GroupKind.BILA_GROUP, PatternTypes.SimpleMemberAdmin),
            Pair(GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED, PatternTypes.MemberAdminInterested),
            Pair(GroupKind.COLLABORATION, PatternTypes.SimpleMemberAdmin),
            Pair(GroupKind.COMMITTEE, PatternTypes.MemberAdminTrial),
            Pair(GroupKind.LDAP_ONLY, PatternTypes.Other),
            Pair(GroupKind.COMPETENCE_GROUP, PatternTypes.MemberAdminTrial),
            Pair(GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC, PatternTypes.SimpleMemberAdmin),
            Pair(GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC, PatternTypes.SimpleMemberAdminCRM),
            Pair(GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET, PatternTypes.SimpleMemberAdmin),
            Pair(GroupKind.PROJECT, PatternTypes.MemberAdminInterested),
            Pair(GroupKind.PR_FR_GROUP, PatternTypes.SimpleMemberAdmin),
        )

    private val prefixGroupKindRelation =
        setOf(
            Pair(IogPluginConstants.RG_PREFIX, GroupKind.REGIONAL_GROUP),
            Pair(IogPluginConstants.AS_PREFIX, GroupKind.COMPETENCE_GROUP),
            Pair(IogPluginConstants.KG_PREFIX, GroupKind.COMPETENCE_GROUP),
            Pair(IogPluginConstants.PROGRAM_PREFIX, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC),
            Pair(IogPluginConstants.ANKER_PREFIX, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC),
        )

    val standardGroupNameGroupKindRelation =
        setOf(
            Pair(LdapNames.IOG_MEMBERS, GroupKind.LDAP_ONLY),
            Pair(LdapNames.INTERESTED_PEOPLE, GroupKind.LDAP_ONLY),
            Pair(LdapNames.TRIAL_MEMBERS, GroupKind.LDAP_ONLY),
            Pair(LdapNames.CENTRAL_OFFICE, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET),
            Pair(LdapNames.OMS, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET),
            Pair(LdapNames.ASSOCIATION_BOARD, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET),
            Pair(LdapNames.PROKO, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET),
            Pair(LdapNames.AP_SPRECHERINNEN, GroupKind.COLLABORATION),
        )

    val memberAdminInterestedPattern = MemberAdminInterestedPattern(credentials)
    val memberCrmPattern = MemberCrmInterestedPattern(credentials)
    val simpleAdminPattern = SimpleMemberAdminPattern(credentials)
    val simpleAdminPatternCRM = SimpleMemberAdminCRM(credentials)
    val memberAdminTrialPattern = MemberAdminTrialPattern(credentials)

    internal fun identifyNodePatternType(groupName: String): Pair<PatternTypes, GroupKind> {
        var prefixedName = groupName
        possibleSubGroupSuffixes.forEach { prefixedName = prefixedName.substringBefore(it) }
        val groupKind =
            standardGroupNameGroupKindRelation.firstOrNull { prefixedName.startsWith(it.first) }?.second
                ?: prefixGroupKindRelation.firstOrNull { prefixedName.startsWith(it.first) }?.second
        if (groupKind == null) {
            if (isProjectName(prefixedName)) {
                return Pair(PatternTypes.MemberAdminInterested, GroupKind.PROJECT)
            }
        }
        return Pair(
            groupKindPatternRelation.firstOrNull { it.first == groupKind }?.second
                ?: PatternTypes.Other,
            groupKind ?: GroupKind.LDAP_ONLY,
        )
    }

    private fun isProjectName(prefixedName: String): Boolean {
        if (prefixedName.indexOf("-") == 3) return true
        return false
    }

    fun createSubGroups(config: CreateGroupConfig): List<Group> {
        val prefixAndSuffix = groupPrefixAndSuffix(config.groupKind)
        val prefixedName = MemberAdminInterestedPattern.concatOrIgnore(config.ldapName, prefixAndSuffix)
        val pattern = groupKindPatternRelation.find { it.first == config.groupKind }!!.second

        when (pattern) {
            PatternTypes.SimpleMemberAdmin -> {
                return simpleAdminPattern.create(config.copy(ldapName = prefixedName))
            }
            PatternTypes.MemberCrmInterested -> {
                return memberCrmPattern.create(config.copy(ldapName = prefixedName))
            }
            PatternTypes.MemberAdminInterested -> {
                return memberAdminInterestedPattern.create(config.copy(ldapName = prefixedName))
            }
            PatternTypes.SimpleMemberAdminCRM -> {
                return simpleAdminPatternCRM.create(config.copy(ldapName = prefixedName))
            }
            PatternTypes.MemberAdminTrial -> {
                return memberAdminTrialPattern.create(config.copy(ldapName = prefixedName))
            }
            PatternTypes.Other ->
                return arrayOf(
                    credentials.createGroup(
                        Group(0, config.ldapName, config.description, emptySet(), emptySet(), emptySet(), emptySet(), emptySet(), emptySet()),
                    ),
                ).toList()
        }
    }

    fun searchSubGroups(
        ldapName: String,
        groupKind: GroupKind,
    ): List<Group> {
        val prefixAndSuffix = groupPrefixAndSuffix(groupKind)
        val prefixedName = MemberAdminInterestedPattern.concatOrIgnore(ldapName, prefixAndSuffix)
        val pattern: PatternTypes = groupKindPatternRelation.find { it.first == groupKind }!!.second

        when (pattern) {
            PatternTypes.SimpleMemberAdmin -> {
                return simpleAdminPattern.search(prefixedName)
            }
            PatternTypes.SimpleMemberAdminCRM -> {
                return simpleAdminPatternCRM.search(prefixedName)
            }
            PatternTypes.MemberCrmInterested -> {
                return memberCrmPattern.search(prefixedName)
            }
            PatternTypes.MemberAdminInterested -> {
                return memberAdminInterestedPattern.search(prefixedName)
            }
            PatternTypes.MemberAdminTrial -> {
                return memberAdminTrialPattern.search(prefixedName)
            }
            PatternTypes.Other ->
                throw Exception("LDAPGroupPatternSelector::searchSubGroups invalid mode")
        }
    }
}
