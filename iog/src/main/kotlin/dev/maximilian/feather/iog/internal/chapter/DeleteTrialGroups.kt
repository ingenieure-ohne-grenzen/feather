/*
 *    Copyright [2021-2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.openproject.IOpenProject
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.UUID

internal class DeleteTrialGroups(
    private val credentials: ICredentialProvider,
    private val op: IOpenProject,
    private val backgroundJobManager: BackgroundJobManager,
    private val nc: Nextcloud,
    private val onc: OPNameConfig,
) {
    companion object : KLogging()

    suspend fun delete(
        jobId: UUID,
        param: Boolean,
    ): TrialOperationResult {
        logger.warn { "DeleteTrial::delete remove all -trial groups " }
        backgroundJobManager.setJobStatus(jobId, "Remove all -trial groups")

        val nct = nc.findGroupFolder(NextcloudFolders.GroupShare) ?: throw Exception("Could not find IOG share in NC")
        val trialGroups = credentials.getGroups().filter { it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX) }
        val allNames = nc.findGroupFolder(NextcloudFolders.GroupShare)?.groups?.mapNotNull { it.key } ?: emptyList()
        trialGroups.forEach {
            credentials.deleteGroup(it)
        }
        runBlocking {
            op.getGroups()
                .filter { it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX) }
                .asFlow()
                .collect { op.deleteGroup(it) }
        }

        allNames.filter { it.endsWith(IogPluginConstants.TRIAL_SUFFIX) }
            .forEach { nc.removeGroupFromGroupFolder(nct, it) }

        logger.warn { "DeleteTrial::delete remove TrialMember group " }
        backgroundJobManager.setJobStatus(jobId, "Remove all TrialMember group")

        if (allNames.contains(LdapNames.TRIAL_MEMBERS)) {
            nc.removeGroupFromGroupFolder(nct, LdapNames.TRIAL_MEMBERS)
        }
        credentials.getGroupByName(LdapNames.TRIAL_MEMBERS)?.let { credentials.deleteGroup(it) }
        op.getGroupByName(onc.trialMemberGroupName)?.let { op.deleteGroup(it) }

        logger.warn { "DeleteTrial::delete finished." }
        backgroundJobManager.setJobStatus(jobId, "Finished.")
        return TrialOperationResult("OK", emptyList())
    }
}
