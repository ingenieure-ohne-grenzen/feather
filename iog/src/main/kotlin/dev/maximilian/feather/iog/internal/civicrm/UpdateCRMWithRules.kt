/*
 *    Copyright [2024] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.civicrm

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.CiviCRMDefaultActions
import dev.maximilian.feather.civicrm.CiviCRMDefaultConditions
import dev.maximilian.feather.civicrm.CiviCRMDefaultTrigger
import dev.maximilian.feather.civicrm.entities.ACL
import dev.maximilian.feather.civicrm.entities.ACLEntityRole
import dev.maximilian.feather.civicrm.internal.ICiviCRM
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.plausibility.OpenProjectPlausibility
import dev.maximilian.feather.iog.internal.groupPattern.SimpleMemberAdminCRM
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.*

class UpdateCRMWithRules(
    private val credentials: ICredentialProvider,
    private val backgroundJobManager: BackgroundJobManager,
    private val openProjectService: OpenProjectService,
    private val onc: OPNameConfig,
    private val nextcloudPublicURL: String,
    private val civicrm: ICiviCRM,
) {
    var groups: Collection<Group> = emptyList()

    companion object : KLogging()

    internal suspend fun resetAndUpdate(
        jobId: UUID,
        autoRepair: Boolean,
    ): CrmOperationResult {
        val errorMessages = mutableListOf<String>()
        var status = "NOK"
        logger.info { "UpdateCRMWithRules::resetAndUpdate fetching all groups " }
        backgroundJobManager.setJobStatus(jobId, "Fetching all users and groups from LDAP")

        val userAndGroups = credentials.getUsersAndGroups()
        groups = userAndGroups.second

        logger.info { "UpdateCRMWithRules::resetAndUpdate check preconditions" }
        backgroundJobManager.setJobStatus(jobId, "Check preconditions")

        val cachedResults = checkPreconditions()
        if (cachedResults.errorMessages.isEmpty()) {
            if (autoRepair) {
                backgroundJobManager.setJobStatus(jobId, "Reset and update groups")
                logger.warn { "UpdateCRMWithRules::resetAndUpdate clear civicrm" }
                deleteCiviOnly()
                logger.info { "UpdateCRMWithRules::resetAndUpdate double check that everything was removed " }
                val error = checkEverythingEmpty()
                if (error != "") {
                    errorMessages.add(error)
                    backgroundJobManager.setJobStatus(
                        jobId,
                        "Das CRM-Update wurde nicht erfolgreich durchgeführt. Einige Elemente konnten nicht gelöscht werden.",
                    )
                    status = "NOK"
                } else {
                    updateCRM(jobId)
                    backgroundJobManager.setJobStatus(jobId, "Das CRM-Update wurde erfolgreich durchgeführt.")
                    status = "OK"
                }
            } else {
                backgroundJobManager.setJobStatus(jobId, "Vorbedingungen für ein CRM-Update sind erfüllt.")
                status = "OK"
            }
            logger.warn {
                "UpdateCRMWithRules::createCrmGroups status: $status result: ${
                    errorMessages.joinToString(
                        ". ",
                    )
                }"
            }
        } else {
            errorMessages.addAll(cachedResults.errorMessages)
            backgroundJobManager.setJobStatus(
                jobId,
                "Das CRM-Update wurde nicht erfolgreich durchgeführt. Die Voraussetzungen sind nicht erfüllt.",
            )
            status = "NOK"
        }
        return CrmOperationResult(status, errorMessages)
    }

    private fun checkPreconditions(): CrmPreconditionResult {
        val errorMessages = mutableListOf<String>()

        runBlocking {
            val contactTypes = civicrm.getContactTypes()
            if (contactTypes.any { it.name == "IogMember" }) {
                errorMessages += "Es gibt den Kontakttyp IogMember. Der Name muss nun IogUser lauten."
            }
            if (contactTypes.any { it.name == "Sonstige_K_rperschaft" }) {
                errorMessages += "Es gibt den Kontakttyp Sonstige_K_rperschaft. Der Name muss nun DefaultOrganization lauten."
            }
        }

        if (credentials.getGroupByName(LdapNames.IOG_MEMBERS) == null) errorMessages += "Gruppe '${LdapNames.IOG_MEMBERS}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.INTERESTED_PEOPLE) == null) errorMessages += "Gruppe '${LdapNames.INTERESTED_PEOPLE}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.TRIAL_MEMBERS) == null) errorMessages += "Gruppe '${LdapNames.TRIAL_MEMBERS}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.ETHIC_VALIDATION_GROUP) == null) errorMessages += "Gruppe '${LdapNames.ETHIC_VALIDATION_GROUP}' existiert nicht"

        groups.filter {
            it.name.endsWith(IogPluginConstants.INTERESTED_SUFFIX) &&
                it.name.startsWith(IogPluginConstants.RG_PREFIX)
        }.forEach {
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.INTERESTED_SUFFIX)
            if (credentials.getGroupByName(groupNameWithoutPrefix) == null) {
                errorMessages += "Konnte Basisgruppe für $groupNameWithoutPrefix nicht findet. Gebildet aus ${it.name}"
            }
            if (credentials.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX) == null) {
                errorMessages += "Admin Gruppe <${groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX} existiert nicht!"
            }
            if (credentials.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX) == null) {
                errorMessages += "Trial Gruppe <${groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX} existiert nicht!"
            }
            if (credentials.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.CRM_SUFFIX) == null) {
                errorMessages += "CRM Gruppe <${groupNameWithoutPrefix + IogPluginConstants.CRM_SUFFIX} existiert nicht!"
            }
        }

        runBlocking {
            try {
                civicrm.findIndexOfACLRole()
                logger.info { "UpdateCRMWithRules::checkPreconditions ${CiviCRMConstants.ACL_ROLE} gefunden" }
            } catch (ex: Exception) {
                errorMessages += "${CiviCRMConstants.ACL_ROLE} nicht in CiviCRM definiert."
            }

            if (civicrm.getCiviRulesTriggers().find { it.name == CiviCRMDefaultTrigger.NEW_CONTACT_TRIGGER } == null) {
                logger.info { "UpdateCRMWithRules::checkPreconditions CiviRule Triggertyp new_contact in Civicrm nicht gefunden." }
                errorMessages += "CiviRule Triggertyp new_contact in Civicrm nicht gefunden."
            }
            if (civicrm.getCiviRuleActions().find { it.name == CiviCRMDefaultActions.ADD_CONTACT_TO_GROUP_ACTION } == null) {
                logger.info { "UpdateCRMWithRules::checkPreconditions CiviRule Aktion add_gontact_group in Civicrm nicht gefunden." }
                errorMessages += "CiviRule Aktion add_gontact_group in Civicrm nicht gefunden."
            }
            if (civicrm.getCiviRuleConditions().find { it.name == CiviCRMDefaultConditions.CREATED_BY_CONDITION } == null) {
                logger.info { "UpdateCRMWithRules::checkPreconditions CiviRule Condition contact_createdby in Civicrm nicht gefunden." }
                errorMessages += "CiviRule Condition add_gontact_group in Civicrm nicht gefunden."
            }
            if (civicrm.getCiviRuleConditions()
                    .find { it.name == CiviCRMDefaultConditions.CONTACT_HAS_SUBTYPE_CONDITION } == null
            ) {
                logger.info { "UpdateCRMWithRules::checkPreconditions CiviRule Condition contact_has_subtype in Civicrm nicht gefunden." }
                errorMessages += "CiviRule Condition contact_has_subtype in Civicrm nicht gefunden."
            }
        }
        return CrmPreconditionResult(errorMessages)
    }

    private fun updateCRM(
        jobId: UUID,
    ) {
        logger.info("UpdateCRMWithRules::updateCRM Start update of CRM - System")
        backgroundJobManager.setJobStatus(jobId, "Create civicrm groups ")
        logger.info("UpdateCRMWithRules::updateCRM Create standard groups ...")
        val createSchemeResult = CiviGroupScheme(credentials, civicrm).createStandardGroups()
        if (createSchemeResult.ethicGroupId == null) {
            logger.error("UpdateCRMWithRules::updateCRM Ethic group is null after creation ... STOP.")
        } else {
            runBlocking {
                val userAndGroups = credentials.getUsersAndGroups()
                val opc = OpenProjectPCreator(openProjectService.openproject, onc, jobId, backgroundJobManager)
                val opp =
                    OpenProjectPlausibility(openProjectService.openproject, credentials, opc, onc, userAndGroups.first)

                val contactAddedTriggerId =
                    civicrm.getCiviRulesTriggers().find { it.name == CiviCRMDefaultTrigger.NEW_CONTACT_TRIGGER }!!.id
                val addContactActionId =
                    civicrm.getCiviRuleActions()
                        .find { it.name == CiviCRMDefaultActions.ADD_CONTACT_TO_GROUP_ACTION }!!.id

                val allConditions = civicrm.getCiviRuleConditions()
                val contactCreatedByConditionId =
                    allConditions.find { it.name == CiviCRMDefaultConditions.CREATED_BY_CONDITION }!!.id
                val contactHasSubTypeConditionId =
                    allConditions.find { it.name == CiviCRMDefaultConditions.CONTACT_HAS_SUBTYPE_CONDITION }!!.id

                val generalRules = GeneralCRMRules(civicrm)
                generalRules.installGeneralCiviRulesDelete(createSchemeResult)
                generalRules.installGeneralCiviRulesAdd(createSchemeResult)

                backgroundJobManager.setJobStatus(jobId, "Create -crm group for each regional group")
                updateCRMforRGs(
                    opp,
                    groups,
                    createSchemeResult.userGroupId,
                    createSchemeResult.orgaContactsId,
                    createSchemeResult.personContactsId,
                    contactAddedTriggerId,
                    addContactActionId,
                    contactCreatedByConditionId,
                    contactHasSubTypeConditionId,
                )
                backgroundJobManager.setJobStatus(jobId, "Create -crm group for program bila")
                updateCRMforProgramGroup(
                    opp,
                    createSchemeResult.userGroupId,
                    createSchemeResult.orgaContactsId,
                    createSchemeResult.personContactsId,
                    contactAddedTriggerId,
                    addContactActionId,
                    contactCreatedByConditionId,
                    contactHasSubTypeConditionId,
                )
                GeneralCRMRules(civicrm).updateGeneralCrmRule(createSchemeResult.personContactsId)
                backgroundJobManager.setJobStatus(jobId, "Assign contacts to test group")
            }
        }
    }

    private fun updateCRMforRGs(
        opPlausibility: OpenProjectPlausibility,
        allGroups: Collection<Group>,
        userGroupId: Int,
        orgaContacts: Int,
        personContacts: Int,
        contactAddedTriggerId: Int,
        addContactActionId: Int,
        contactCreatedByConditionId: Int,
        contactHasSubTypeConditionId: Int,
    ) {
        logger.info("UpdateCRMWithRules::updateCRMforRGs")
        val groupScheme = CiviGroupScheme(credentials, civicrm)
        allGroups.filter {
            it.name.endsWith(IogPluginConstants.INTERESTED_SUFFIX) && it.name.startsWith(IogPluginConstants.RG_PREFIX)
        }.forEach {
            logger.info("UpdateCRMWithRules::updateCRMforRGs check group based on ${it.name}")
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.INTERESTED_SUFFIX)
            val opDescription = opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(it.name))
            val crmGroupName = groupNameWithoutPrefix.plus(IogPluginConstants.CRM_SUFFIX)
            val credentialGroup = credentials.getGroupByName(crmGroupName)
            logger.warn { ("UpdateCRMWithRules::updateCRMforRGs $crmGroupName does not exist") }

            credentialGroup?.let {
                groupScheme.createSingleCiviChapter(
                    credentialGroup,
                    userGroupId,
                    orgaContacts,
                    personContacts,
                    contactAddedTriggerId,
                    addContactActionId,
                    contactCreatedByConditionId,
                    contactHasSubTypeConditionId,
                    false,
                    opDescription,
                )
            }
        }
    }

    private fun updateCRMforProgramGroup(
        opPlausibility: OpenProjectPlausibility,
        userGroupId: Int,
        orgaContacts: Int,
        personContacts: Int,
        contactAddedTriggerId: Int,
        addContactActionId: Int,
        contactCreatedByConditionId: Int,
        contactHasSubTypeConditionId: Int,
    ) {
        logger.info { "UpdateCRMWithRules::updateCRMforProgramGroup" }
        val groupScheme = CiviGroupScheme(credentials, civicrm)

        groups.filter {
            it.name.startsWith(IogPluginConstants.PROGRAM_PREFIX) && it.name.endsWith(IogPluginConstants.PR_FR_SUFFIX)
        }.forEach {
            logger.info { "UpdateCRMWithRules::updateCRMforProgramGroup Create CRM for ${it.name}" }
            val pat = SimpleMemberAdminCRM(credentials)

            val opDescription = opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(it.name))

            val credentialGroup =
                pat.addCrmGroupIfNecessary(
                    CreateGroupConfig(
                        it.name,
                        opDescription,
                        emptySet(),
                        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC,
                        nextcloudPublicURL,
                    ),
                )
            groupScheme.createSingleCiviChapter(
                credentialGroup,
                userGroupId,
                orgaContacts,
                personContacts,
                contactAddedTriggerId,
                addContactActionId,
                contactCreatedByConditionId,
                contactHasSubTypeConditionId,
                false,
                opDescription,
            )
        }
    }

    private suspend fun checkEverythingEmpty(): String {
        val acls = civicrm.getACLs()
        val aclroles = civicrm.getACLEntityRoles()
        var errorString = ""
        if (acls.any { it.name != "Edit All Contacts" && it.name != "Core ACL" }) {
            errorString += "Es gibt noch ACLs."
        }

        if (aclroles.any()) {
            errorString += "Es gibt noch ACl Roles."
        }
        val contactTypes = civicrm.getContactTypes()
        if (contactTypes.count() > 13) {
            errorString += "Es gibt zuviele Kontakttypen."
        }
        if (contactTypes.any { it.name == "IogMember" }) {
            errorString += "Es gibt den Kontakttyp IogMember. Der Name muss nun IogUser lauten."
        }
        return errorString
    }

    private suspend fun deleteCiviOnly() {
        val civiGroups = civicrm.getGroups()
        val acls = civicrm.getACLs()
        val aclroles = civicrm.getACLEntityRoles()
        val aclRoleIndex = civicrm.getOptionGroup(CiviCRMConstants.ACL_ROLE)?.id

        civiGroups.firstOrNull { it.name == LdapNames.ETHIC_VALIDATION_GROUP }?.let { ethicGroup ->
            acls.filter { it.entityId == ethicGroup.id }.forEach {
                logger.info { "UpdateCRMWithRules::deleteCiviOnly delete acl: ${it.name} " }
                civicrm.deleteACL(it.id)
            }
            aclroles.filter {
                it.entityId == ethicGroup.id
            }.forEach { r ->
                civicrm.deleteACLEntityRole(r.id)
            }
        }

        acls.filter { it.name != "Edit All Contacts" && it.name != "Core ACL" }.forEach {
            logger.info { "UpdateCRMWithRules::deleteCiviOnly delete acl: ${it.name} " }
            civicrm.deleteACL(it.id)
        }

        deleteDependenciesOfGroupOnly(civicrm, IogPluginConstants.RG_PREFIX, civiGroups, acls, aclroles)
        deleteDependenciesOfGroupOnly(civicrm, IogPluginConstants.PROGRAM_PREFIX, civiGroups, acls, aclroles)
        deleteDependenciesOfGroupOnly(civicrm, CiviCRMNames.USER_GROUP_NAME, civiGroups, acls, aclroles)
        deleteDependenciesOfGroupOnly(civicrm, CiviCRMNames.CONTACT_GROUP_NAME, civiGroups, acls, aclroles)
        deleteDependenciesOfGroupOnly(civicrm, LdapNames.CENTRAL_OFFICE, civiGroups, acls, aclroles)

        aclRoleIndex?.let { aclRoleIndexT ->
            civicrm.getOptionValueByOptionGroupId(aclRoleIndexT).filter {
                it.name?.contains("Admin") == false && it.name?.contains("Auth") == false
            }.forEach {
                civicrm.deleteOptionValue(it.id)
            }
        }

        aclroles.forEach {
            civicrm.deleteACLEntityRole(it.id)
        }
        civicrm.getCiviRulesRuleConditions().forEach {
            civicrm.deleteCiviRulesRuleCondition(it.id)
        }
        civicrm.getCiviRulesRuleActions().forEach {
            civicrm.deleteCiviRulesRuleAction(it.id)
        }
        civicrm.getCiviRulesRules().forEach {
            logger.info { "UpdateCRMWithRules::deleteCiviOnly delete rule ${it.name} " }
            civicrm.deleteCiviRulesRule(it.id)
        }
    }

    private suspend fun deleteDependenciesOfGroupOnly(
        civicrm: ICiviCRM,
        prefix: String,
        civiGroups: List<dev.maximilian.feather.civicrm.entities.Group>,
        acls: List<ACL>,
        aclroles: List<ACLEntityRole>,
    ) {
        civiGroups.filter { it.name.startsWith(prefix) }.forEach { g ->
            acls.filter { it.entityId == g.id }.forEach { civicrm.deleteACL(it.id) }
            aclroles.filter { it.entityId == g.id }.forEach { civicrm.deleteACLEntityRole(it.id) }
        }
    }
}
