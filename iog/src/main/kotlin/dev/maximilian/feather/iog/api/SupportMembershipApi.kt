/*
*    Copyright [2020-2021] Feather development team, see AUTHORS.md
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package dev.maximilian.feather.iog.api

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.bindings.SupportMemberRegistrationRequest
import dev.maximilian.feather.iog.api.bindings.TrialDecision
import dev.maximilian.feather.iog.api.bindings.TrialRequestResponse
import dev.maximilian.feather.iog.api.bindings.UpdateDatabaseRequest
import dev.maximilian.feather.iog.internal.memberCheck.MemberCheck
import dev.maximilian.feather.iog.internal.tools.DatabaseValidation
import dev.maximilian.feather.iog.internal.trials.TrialProtocol
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.requireAdminPermission
import dev.maximilian.feather.requirePermission
import dev.maximilian.feather.session
import dev.maximilian.feather.sessionOrMinisession
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.bodyAsClass
import io.javalin.openapi.HttpMethod
import io.javalin.openapi.OpenApi
import io.javalin.openapi.OpenApiContent
import io.javalin.openapi.OpenApiRequestBody
import io.javalin.openapi.OpenApiResponse
import mu.KotlinLogging
import org.eclipse.jetty.http.HttpStatus
import java.time.format.DateTimeFormatter

internal class SupportMembershipApi(
    app: Javalin,
    private val trialProtocol: TrialProtocol,
    private val memberCheck: MemberCheck,
    private val credentialProvider: ICredentialProvider,
) {
    init {
        app.routes {
            path("bindings/iog") {
                get("/users/verify", ::handleSupportMemberVerification)
                get("/users/registered", ::handleGetAllRegisteredSupportMember, Permission.ADMIN)
                get("/users/hash", ::handleGetDatabaseSupportMember, Permission.ADMIN)
                get("/users/simulate", ::handleSimulate, Permission.ADMIN)
                post("/users/registerByMail", ::handleRegisterByMail)
                post("/users/registerByBirthdate", ::handleSupportMemberRegistrationByNameAndBirthdate)
                post("/users/downgrade", ::handleDowngradeUserToInterested)
                post("/users/upgradeByMail", ::handleUpgradeToSupportMemberByMail)
                post("/users/upgradeByBirthdate", ::handleUpgradeToSupportMemberByNameBirthdate)
                post(
                    "/users/updateDB",
                    ::handleUpdateSupportMemberDatabase,
                    Permission.UPLOAD_SUPPORT_MEMBER,
                    Permission.ADMIN,
                )
                get("/users/dbLastUpdate", ::getLastDbUpdate)
                post("/users/requestTrial", ::requestTrial)
                get("/users/getAllTrialRequests", ::getAllTrialRequests)
                post("/users/processTrialRequest", ::processTrialRequest)
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    @OpenApi(
        summary = "Check if a name birthday hash registration process is required.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [
            OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403"),
        ],
        path = "/v1/bindings/iog/users/verify",
        methods = [HttpMethod.GET],
    )
    private fun handleSupportMemberVerification(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        val result = memberCheck.getRegistrationState(session.user)
        if (MemberCheck.userIsBlockedBySM(result)) {
            logger.info { "User ${session.user.displayName} blocked by support membership check: $result" }
            ctx.status(HttpStatus.FORBIDDEN_403)
        } else if (MemberCheck.userIsBlockedByTrial(result)) {
            logger.info { "User ${session.user.displayName} blocked by trial: $result" }
            ctx.status(HttpStatus.FORBIDDEN_403)
        } else if (MemberCheck.userCanRequestTrial(result)) {
            logger.info { "User ${session.user.displayName} can request trial: $result" }
            ctx.status(HttpStatus.OK_200)
        } else if (MemberCheck.userCanUpgrade(result)) {
            logger.info { "User ${session.user.displayName} can upgrade to support member: $result" }
            ctx.status(HttpStatus.OK_200)
        } else {
            logger.info { "User accepted: $result" }
            ctx.status(HttpStatus.OK_200)
        }
        val lastDbUpdate = memberCheck.getLastDatabaseUpdate()
        if (lastDbUpdate != null) {
            ctx.header("Last-Modified", DateTimeFormatter.RFC_1123_DATE_TIME.format(lastDbUpdate))
        }
        ctx.json(result.toString())
    }

    @OpenApi(
        summary = "Register a support member by name and birthdate hash.",
        requestBody = OpenApiRequestBody([OpenApiContent(SupportMemberRegistrationRequest::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/registerByBirthdate",
        methods = [HttpMethod.POST],
    )
    private fun handleSupportMemberRegistrationByNameAndBirthdate(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        val body: SupportMemberRegistrationRequest
        try {
            body = ctx.bodyAsClass<SupportMemberRegistrationRequest>()
        } catch (e: Exception) {
            logger.warn { "SupportMembershipApi::handleSupportMemberRegistrationByNameAndBirthdate() malformed entity" }
            throw BadRequestResponse()
        }

        val result = memberCheck.registerByNameAndBirthdate(body.nameBirthdayHash, session.user.id)
        if (result) {
            logger.info { "User ${session.user.displayName} registered as support member by name and birthday." }
            ctx.status(HttpStatus.OK_200)
        } else {
            logger.info { "User ${session.user.displayName} could not be registered." }
            ctx.status(HttpStatus.FORBIDDEN_403)
        }
    }

    @OpenApi(
        summary = "Downgrade a person from *-member groupto *-interested group and remove from all special groups.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/downgrade",
        methods = [HttpMethod.POST],
    )
    private fun handleDowngradeUserToInterested(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        memberCheck.downgrade(session.user.id)
        logger.info { "User ${session.user.displayName} downgraded to interested." }
        ctx.status(HttpStatus.OK_200)
    }

    @OpenApi(
        summary = "Try to register an support member by mail hash.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/registerByMail",
        methods = [HttpMethod.POST],
    )
    private fun handleRegisterByMail(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        val result = memberCheck.registerByMail(session.user)
        if (result) {
            logger.info { "User ${session.user.displayName} successful registered as support member by mail." }
            ctx.status(HttpStatus.OK_200)
        } else {
            logger.info { "User ${session.user.displayName} could not register support member by mail." }
            ctx.status(HttpStatus.FORBIDDEN_403)
        }
    }

    @OpenApi(
        summary = "Try to register an interested member as support member by mail hash. Upgrade *-interested group to *-member group after success. ",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("204"), OpenApiResponse("205"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/upgradeByMail",
        methods = [HttpMethod.POST],
    )
    private fun handleUpgradeToSupportMemberByMail(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        if (memberCheck.smr.getExternalIDbyLDAP(session.user.id) != null) {
            logger.info { "User ${session.user.displayName} was already registered as support member." }
            ctx.status(HttpStatus.NO_CONTENT_204)
        } else {
            val result = memberCheck.checkAndUpgradeSM(session.user.id)
            if (result) {
                logger.info { "User ${session.user.displayName} successful upgrade to support member by mail." }
                ctx.status(HttpStatus.CREATED_201)
            } else {
                logger.info { "User ${session.user.displayName} could not upgrade to support member by mail." }
                ctx.status(HttpStatus.FORBIDDEN_403)
            }
        }
    }

    @OpenApi(
        summary = "Try to register an interested member as support member by name and birthday hash. Upgrade *-interested group to *-member group after success. ",
        requestBody = OpenApiRequestBody([OpenApiContent(SupportMemberRegistrationRequest::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/upgradeByBirthdate",
        methods = [HttpMethod.POST],
    )
    private fun handleUpgradeToSupportMemberByNameBirthdate(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val body: SupportMemberRegistrationRequest
        try {
            body = ctx.bodyAsClass<SupportMemberRegistrationRequest>()
        } catch (e: Exception) {
            logger.warn { "SupportMembershipApi::handleUpgradeToSupportMemberByNameBirthdate() malformed entity" }
            throw BadRequestResponse()
        }

        if (memberCheck.smr.getExternalIDbyLDAP(session.user.id) != null) {
            logger.info { "User ${session.user.displayName} was already registered as support member." }
            ctx.status(HttpStatus.NO_CONTENT_204)
        } else {
            val result = memberCheck.registerByNameAndBirthdate(body.nameBirthdayHash, session.user.id)

            if (result) {
                memberCheck.upgrade(session.user.id)
                logger.info { "User ${session.user.displayName} successful upgrade to support member by name and birthdate." }
                ctx.status(HttpStatus.CREATED_201)
            } else {
                logger.info { "User ${session.user.displayName} could not upgrade to support member by name and birthdate." }
                ctx.status(HttpStatus.FORBIDDEN_403)
            }

            ctx.json(result)
        }
    }

    @OpenApi(
        summary = "Upload support member database. Needs ADMIN or UPLOAD_SUPPORT_MEMBER permission. At least 5 unique support members required.",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["UPLOAD_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/updateDB",
        methods = [HttpMethod.POST],
    )
    private fun handleUpdateSupportMemberDatabase(ctx: Context) {
        val session = ctx.session()

        logger.info { "SupportMembershipApi::handleUpdateSupportMemberDatabase()" }

        try {
            session.user.requirePermission("updateSupportMemberDatabase", setOf(Permission.ADMIN, Permission.UPLOAD_SUPPORT_MEMBER))
        } catch (ex: Exception) {
            logger.warn { "SupportMembershipApi::handleUpdateSupportMemberDatabase() Permission denied. updateSupportMemberDatabase required" }
            throw ForbiddenResponse()
        }

        val body: UpdateDatabaseRequest
        try {
            body = ctx.bodyAsClass<UpdateDatabaseRequest>()
        } catch (e: Exception) {
            logger.warn { "SupportMembershipApi::handleUpdateSupportMemberDatabase() malformed entity" }
            throw BadRequestResponse()
        }

        memberCheck.databaseValidation.minSupportMemberCount = 5
        val result = memberCheck.databaseValidation.validate(body.hashEntries)

        if (result == DatabaseValidation.ValidationResult.OK) {
            memberCheck.updateDatabase(body.hashEntries)
            logger.info {
                "SupportMembershipApi::handleUpdateSupportMemberDatabase Database successfully updated with ${body.hashEntries.count()} entries"
            }
            ctx.status(HttpStatus.OK_200)
        } else {
            val errorMessages = mutableListOf<String>()
            val reason =
                when (result) {
                    DatabaseValidation.ValidationResult.NOT_ENOUGH_SUPPORT_MEMBER -> "Not enough support members in database. Minimum is ${memberCheck.databaseValidation.minSupportMemberCount}. Upload rejected."
                    DatabaseValidation.ValidationResult.EXTERNAL_ID_NOT_UNIQUE -> "The external IDs are not unique."
                    DatabaseValidation.ValidationResult.MAIL_HASH_SIZE_INVALID -> "The email hash size is not 64 signs."
                    DatabaseValidation.ValidationResult.NOT_ENOUGH_UNIQUE_MAIL_ADDRESS -> "The database does not contain at least ${memberCheck.databaseValidation.minSupportMemberCount} unique mail addresses."
                    DatabaseValidation.ValidationResult.NAME_HASH_SIZE_INVALID -> "Size of a name birthday hash is not 64 signs"
                    DatabaseValidation.ValidationResult.NAME_HASH_EMPTY -> "Name hash is hash of an empty string"
                    else -> "unknown database validation result $result"
                }

            errorMessages += reason
            logger.warn { "SupportMembershipApi::handleUpdateSupportMemberDatabase $reason. Upload REJECTED." }
            ctx.status(HttpStatus.BAD_REQUEST_400)
            ctx.json(errorMessages)
        }
    }

    @OpenApi(
        summary = "Get last update time of the support member database.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200")],
        path = "/v1/bindings/iog/users/dbLastUpdate",
        methods = [HttpMethod.GET],
    )
    private fun getLastDbUpdate(ctx: Context) {
        val lastDbUpdate = memberCheck.getLastDatabaseUpdate()
        if (lastDbUpdate == null) {
            ctx.status(204)
        } else {
            ctx.status(200).result(DateTimeFormatter.ISO_DATE_TIME.format(lastDbUpdate))
        }
    }

    @OpenApi(
        summary = "Get all registered support member.",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["DUMP_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/registered",
        methods = [HttpMethod.GET],
    )
    private fun handleGetAllRegisteredSupportMember(ctx: Context) {
        val session = ctx.session()

        try {
            session.user.requireAdminPermission("getAllRegisteredSupportMember")
        } catch (ex: Exception) {
            logger.warn { "SupportMembershipApi::handleGetAllRegisteredSupportMember() permission denied. getAllRegisteredSupportMember required." }
            throw ForbiddenResponse()
        }
        val allUser = memberCheck.getAllRegisteredSupportMember()
        val lastDbUpdate = memberCheck.getLastDatabaseUpdate()
        logger.info { "Dumped ${allUser.count()} entries" }
        ctx.status(HttpStatus.OK_200)
        if (lastDbUpdate != null) {
            ctx.header("Last-Modified", DateTimeFormatter.RFC_1123_DATE_TIME.format(lastDbUpdate))
        }
        ctx.json(allUser)
    }

    @OpenApi(
        summary = "Get all support members of fundraising box database.",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["DUMP_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/hash",
        methods = [HttpMethod.GET],
    )
    private fun handleGetDatabaseSupportMember(ctx: Context) {
        val session = ctx.session()
        try {
            session.user.requireAdminPermission("getDatabaseSupportMember")
        } catch (ex: Exception) {
            logger.warn { "SupportMembershipApi::handleGetDatabaseSupportMember() permission denied. getDatabaseSupportMember required." }
            throw ForbiddenResponse()
        }

        val allUser = memberCheck.getAllSupportMembersOfFundraisingBox()
        logger.info { "Dumped ${allUser.count()} entries" }
        ctx.status(HttpStatus.OK_200)
        ctx.json(allUser)
    }

    @OpenApi(
        summary = "Get all users that could be registered by mail",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["DUMP_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/simulate",
        methods = [HttpMethod.GET],
    )
    private fun handleSimulate(ctx: Context) {
        val session = ctx.session()

        try {
            session.user.requireAdminPermission("simulateSupportMember")
        } catch (ex: Exception) {
            logger.warn { "SupportMembershipApi::handleSimulate() permission denied. simulateSupportMember required." }
            throw ForbiddenResponse()
        }

        val allUser = memberCheck.getSimulation()
        logger.info { "Dumped ${allUser.count()} entries" }
        ctx.status(HttpStatus.OK_200)
        ctx.json(allUser)
    }

    @OpenApi(
        summary = "Request a trial membership for an interested member.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("204"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/requestTrial",
        methods = [HttpMethod.POST],
    )
    private fun requestTrial(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        when (val supportMemberState = memberCheck.getRegistrationState(session.user)) {
            MemberCheck.RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED, MemberCheck.RegistrationState.TRIAL_REQUIRED, MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, MemberCheck.RegistrationState.INTERESTED_SM_UPGRADE_CAN_BE_OFFERED, MemberCheck.RegistrationState.INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED -> {
                if (trialProtocol.requestTrial(session.user.id)) {
                    logger.info { "User ${session.user.displayName} successfully requested trial." }
                } else {
                    logger.info { "User ${session.user.displayName} could not request trial." }
                }
                ctx.status(HttpStatus.NO_CONTENT_204)
            }
            MemberCheck.RegistrationState.INTERESTED_TRIAL_UPGRADE_POSSIBLE -> {
                trialProtocol.upgradeTrial(session.user.id)
                logger.info { "User ${session.user.displayName} already in trial database. Accept automatically" }
                ctx.status(HttpStatus.NO_CONTENT_204)
            }
            else -> {
                logger.info { "User ${session.user.displayName} could not upgrade to trial member. State is $supportMemberState" }
                ctx.status(HttpStatus.FORBIDDEN_403)
            }
        }
    }

    @OpenApi(
        summary = "Reject a trial request of an interested member.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("204"), OpenApiResponse("400"), OpenApiResponse("403"), OpenApiResponse("404")],
        path = "/v1/bindings/iog/users/processTrialRequest",
        methods = [HttpMethod.POST],
    )
    private fun processTrialRequest(ctx: Context) {
        val session = ctx.session()
        val errorMessages = mutableListOf<String>()

        val body: TrialDecision
        try {
            body = ctx.bodyAsClass<TrialDecision>()
        } catch (e: Exception) {
            logger.warn { "SupportMembershipApi::processTrialRequest() malformed entity" }
            throw BadRequestResponse()
        }

        val users = mutableListOf<User>()
        var allFound = true
        body.requestId.forEach {
            val requestUser = credentialProvider.getUser(it)
            if (requestUser == null) {
                errorMessages.add("User not found ${body.requestId}.")
                logger.warn { "User not found ${body.requestId}." }
                allFound = false
            } else {
                users.add(requestUser)
            }
        }
        if (!allFound) {
            ctx.status(HttpStatus.NOT_FOUND_404)
            ctx.json(errorMessages)
        } else {
            var allAllowed = true
            val nodeAdminBaseGroupNames =
                trialProtocol.getNodeAdminGroups(
                    session.user,
                ).map { it.removeSuffix(IogPluginConstants.ADMIN_SUFFIX) }
            if (session.user.permissions.contains(Permission.ADMIN)) {
                allAllowed = true
            } else {
                users.forEach { trialCandidate ->
                    val isNodeAdmin =
                        trialCandidate.groups.any { rGroup ->
                            nodeAdminBaseGroupNames.any {
                                credentialProvider.getGroup(rGroup)?.name?.contains(it) == true
                            }
                        }

                    if (!isNodeAdmin) {
                        errorMessages.add(
                            "No permission to process user ${body.requestId} with name ${trialCandidate.displayName} by ${session.user.displayName}.",
                        )
                        logger.warn { "No permission to process user ${body.requestId} with name ${trialCandidate.displayName}  by ${session.user.displayName}." }
                        allAllowed = false
                    }
                }
            }

            if (allAllowed) {
                if (body.accept) {
                    users.forEach { requestUser ->
                        trialProtocol.acceptTrialRequest(requestUser.id)
                        logger.info { "Node admin ${session.user.displayName} successful upgraded trial membership of ${requestUser.displayName}." }
                    }
                } else {
                    users.forEach { requestUser ->
                        trialProtocol.rejectTrialRequest(requestUser.id)
                        logger.info { "Node admin ${session.user.displayName} successful rejected trial of ${requestUser.displayName}." }
                    }
                }
                ctx.status(HttpStatus.NO_CONTENT_204)
            } else {
                ctx.status(HttpStatus.FORBIDDEN_403)
                ctx.json(errorMessages)
            }
        }
    }

    @OpenApi(
        summary = "Get all trial requests",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/iog/users/getAllTrialRequests",
        methods = [HttpMethod.GET],
    )
    private fun getAllTrialRequests(ctx: Context) {
        val session = ctx.session()

        val nodeAdminBaseGroupNames =
            trialProtocol.getNodeAdminGroups(
                session.user,
            ).map { it.removeSuffix(IogPluginConstants.ADMIN_SUFFIX) }
        val isAdmin = session.user.permissions.contains(Permission.ADMIN)
        if (nodeAdminBaseGroupNames.isNotEmpty() || isAdmin) {
            val response = TrialRequestResponse(trialProtocol.getAllRequests(nodeAdminBaseGroupNames, isAdmin), isAdmin)
            logger.info { "Get all open trial requests (${response.openRequests.count()}) admin = $isAdmin" }
            ctx.status(HttpStatus.OK_200)
            ctx.json(response)
        } else {
            logger.warn { "SupportMembershipApi::getAllTrialRequests() permission denied. Admin or node admin required." }
            ctx.status(HttpStatus.FORBIDDEN_403)
        }
    }
}
