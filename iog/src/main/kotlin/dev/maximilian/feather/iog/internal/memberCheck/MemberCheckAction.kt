package dev.maximilian.feather.iog.internal.memberCheck

import dev.maximilian.feather.action.Action

internal data class MemberCheckAction(
    override val userId: Int,
    override val payload: MemberCheck.RegistrationState,
) : Action<MemberCheck.RegistrationState> {
    companion object {
        // has to match the router endpoint in the frontend where the user can act on this action
        const val NAME: String = "membership"
    }

    override val name: String = NAME

    override fun payloadToString(): String = payload.toString()

    override val description: String = "Fördermitgliedschaft überprüfen ($payload)"
}
