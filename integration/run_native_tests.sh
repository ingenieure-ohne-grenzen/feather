#!/bin/bash

LOG_LEVEL= #--info

function run_all_tests {
    ./gradlew $LOG_LEVEL cleanTest $1
}

function run_tests {
    ./gradlew $LOG_LEVEL cleanTest $1 --tests $2
}

TEST_ROOT=dev.maximilian.feather

run_all_tests :core:test

#run_all_tests :kubernetes:test # need a test namespace in a running Kubernetes instance
