#!/bin/sh
#
#    Copyright [2023] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

KEYCLOAK_IDP_CLIENT_UUID="$1"

/opt/keycloak/bin/kcadm.sh create components -r feather-local-test \
    -s name=openproject-admin \
    -s providerId=user-attribute-ldap-mapper \
    -s providerType=org.keycloak.storage.ldap.mappers.LDAPStorageMapper \
    -s parentId="$KEYCLOAK_IDP_CLIENT_UUID" \
    -s 'config."user.model.attribute"=["opAdmin"]' \
    -s 'config."ldap.attribute"=["opAdmin"]' \
    -s 'config."read.only"=["true"]' \
    -s 'config."always.read.value.from.ldap"=["true"]' \
    -s 'config."is.mandatory.in.ldap"=["false"]' \
    -s 'config."attribute.default.value"=["false"]' \
    -s 'config."attribute.force.default"=["true"]' \
    -s 'config."is.binary.attribute"=["false"]'

PARENT_ID=$(/opt/keycloak/bin/kcadm.sh create -r feather-local-test -i clients -s clientId=openproject -s "redirectUris=[\"http://localhost:16016/*\"]")
/opt/keycloak/bin/kcadm.sh create -r feather-local-test "clients/$PARENT_ID/client-secret"
CLIENT_SECRET=$(/opt/keycloak/bin/kcadm.sh get -r feather-local-test "clients/$PARENT_ID/client-secret" --fields value --format csv --noquotes)

/opt/keycloak/bin/kcadm.sh create -r feather-local-test "clients/$PARENT_ID/protocol-mappers/models" \
  -s name=openproject-admin \
  -s protocol=openid-connect \
  -s protocolMapper=oidc-usermodel-attribute-mapper \
  -s 'config."user.attribute"="opAdmin"' \
  -s 'config."claim.name"="admin"' \
  -s 'config."userinfo.token.claim"="true"' \
  -s 'config."id.token.claim"="true"' \
  -s 'config."access.token.claim"="true"' \
  -s 'config."jsonType.label"="boolean"'

echo "$CLIENT_SECRET"
