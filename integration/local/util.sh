#!/bin/sh

service_ready() {
  set -e

  SERVICE_FRIENDLY_NAME=$1
  TIME_STEP=$2
  TIME_MAXIMAL=$3
  TIME_WAITED=0
  CHECK_FUNCTION=$4

  echo "Waiting for $SERVICE_FRIENDLY_NAME to be up and running"

  # Initial sleep
  sleep 2

  until sh -c "$CHECK_FUNCTION" || false;
  do
    if [ $TIME_WAITED -gt "$TIME_MAXIMAL" ]; then
      echo ""
      echo "Timed out waiting for $SERVICE_FRIENDLY_NAME to become ready."
      return 1
    fi

    printf "."
    TIME_WAITED=$((TIME_WAITED+TIME_STEP))
    sleep "$TIME_STEP"
  done

  set -e
  echo "$SERVICE_FRIENDLY_NAME ready"
}
