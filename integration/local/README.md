# Local development setup

This scripts help you to setup a clean local environment. Feather currently depends on 3 services to operate normally. A PostgreSQL database, a Redis server and a LDAP server. The OpenProject and Nextcloud integrations were optional.

To test the [keycloak-plugin](https://gitlab.com/ingenieure-ohne-grenzen/keycloak-plugin) and its session handling, also a Keykloak server hosting this plugin is needed.

## Prerequirements

You need to have Docker and the Docker compose plugin (docker-compose-v2) installed. You need also a BASH shell.

## Setup

Run `./local_up.sh` to setup the dependent services of Feather. After that the script prints the `config.js` script you can use in the frontend for local development and the `DATABASE_URL` environment variable for the backend.

## Options for the shell script

+ `--no-frontend` Do not start the frontend
+ `--no-backend` Do not start the backend
+ `--keycloak[=<version>|current]` Enable the Keycloak service (including our plugin, the default version is maintained by renovate)
+ `--iog` Enable the Support-Membership-Check

When you choose `--keycloak=current`, the script assumes that your local development artefact for the Keycloak plugin was copied to the local folder at `./feather-keycloak-plugin.jar`.

See  [keycloak-plugin README](https://gitlab.com/ingenieure-ohne-grenzen/keycloak-plugin/-/blob/main/README.md) for a description how such a local development setup works.

### Limitations

If you enable keycloak it's (currently) mandatory to disable the backend start with this script.
