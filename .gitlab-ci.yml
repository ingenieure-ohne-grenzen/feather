cache: &global_cache
  policy: pull-push
  paths:
    - .gradleHome/caches
    - .gradleHome/notifications
    - .gradleHome/wrapper

variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false -Dorg.gradle.console=plain"
  DOCKER_DRIVER: overlay2
  FF_USE_FASTZIP: "true"
  ARTIFACT_COMPRESSION_LEVEL: fast
  CACHE_COMPRESSION_LEVEL: fast
  TRANSFER_METER_FREQUENCY: 5s
  # resource management (all at the limit offered by our Gitlab Runner instance
  #                      execpt for KUBERNETES_MEMORY_LIMIT)
  KUBERNETES_CPU_REQUEST: "1"
  KUBERNETES_CPU_LIMIT: "2"
  KUBERNETES_MEMORY_REQUEST: "2Gi"
  KUBERNETES_MEMORY_LIMIT: "4Gi" # 8 Gi would be allowed
  KUBERNETES_SERVICE_CPU_REQUEST: "500m"
  KUBERNETES_SERVICE_CPU_LIMIT: "750m"
  KUBERNETES_SERVICE_MEMORY_REQUEST: "1Gi"
  KUBERNETES_SERVICE_MEMORY_LIMIT: "2304Mi" # needed for OpenProject v14.0.0 and later
  # leave empty to avoid additional logging or set to
  # '--info' or higher for more logging (will flood the CI logs,
  # e.g. Multiservice/IoG log will spill over and be abridged)
  TEST_LOG_LEVEL: "" # "--info"
  # set empty to avoid parallel building & testing
  # (anyway only applies where suitable)
  DO_PARALLEL: "--parallel"

stages:
  - build
  - test1
  - test2
  - package
  - deploy

build:gradle:
  image: eclipse-temurin:17.0.12_7-jdk-alpine
  stage: build
  dependencies: []
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk add --no-cache git
    # Workaround for https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29022
    - git config --global --add safe.directory $(pwd)
  script:
    - ./gradlew clean classes testClasses installDist
  artifacts:
    expire_in: 2hrs
    paths:
      - .gradle
      - "**/build/classes/"
      - "**/build/resources/"
      - "**/build/install/"
      - "**/build/kotlin/"
      - "**/build/libs/"

test:ktlint:
  image: eclipse-temurin:17.0.12_7-jre-alpine
  stage: build
  dependencies: []
  cache: {}
  needs: []
  before_script:
    - apk add --no-cache curl
    # renovate: datasource=github-releases depName=pinterest/ktlint
    - export KTLINT_VERSION=1.3.1
    - curl --fail -sSL --output /usr/local/bin/ktlint "https://github.com/pinterest/ktlint/releases/download/$KTLINT_VERSION/ktlint"
    - chmod +x /usr/local/bin/ktlint
  script:
    - ktlint

test:hadolint:
  image: hadolint/hadolint:latest-alpine
  stage: test1
  allow_failure: true
  dependencies: []
  needs: []
  cache: {}
  script:
    - hadolint --ignore DL3018 Dockerfile

test:openldap:
  image: eclipse-temurin:17.0.12_7-jdk-alpine
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    LDAP_HOST: "openldap"
    LDAP_PORT: "1389"
    LDAP_ORGANISATION: "TEST"
    LDAP_DOMAIN: "example.org"
    LDAP_ADMIN_PASSWORD: "admin"
    LDAP_CONFIGURE_PPOLICY: "yes"
    LDAP_PPOLICY_USE_LOCKOUT: "yes"
  services:
    - name: registry.gitlab.com/ingenieure-ohne-grenzen/container_images/openldap-iog:OpenLDAP_2.6
      alias: openldap
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk add --no-cache git
    # Workaround for https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29022
    - git config --global --add safe.directory $(pwd)
  script:
    - ./gradlew $TEST_LOG_LEVEL :authorization:check
  artifacts:
    expire_in: 30days
    when: always
    reports:
      junit:
        - "**/build/test-results/test/**/TEST-*.xml"

test:nextcloud:
  image: eclipse-temurin:17.0.12_7-jdk-alpine
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    NEXTCLOUD_ADMIN_USER: "ncadmin" # for Nextcloud
    NEXTCLOUD_ADMIN_PASSWORD: "ncadminsecret"
    NEXTCLOUD_TRUSTED_DOMAINS: "localhost nextcloud"
    SQLITE_DATABASE: nctest # to enable auto installation with SQLite
    NEXTCLOUD_BASE_URL: "http://nextcloud" # for Feather
    NEXTCLOUD_USER: "ncadmin"
    NEXTCLOUD_PWD: "ncadminsecret"
  services:
    - name: nextcloud:30.0.2
      alias: nextcloud
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk add --no-cache git
    # Workaround for https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29022
    - git config --global --add safe.directory $(pwd)
  script:
    # Wait for Nextcloud to be up, ready and running
    - apk upgrade --no-cache
    - apk add --no-cache curl bash
    - integration/nextcloud_tests/wait_ready.sh
    - integration/nextcloud_tests/install_nc_apps.sh
    - ./gradlew $TEST_LOG_LEVEL :nextcloud:check
  artifacts:
    expire_in: 30days
    when: always
    reports:
      junit:
        - "**/build/test-results/test/**/TEST-*.xml"

test:kubernetes:
  image: eclipse-temurin:17.0.12_7-jdk
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    TEST_K8S_NAMESPACE: "feather-test"
    TEST_K8S_DEPLOYMENT: "feather-test"
    TEST_K8S_READONLY: "true"
  rules:
    - if: $CI_PROJECT_NAMESPACE == "ingenieure-ohne-grenzen" && $CI_PIPELINE_SOURCE != 'merge_request_event'
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apt-get update
    - apt-get install --no-install-recommends --yes git
    # Workaround for https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29022
    - git config --global --add safe.directory $(pwd)
    - wget https://mirror.openshift.com/pub/openshift-v4/clients/oc/latest/linux/oc.tar.gz
    - tar xvf oc.tar.gz
    - cp "$TEST_K8S_CA" /usr/local/share/ca-certificates/kube.crt
    - update-ca-certificates
    - /opt/java/openjdk/bin/keytool -import -trustcacerts -cacerts -storepass changeit -noprompt -file "$TEST_K8S_CA"
  script:
    - ./oc login "$TEST_K8S_API_SERVER" --token "$TEST_K8S_TOKEN"
    - ./gradlew $TEST_LOG_LEVEL :kubernetes:check
  artifacts:
    expire_in: 30days
    when: always
    reports:
      junit:
        - "**/build/test-results/test/**/TEST-*.xml"

test:openproject:
  image: eclipse-temurin:17.0.12_7-jdk-alpine
  stage: test1
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    LDAP_HOST: "openldap"
    LDAP_PORT: "1389"
    LDAP_DOMAIN: "example.org"
    LDAP_ADMIN_PASSWORD: "admin"
    LDAP_ORGANISATION: "TEST"
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_USER: "admin"
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_PASSWORD: "admin"
    OPENPROJECT_AUTH__SOURCE__SSO_HEADER: "X-Remote-User"
    OPENPROJECT_AUTH__SOURCE__SSO_SECRET: "s3cr3t"
    OPENPROJECT_AUTH__SOURCE__SSO_OPTIONAL: "true"
    OPENPROJECT_HOST: "http://openproject:8080"
    OPENPROJECT_POSTGRES_HOST: "openproject"
    OPENPROJECT_POSTGRES_PORT: "5432"
    OPENPROJECT_EMAIL__DELIVERY__METHOD: "sendmail"
    OPENPROJECT_HTTPS: "false"
    OPENPROJECT_USERS__DELETABLE__BY__ADMINS: "true"
    OPENPROJECT_HOST__NAME: "openproject"
    # as expected in openproject/src/test/kotlin/dev/maximilian/feather/openproject/UserTest.kt
    OPENPROJECT_SEED__ADMIN__USER__MAIL: "admin@example.net"
    OPENPROJECT_SEED__ADMIN__USER__NAME: "OpenProject Admin"
    OPENPROJECT_SEED__ADMIN__USER__PASSWORD: "admin"
    OPENPROJECT_SEED__ADMIN__USER__PASSWORD__RESET: "false"
    OPENPROJECT_PLUGIN__OPENPROJECT__AVATARS_ENABLE__GRAVATARS: "false"
    OPENPROJECT_SEED_LDAP_IOG_HOST: "openldap"
    OPENPROJECT_SEED_LDAP_IOG_PORT: "1389"
    OPENPROJECT_SEED_LDAP_IOG_SECURITY: "plain_ldap"
    OPENPROJECT_SEED_LDAP_IOG_TLS__VERIFY: "false"
    OPENPROJECT_SEED_LDAP_IOG_BINDUSER: "cn=admin,dc=example,dc=org"
    OPENPROJECT_SEED_LDAP_IOG_BINDPASSWORD: "admin"
    OPENPROJECT_SEED_LDAP_IOG_BASEDN: "dc=example,dc=org"
    OPENPROJECT_SEED_LDAP_IOG_FILTER: "(cn=*)"
    OPENPROJECT_SEED_LDAP_IOG_SYNC__USERS: "true"
    OPENPROJECT_SEED_LDAP_IOG_LOGIN__MAPPING: "cn"
    OPENPROJECT_SEED_LDAP_IOG_FIRSTNAME__MAPPING: "givenName"
    OPENPROJECT_SEED_LDAP_IOG_LASTNAME__MAPPING: "sn"
    OPENPROJECT_SEED_LDAP_IOG_MAIL__MAPPING: "mail"
    OPENPROJECT_SEED_LDAP_IOG_ADMIN__MAPPING: "opAdmin"
    OPENPROJECT_LOGIN__REQUIRED: "true"
    OPENPROJECT_LOST__PASSWORD: "false"
    OPENPROJECT_SELF__REGISTRATION: "0"
    OPENPROJECT_IMAP_ENABLED: "false"
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_DISPLAY__NAME: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_HOST: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_PORT: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_SCHEME: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_ISSUER: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_CLIENT__OPTIONS_PORT: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_CLIENT__OPTIONS_SCHEME: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_AUTHORIZATION__ENDPOINT: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_TOKEN__ENDPOINT: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_USERINFO__ENDPOINT: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_END__SESSION__ENDPOINT: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_DISCOVERY: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_IDENTIFIER: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_SECRET: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_ATTRIBUTE__MAP_LOGIN: null
    OPENPROJECT_OPENID__CONNECT_KEYCLOAK_ATTRIBUTE__MAP_ADMIN: null
    OPENPROJECT_OMNIAUTH__DIRECT__LOGIN__PROVIDER: null
    OPENPROJECT_DISABLE__PASSWORD__LOGIN: "false"
    OPENPROJECT_2FA_DISABLED: "true"
    OPENPROJECT_2FA_ACTIVE__STRATEGIES: "[]"
    # next two lines for debugging purposes
    #TEST_LOG_LEVEL: "--info"
    #CI_DEBUG_SERVICES: "true"
  services:
    - name: registry.gitlab.com/ingenieure-ohne-grenzen/container_images/openldap-iog:OpenLDAP_2.6
      alias: openldap
    - name: openproject/openproject:15.0.2
      alias: openproject
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk upgrade --no-cache
    - apk add --no-cache postgresql-client curl git
    # Workaround for https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29022
    - git config --global --add safe.directory $(pwd)
  script:
    # Wait for LDAP to be up, ready and running
    - sleep 10 # currently no better solution, see integration/openldap_test/local_up.sh
    # Wait for OpenProject to be up, ready and running
    - integration/openproject/wait_ready.sh
    - integration/openproject/migrate_db.sh
    # Do the test
    - ./gradlew $TEST_LOG_LEVEL $DO_PARALLEL :openproject:check
  artifacts:
    expire_in: 30days
    when: always
    reports:
      junit: "**/build/test-results/test/**/TEST-*.xml"

test:civicrm:
  image: eclipse-temurin:17.0.12_7-jdk-alpine
  stage: test1
  cache:
    # Switch caching of for test purpose
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    DB_HOST: "127.0.0.1"
    DB_NAME: "civicrm"
    DB_USER: "root"
    DB_PASSWORD: "TCkIFkVbPANR4tOYRBvH"
    # select 0 for using internal database in civicrm container and 1 for external database
    EXTERNAL_DATABASE: 0
    MARIADB_DATABASE: "civicrm"
    MARIADB_ROOT_HOST: localhost
    DRUPAL_ADMIN_PASSWORD: "lJ1Ldl8jD0bQzYow9J83"
    DRUPAL_TRUSTED_HOST: "civicrm"
    SITE_NAME: "CiviCRM-Testinstallation"
    CRON_KEY: "hC4HREUnPyufjVVrvfGOq1z3D2GOtPAoShgsUWJFhsWZZ06iCmS4F7lo6m35RZ6F7lJf4d"
    CIVICRM_HOST: "http://civicrm:8083"
    CIVICRM_APIKEY: "WuM5g2nuG8kFHXLID74p"
    TEST_REDIS_HOST: "redis"
  services:
    - name: registry.gitlab.com/ingenieure-ohne-grenzen/container_images/civicrm:CiviCRM_5.74.5
      alias: civicrm
      pull_policy: always
    - name: redis:latest
      alias: redis
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk add --no-cache curl bash netcat-openbsd git
    # Workaround for https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29022
    - git config --global --add safe.directory $(pwd)
  script:
    - ping civicrm -c 1
    - integration/civicrm/wait_ready.sh
    - ./gradlew $TEST_LOG_LEVEL $DO_PARALLEL :civicrm:check
  artifacts:
    expire_in: 30days
    when: always
    reports:
      junit: "**/build/test-results/test/**/TEST-*.xml"

# resource considerations:
# will the values for KUBERNETES_xxx_REQUEST and
# KUBERNETES_xxx_LIMIT, this CI job (nServices=5)
# needs 1 + nServices * 0.5 = 3.5 cpu cores
# (limited to 2 + nServices * 0.75 = 5.75)
# and 2 GiB + nServices * 1 GiB = 7 GiB memory
# (limited to 4 GiB + nServices * 2 GiB = 14 GiB)
# already. Thus, adding more service containers
# not recommended!
test:multiserviceAndIog:
  image: eclipse-temurin:17.0.12_7-jdk-alpine
  stage: test2
  cache:
    # inherit all global cache settings
    <<: *global_cache
    # override the policy
    policy: pull
  variables:
    LDAP_HOST: "openldap"
    LDAP_PORT: "1389"
    LDAP_DOMAIN: "example.org"
    LDAP_ADMIN_PASSWORD: "admin"
    LDAP_ORGANISATION: "TEST"
    LDAP_CONFIGURE_PPOLICY: "yes"
    LDAP_PPOLICY_USE_LOCKOUT: "yes"
    NEXTCLOUD_ADMIN_USER: "ncadmin" # for Nextcloud
    NEXTCLOUD_ADMIN_PASSWORD: "ncadminsecret"
    NEXTCLOUD_TRUSTED_DOMAINS: "localhost nextcloud"
    SQLITE_DATABASE: nctest # to enable auto installation with SQLite
    NEXTCLOUD_BASE_URL: "http://nextcloud" # for Feather
    NEXTCLOUD_USER: "ncadmin"
    NEXTCLOUD_PWD: "ncadminsecret"
    TEST_REDIS_HOST: "redis"
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_USER: "admin"
    OPENPROJECT_AUTHENTICATION_GLOBAL__BASIC__AUTH_PASSWORD: "admin"
    OPENPROJECT_AUTH__SOURCE__SSO_HEADER: "X-Remote-User"
    OPENPROJECT_AUTH__SOURCE__SSO_SECRET: "s3cr3t"
    OPENPROJECT_AUTH__SOURCE__SSO_OPTIONAL: "true"
    OPENPROJECT_SEED_LDAP_IOG_HOST: "openldap"
    OPENPROJECT_SEED_LDAP_IOG_PORT: "1389"
    OPENPROJECT_SEED_LDAP_IOG_SECURITY: "plain_ldap"
    OPENPROJECT_SEED_LDAP_IOG_TLS__VERIFY: "false"
    OPENPROJECT_SEED_LDAP_IOG_BINDUSER: "cn=admin,dc=example,dc=org"
    OPENPROJECT_SEED_LDAP_IOG_BINDPASSWORD: "admin"
    OPENPROJECT_SEED_LDAP_IOG_BASEDN: "dc=example,dc=org"
    OPENPROJECT_SEED_LDAP_IOG_FILTER: "(cn=*)"
    OPENPROJECT_SEED_LDAP_IOG_SYNC__USERS: "true"
    OPENPROJECT_SEED_LDAP_IOG_LOGIN__MAPPING: "cn"
    OPENPROJECT_SEED_LDAP_IOG_FIRSTNAME__MAPPING: "givenName"
    OPENPROJECT_SEED_LDAP_IOG_LASTNAME__MAPPING: "sn"
    OPENPROJECT_SEED_LDAP_IOG_MAIL__MAPPING: "mail"
    OPENPROJECT_SEED_LDAP_IOG_ADMIN__MAPPING: "opAdmin"

    # directly use the web-worker because the apache will fail to start
    # in a Kubernetes executor (because the apache of the nextcloud
    # service already blocks the POD's port 80)
    OPENPROJECT_HOST: "http://openproject:8080"
    OPENPROJECT_WEB_TIMEOUT: 120
    OPENPROJECT_WEB_WAIT__TIMEOUT: 60
    OPENPROJECT_POSTGRES_HOST: "openproject"
    OPENPROJECT_POSTGRES_PORT: 5432
    OPENPROJECT_HTTPS: "false"
    OPENPROJECT_USERS__DELETABLE__BY__ADMINS: "true"
    OPENPROJECT_SEED__ADMIN__USER__PASSWORD__RESET: "false"
    OPENPROJECT_PLUGIN__OPENPROJECT__AVATARS_ENABLE__GRAVATARS: "false"
    # CIVICRM Settings
    DB_HOST: "127.0.0.1"
    DB_NAME: "civicrm"
    DB_USER: "root"
    DB_PASSWORD: "TCkIFkVbPANR4tOYRBvH"
    # select 0 for using internal database in civicrm container and 1 for external database
    EXTERNAL_DATABASE: 0
    MARIADB_DATABASE: "civicrm"
    MARIADB_ROOT_HOST: localhost
    DRUPAL_ADMIN_PASSWORD: "lJ1Ldl8jD0bQzYow9J83"
    DRUPAL_TRUSTED_HOST: "civicrm"
    SITE_NAME: "CiviCRM-Testinstallation"
    CRON_KEY: "hC4HREUnPyufjVVrvfGOq1z3D2GOtPAoShgsUWJFhsWZZ06iCmS4F7lo6m35RZ6F7lJf4d"
    CIVICRM_HOST: "http://civicrm:8083"
    CIVICRM_APIKEY: "WuM5g2nuG8kFHXLID74p"
  services:
    - name: registry.gitlab.com/ingenieure-ohne-grenzen/container_images/openldap-iog:OpenLDAP_2.6
      alias: openldap
    - name: nextcloud:30.0.2
      alias: nextcloud
    - name: openproject/openproject:15.0.2
      alias: openproject
    - name: redis:latest
      alias: redis
    - name: registry.gitlab.com/ingenieure-ohne-grenzen/container_images/civicrm:CiviCRM_5.74.5
      alias: civicrm
      pull_policy: always
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradleHome
    - apk upgrade --no-cache
    - apk add --no-cache postgresql-client curl bash netcat-openbsd git
    # Workaround for https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29022
    - git config --global --add safe.directory $(pwd)
  script:
    # Wait for LDAP to be up, ready and running
    - sleep 10 # currently no better solution, see integration/openldap_test/local_up.sh
    - integration/nextcloud_tests/wait_ready.sh
    - integration/nextcloud_tests/install_nc_apps.sh
    - integration/openproject/wait_ready.sh
    - integration/openproject/migrate_db.sh --iog
    - ping civicrm -c 1
    - integration/civicrm/wait_ready.sh
    - ./gradlew $TEST_LOG_LEVEL :core:check :lib:check :multiservice:check :iog:check
  artifacts:
    expire_in: 30days
    when: always
    reports:
      junit:
        - "**/build/test-results/test/**/TEST-*.xml"

build:docker:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: package
  cache: {}
  dependencies:
    - build:gradle
  only:
    - main
    - tags
  before_script:
    - if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then TAG_LIST="latest"; fi
    - if [[ ! -z "$CI_COMMIT_TAG" ]]; then TAG_LIST="$TAG_LIST stable $CI_COMMIT_TAG"; fi
    - for TAG in $TAG_LIST; do FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_REGISTRY_IMAGE:$TAG "; done;
    - FORMATTEDTAGLIST=$(echo "${FORMATTEDTAGLIST}" | sed s/\-\-tag/\-\-destination/g)
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile $FORMATTEDTAGLIST --cache=true --verbosity=warn

deploy:dev:
  environment: dev
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: deploy
  cache: {}
  only:
    - main
  dependencies:
    - build:gradle
  script:
    - echo "{\"auths\":{\"$DEPLOY_REGISTRY_URL\":{\"username\":\"$DEPLOY_REGISTRY_USER\",\"password\":\"$DEPLOY_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $DEPLOY_REGISTRY_IMAGE:latest --cache=false --verbosity=warn

deploy:prod:
  environment: prod
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: deploy
  cache: {}
  only:
    - tags
  dependencies:
    - build:gradle
  script:
    - echo "{\"auths\":{\"$DEPLOY_REGISTRY_URL\":{\"username\":\"$DEPLOY_REGISTRY_USER\",\"password\":\"$DEPLOY_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $DEPLOY_REGISTRY_IMAGE:stable --cache=false --verbosity=warn
