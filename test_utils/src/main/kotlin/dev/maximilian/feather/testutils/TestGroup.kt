package dev.maximilian.feather.testutils

import dev.maximilian.feather.Group
import java.util.UUID

public object TestGroup {
    public fun generateTestGroup(
        id: Int = 0,
        name: String = UUID.randomUUID().toString(),
        description: String = UUID.randomUUID().toString(),
        userMembers: Set<Int> = emptySet(),
        groupMembers: Set<Int> = emptySet(),
        parentGroups: Set<Int> = emptySet(),
        owners: Set<Int> = emptySet(),
        ownerGroups: Set<Int> = emptySet(),
        ownedGroups: Set<Int> = emptySet(),
    ): Group =
        Group(
            id = id,
            name = name,
            description = description,
            userMembers = userMembers,
            groupMembers = groupMembers,
            parentGroups = parentGroups,
            owners = owners,
            ownerGroups = ownerGroups,
            ownedGroups = ownedGroups,
        )
}
