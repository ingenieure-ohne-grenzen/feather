/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.testutils

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.RedisFactory
import dev.maximilian.feather.account.AccountController
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool
import java.sql.DriverManager

public object ServiceConfig {
    public val DATABASE: Database by lazy { database("feather") }
    public val ACCOUNT_CONTROLLER: AccountController by lazy {
        AccountController(DATABASE, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))
    }
    public val CREDENTIAL_PROVIDER: ICredentialProvider by lazy { ACCOUNT_CONTROLLER }

    public val JEDIS_POOL: JedisPool by lazy {
        val testSetting =
            mutableMapOf(
                "redis.host" to getEnv("TEST_REDIS_HOST", "127.0.0.1"),
                "redis.port" to getEnv("TEST_REDIS_PORT", "6379"),
            )
        RedisFactory(testSetting).create().apply {
            resource.use { it.ping() }
        }
    }

    private val databaseMap: MutableMap<String, Database> = mutableMapOf()

    public fun database(name: String): Database =
        databaseMap.getOrPut(name) {
            Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$name;DB_CLOSE_DELAY=-1") })
        }
}
