/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.keycloakActions

import dev.maximilian.feather.account.AccountController
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import io.javalin.http.bodyAsClass

internal class KeycloakActionsApi(
    private val controller: KeycloakActionsController,
    app: Javalin,
    private val authToken: String,
    private val accountController: AccountController,
) {
    init {
        app.routes {
            path("/plugins/keycloak-actions") {
                post("/check/{user}", ::handleCheckUserAndCreateTokenIfNeeded)
            }
        }
    }

    private fun handleCheckUserAndCreateTokenIfNeeded(ctx: Context) {
        if (ctx.header("X-Token") != authToken) {
            throw ForbiddenResponse()
        }

        val userId = ctx.pathParam("user")
        val user = accountController.getUserByExternalId(userId) ?: throw NotFoundResponse()
        val backUrl = ctx.bodyAsClass<BackUrlBody>().backUrl

        val token = controller.checkUserAndCreateTokenIfNeeded(user, backUrl)
        accountController.updateLastLoginOf(user)

        ctx.json(TokenAnswer(token))
    }

    private data class BackUrlBody(
        val backUrl: String,
    )

    private data class TokenAnswer(
        val token: String?,
    )
}
