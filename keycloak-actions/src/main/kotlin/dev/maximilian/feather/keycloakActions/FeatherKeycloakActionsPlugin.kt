/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.keycloakActions

import dev.maximilian.feather.plugin.FeatherPlugin
import dev.maximilian.feather.plugin.FeatherPluginInitialization
import java.util.UUID

public class FeatherKeycloakActionsPlugin : FeatherPlugin {
    private lateinit var featherPluginInitialization: FeatherPluginInitialization

    private val actionController by lazy {
        KeycloakActionsController(
            featherPluginInitialization.db,
            featherPluginInitialization.actionController,
            featherPluginInitialization.accountController,
        )
    }

    private val actionsApi by lazy {
        KeycloakActionsApi(
            actionController,
            featherPluginInitialization.app,
            featherPluginInitialization.propertyMap.getOrPut("keycloak_actions.token") { UUID.randomUUID().toString() },
            featherPluginInitialization.accountController,
        )
    }

    override val name: String = "Feather Keycloak Actions"

    override fun initialize(featherPluginInitialization: FeatherPluginInitialization) {
        this.featherPluginInitialization = featherPluginInitialization
        actionsApi.apply { /* Initialize lazy */ }
        featherPluginInitialization.authorizationController.authorizer.add(
            KeycloakActionsAuthorizer(
                featherPluginInitialization.baseUrl,
                actionController,
                featherPluginInitialization.actionController,
            ),
        )
    }

    override fun start() {
    }

    override fun stop() {
    }

    override fun dispose() {
        // TODO remove endpoints from app
    }
}
