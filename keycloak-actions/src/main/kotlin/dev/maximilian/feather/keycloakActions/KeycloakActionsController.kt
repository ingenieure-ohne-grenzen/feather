/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.keycloakActions

import dev.maximilian.feather.Session
import dev.maximilian.feather.User
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.EMPTY_UUID
import org.jetbrains.exposed.sql.Database
import java.util.UUID

internal class KeycloakActionsController(
    db: Database,
    private val actionController: ActionController,
    accountController: AccountController,
) {
    private val db = KeycloakActionsDatabase(db, accountController)

    internal fun getActionByIdAndUse(id: UUID) = db.getKeycloakActionById(id, true)

    internal fun getActionBySession(session: Session): KeycloakAction? =
        session.refreshToken?.let { db.getKeycloakActionById(UUID.fromString(it), false) }

    internal fun deleteActionBySession(session: Session) {
        session.refreshToken?.let { db.deleteActionById(UUID.fromString(it)) }
    }

    internal fun checkUserAndCreateTokenIfNeeded(
        user: User,
        backUrl: String,
    ): String? {
        val needsAction = actionController.getOpenUserActions(user).isNotEmpty()

        if (!needsAction) return null
        return db.insertKeycloakAction(
            KeycloakAction(
                id = EMPTY_UUID,
                user = user,
                backUrl = backUrl,
            ),
        ).id.toString()
    }
}
