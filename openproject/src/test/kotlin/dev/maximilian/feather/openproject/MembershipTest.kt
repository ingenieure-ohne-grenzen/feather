/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

class MembershipTest {
    private val api = TestUtil.openProject

    @Test
    fun `createMembership() with user works`() {
        runBlocking {
            val userAsync = async { api.createUser(generateTestUser(), "1234567890") }
            val projectAsync = async { api.createProject(generateTestProject()) }
            val roleAsync = async { api.getRoles().first { it.name == "Project admin" } }

            val (user, project, role) = Triple(userAsync.await(), projectAsync.await(), roleAsync.await())

            val membership =
                OpenProjectMembership(
                    0,
                    IdLazyEntity(project.id) { project },
                    OpenProjectPrincipalLazyEntity(user.id, OpenProjectPrincipalType.USER) { user },
                    listOf(role),
                )

            val answer = api.createMembership(membership)

            // The laziness was not touched!
            assertFalse(answer.project.isInitialized())
            assertFalse(answer.principal.isInitialized())

            // Now load and check quality
            assertEquals(membership, answer.copy(id = 0))
        }
    }

    @Test
    fun `createMembership() with group works`() {
        runBlocking {
            val (group, project, roles) = createGroupProjectRoles()
            val role = roles.first { it.name == "Project admin" }

            val membership =
                OpenProjectMembership(
                    0,
                    IdLazyEntity(project.id) { project },
                    OpenProjectPrincipalLazyEntity(group.id, OpenProjectPrincipalType.GROUP) { group },
                    listOf(role),
                )

            val answer = api.createMembership(membership)

            // The laziness was not touched!
            assertFalse(answer.project.isInitialized())
            assertFalse(answer.principal.isInitialized())

            // Now load and check quality
            assertEquals(membership, answer.copy(id = 0))
        }
    }

    @Test
    fun `getMembership() works`() {
        runBlocking {
            val (group, project, roles) = createGroupProjectRoles()
            val role = roles.first { it.name == "Project admin" }
            val membership = api.createMembership(OpenProjectMembership(0, project, group, listOf(role)))
            val answer = api.getMembership(membership.id, roles)

            assertNotNull(answer)

            // The answer contains lazy objects
            assertFalse(answer.project.isInitialized())
            assertFalse(answer.principal.isInitialized())

            // Now load and check quality
            assertEquals(membership, answer)
        }
    }

    @Test
    fun `getMemberships() works`() {
        runBlocking {
            val userAsync = async { api.createUser(generateTestUser(), "1234567890") }
            val (group, project, roles) = createGroupProjectRoles()
            val user = userAsync.await()

            val membership1 =
                async {
                    api.createMembership(
                        OpenProjectMembership(
                            0,
                            project,
                            group,
                            roles.filter { it.name == "Project admin" },
                        ),
                    )
                }
            val membership2 =
                async { api.createMembership(OpenProjectMembership(0, project, user, roles.filter { it.name == "Member" })) }

            val answer1 = async { api.getMembership(membership1.await().id, roles) }
            val answer2 = async { api.getMembership(membership2.await().id, roles) }

            assertNotNull(answer1.await())
            assertNotNull(answer2.await())

            // Now load and check quality
            assertEquals(membership1.await(), answer1.await())
            assertEquals(membership2.await(), answer2.await())
        }
    }

    @Test
    fun `deleteMembership() works`() {
        runBlocking {
            val (group, project, roles) = createGroupProjectRoles()
            val role = roles.first { it.name == "Project admin" }

            val answer =
                api.createMembership(
                    OpenProjectMembership(
                        0,
                        IdLazyEntity(project.id) { project },
                        OpenProjectPrincipalLazyEntity(group.id, OpenProjectPrincipalType.GROUP) { group },
                        listOf(role),
                    ),
                )

            api.deleteMembership(answer)

            // Now load and check quality
            assertNull(api.getMembership(answer.id, roles))
        }
    }

    @Test
    fun `lazy loaded identities with the same id are the same object in getMemberships()`() {
        runBlocking {
            val group1 = async { api.createGroup(generateTestGroup()) }
            val group2 = async { api.createGroup(generateTestGroup()) }

            val project1 = async { api.createProject(generateTestProject()) }
            val project2 = async { api.createProject(generateTestProject()) }

            val roles = async { api.getRoles() }
            val role = roles.await().first { it.name == "Project admin" }

            val asyncMemberships =
                Triple(
                    async {
                        api.createMembership(
                            OpenProjectMembership(
                                0,
                                project1.await(),
                                group1.await(),
                                listOf(role),
                            ),
                        )
                    },
                    async {
                        api.createMembership(
                            OpenProjectMembership(
                                0,
                                project2.await(),
                                group1.await(),
                                listOf(role),
                            ),
                        )
                    },
                    async {
                        api.createMembership(
                            OpenProjectMembership(
                                0,
                                project2.await(),
                                group2.await(),
                                listOf(role),
                            ),
                        )
                    },
                )

            val (m1, m2, m3) =
                Triple(
                    asyncMemberships.first.await(),
                    asyncMemberships.second.await(),
                    asyncMemberships.third.await(),
                )

            val allMemberships = api.getMemberships(roles.await())
            val memberships = allMemberships.filter { it.id == m1.id || it.id == m2.id }
            val memberships2 = allMemberships.filter { it.id == m2.id || it.id == m3.id }

            assertEquals(2, memberships.size)
            assertTrue("Principals must be the same object") { memberships[0].principal === memberships[1].principal }
            assertEquals(2, memberships2.size)
            assertTrue("Projects must be the same object") { memberships2[0].project === memberships2[1].project }
        }
    }

    private suspend fun createGroupProjectRoles(): Triple<OpenProjectGroup, OpenProjectProject, List<OpenProjectRole>> =
        coroutineScope {
            val group = async { api.createGroup(generateTestGroup()) }
            val project = async { api.createProject(generateTestProject()) }
            val roles = async { api.getRoles() }

            Triple(group.await(), project.await(), roles.await())
        }
}
