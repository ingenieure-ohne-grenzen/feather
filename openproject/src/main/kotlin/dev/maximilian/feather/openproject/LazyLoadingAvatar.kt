package dev.maximilian.feather.openproject

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ResponseException
import io.ktor.client.request.get
import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.runBlocking
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import javax.imageio.ImageIO

internal class LazyLoadingAvatar(
    private val webpageClientFunction: () -> HttpClient,
    internal val source: String,
) : OpenProjectUserAvatar {
    private fun loadImage(): BufferedImage? {
        if (source.isBlank()) return null

        return runBlocking {
            try {
                webpageClientFunction().get(source).body<ByteArray>()
            } catch (e: ResponseException) {
                if (e.response.status == HttpStatusCode.NotFound) {
                    null
                } else {
                    throw e
                }
            }
        }?.let { ImageIO.read(ByteArrayInputStream(it)) }
    }

    private val holder = lazy(initializer = ::loadImage)

    override fun hashCode(): Int = source.hashCode()

    override fun isInitialized(): Boolean = holder.isInitialized()

    override fun toString(): String = if (isInitialized()) {
        value.toString()
    } else {
        "Lazy value not initialized yet. Source: $source"
    }

    override val value: BufferedImage? by holder

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is EmptyAvatar && source == "") return true
        if (javaClass != other?.javaClass) return false

        other as LazyLoadingAvatar

        return source == other.source
    }
}
