/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

plugins {
    alias(libs.plugins.kotlin.serialization)
}

dependencies {
    // Html parsing
    implementation(libs.jsoup)

    // Ktor client
    implementation(libs.bundles.ktor.client)

    // Semantic versioning lib
    implementation(libs.semver)

    // Ktor server, only used for testing
    testImplementation(libs.bundles.ktor.server)
}

tasks {
    test {
        maxParallelForks = (Runtime.getRuntime().availableProcessors() / 2).takeIf { it > 0 } ?: 1
    }
}

kotlin {
    explicitApi()
}
