/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals

import com.github.mustachejava.DefaultMustacheFactory
import com.github.mustachejava.Mustache
import dev.maximilian.feather.multiservice.settings.MailContentSettings
import dev.maximilian.feather.multiservice.settings.WriteMailSettings
import java.io.StringReader
import java.io.StringWriter
import java.util.UUID

fun Mustache.execute(value: Any): String = StringWriter().also { execute(it, value) }.toString()

class FeatherMustache<T : Any>(value: String) {
    private val mustache = DefaultMustacheFactory().compile(StringReader(value), UUID.randomUUID().toString())

    fun execute(on: T): String = StringWriter().also { mustache.execute(it, on) }.toString()
}

class PartialFeatherMustache<T : Any>(
    value: String,
    private val defaults: WriteMailSettings,
) {
    private val mustache: Mustache
    init {
        val updatedValue = value.replace("{{ link }}", "{{ baseUrl }}{{ link }}")
        mustache = DefaultMustacheFactory().compile(StringReader(updatedValue), UUID.randomUUID().toString())
    }

    fun execute(on: T): String = StringWriter().also { mustache.execute(it, listOf(on, defaults)) }.toString()
}

@Suppress("ktlint:standard:property-naming")
class EmailTemplates(
    mailContent: MailContentSettings,
    writeMailSettings: WriteMailSettings,
) {
    val MAIL_PASSWORD_FORGOT_CONTENT = PartialFeatherMustache<MailPasswordForgotData>(mailContent.forgot.content, writeMailSettings)
    val MAIL_PASSWORD_FORGOT_SUBJECT = FeatherMustache<SitenameData>(mailContent.forgot.subject).execute(SitenameData(writeMailSettings.siteName))

    val MAIL_INVITATION_CONTENT = PartialFeatherMustache<MailInvitationData>(mailContent.invite.content, writeMailSettings)
    val MAIL_INVITATION_SUBJECT = FeatherMustache<SitenameData>(mailContent.invite.subject).execute(SitenameData(writeMailSettings.siteName))

    val MAIL_CHANGE_MAIL_ADDRESS_CONTENT =
        PartialFeatherMustache<MailChangeMailAddressData>(mailContent.change.content, writeMailSettings)
    val MAIL_CHANGE_MAIL_ADDRESS_SUBJECT = FeatherMustache<SitenameData>(mailContent.change.subject).execute(SitenameData(writeMailSettings.siteName))

    val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION1 =
        PartialFeatherMustache<MailChangeMailAddressNotificationData>(mailContent.change.addressNotification1, writeMailSettings)
    val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION2 =
        FeatherMustache<MailChangeMailAddressNotificationData>(mailContent.change.addressNotification2)

    val MAIL_INACTIVITY_REMINDER_1_CONTENT =
        PartialFeatherMustache<InactivityReminder1Data>(mailContent.inactivityReminders.first.content, writeMailSettings)
    val MAIL_INACTIVITY_REMINDER_1_SUBJECT =
        FeatherMustache<SitenameData>(mailContent.inactivityReminders.first.subject).execute(SitenameData(writeMailSettings.siteName))

    val MAIL_INACTIVITY_REMINDER_2_CONTENT =
        PartialFeatherMustache<InactivityReminder2Data>(mailContent.inactivityReminders.second.content, writeMailSettings)
    val MAIL_INACTIVITY_REMINDER_2_SUBJECT =
        FeatherMustache<SitenameData>(mailContent.inactivityReminders.second.subject)
}

data class MailPasswordForgotData(
    val firstname: String,
    val surname: String,
    val username: String,
    val link: String,
)

data class MailInvitationData(
    val firstname: String,
    val surname: String,
    val link: String,
)

data class MailChangeMailAddressData(
    val firstname: String,
    val surname: String,
    val username: String,
    val link: String,
)

data class MailChangeMailAddressNotificationData(
    val firstname: String,
    val surname: String,
    val username: String,
    val oldMail: String,
    val newMail: String,
)

data class InactivityReminder1Data(
    val firstname: String,
    val surname: String,
    val username: String,
    val accountDeactivationDate: String,
    val accountDeletionDate: String,
)

data class InactivityReminder2Data(
    val firstname: String,
    val surname: String,
    val username: String,
    val accountDeletionDate: String,
)

fun LostPassword.toMailPasswordForgotData() =
    MailPasswordForgotData(
        firstname = user.firstname,
        username = user.username,
        surname = user.surname,
        link = "/reset/$id",
    )

fun Invitation.toMailInvitationData() =
    MailInvitationData(
        firstname = invitee.firstname,
        surname = invitee.surname,
        link = "/invite/$id",
    )

fun ChangeMailAddress.toMailChangeMailAddressData() =
    MailChangeMailAddressData(
        firstname = user.firstname,
        username = user.username,
        surname = user.surname,
        link = "/change_mail/$id",
    )

fun ChangeMailAddress.toMailChangeMailAddressNotificationData() =
    MailChangeMailAddressNotificationData(
        firstname = user.firstname,
        username = user.username,
        surname = user.surname,
        oldMail = user.mail,
        newMail = newMail,
    )

data class SitenameData(
    val sitename: String,
)
