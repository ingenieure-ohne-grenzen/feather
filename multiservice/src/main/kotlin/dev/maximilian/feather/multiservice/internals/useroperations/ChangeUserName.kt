/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.useroperations

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import mu.KLogging

internal class ChangeUserName(
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>,
) {
    companion object : KLogging()

    fun changeUserName(
        user: User,
        newFirstname: String,
        newSurname: String,
        newDisplayName: String,
    ): User {
        logger.info { "ChangeUserName::changeUserName Changing name for user ${user.id} to $newSurname, $newFirstname ($newDisplayName)" }

        var ok = true

        services.forEach {
            if (!it.changeUserName(user, newFirstname, newSurname, newDisplayName)) {
                ok = false
            }
        }

        if (!ok) {
            throw IllegalStateException("Cannot change mail address because of previous error")
        }

        return credentialProvider.updateUser(
            user.copy(
                firstname = newFirstname,
                surname = newSurname,
                displayName = newDisplayName,
            ),
        ) ?: throw IllegalStateException("Failed to change mail in LDAP")
    }
}
