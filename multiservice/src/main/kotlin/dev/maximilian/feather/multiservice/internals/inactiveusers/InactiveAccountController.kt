package dev.maximilian.feather.multiservice.internals.inactiveusers

import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.Action
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.multiservice.internals.EmailTemplates
import dev.maximilian.feather.multiservice.internals.IMailSender
import dev.maximilian.feather.multiservice.internals.InactivityReminder1Data
import dev.maximilian.feather.multiservice.internals.InactivityReminder2Data
import dev.maximilian.feather.multiservice.internals.Mail
import dev.maximilian.feather.multiservice.internals.SitenameData
import dev.maximilian.feather.multiservice.settings.InactiveUsersSettings
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import jakarta.mail.internet.InternetAddress
import mu.KotlinLogging
import java.time.Duration
import java.time.Instant
import java.time.InstantSource
import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAmount
import java.util.Date
import kotlin.concurrent.fixedRateTimer

const val LAST_RUN_DATE_DB_KEY = "inactive_accounts.last_run_date"

val zone: ZoneId = ZoneId.systemDefault()
fun Instant.toLocalDate(): LocalDate = LocalDate.ofInstant(this, zone)
fun LocalDate.toInstant(): Instant = this.atStartOfDay(zone).toInstant()

class ReactivateAction(
    override val userId: Int,
) : Action<Unit> {
    companion object {
        const val NAME: String = "reactivate"
    }

    override val name: String = NAME
    override val payload = Unit
    override val description: String = "Account reaktivieren"

    override fun payloadToString(): String = ""
}

val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)

internal class InactiveAccountController(
    settings: InactiveUsersSettings,
    private val accountController: AccountController,
    private val mailSender: IMailSender,
    private val mailSettings: MultiServiceMailSettings,
    private val emailTemplates: EmailTemplates,
    private val persistentVariableStore: MutableMap<String, String>,
    private val actionController: ActionController,
    // Used only for testing
    private val clock: InstantSource = InstantSource.system(),
) {
    private val logger = KotlinLogging.logger {}

    private val inactivityPeriodBeforReminder: TemporalAmount =
        Period.ofMonths(settings.reminderAfterInactivityInMonths)
    private val deactivationPeriodAfterReminder: TemporalAmount =
        Period.ofMonths(settings.deactivationAfterReminderInMonths)
    private val deletionPeriodAfterReminder: TemporalAmount =
        Period.ofMonths(settings.deletionAfterDeactivationInMonths + settings.deactivationAfterReminderInMonths)
    private val protectedGroupName = settings.protectedGroup

    private fun User.lastActiveDate() = maxOf(lastLoginAt, registeredSince)

    // If a user is last active e.g. on the 15th (at any time in the day),
    // they will be reminded, deactivated, and deleted on the 16th of a month.
    // toLocalDate rounds down, so we have to add a day
    // all times are considered in the time zone zoneId (see above)
    private fun User.deactivationDate() = lastActiveDate().toLocalDate().plus(inactivityPeriodBeforReminder).plus(deactivationPeriodAfterReminder).plus(1, ChronoUnit.DAYS)
    private fun User.deletionDate() = lastActiveDate().toLocalDate().plus(inactivityPeriodBeforReminder).plus(deletionPeriodAfterReminder).plus(1, ChronoUnit.DAYS)

    init {
        actionController.registerActionType("reactivate", {}) { userId: Int, payload: String ->
            assert(payload == "")
            ReactivateAction(userId)
        }
    }

    internal fun getCentralOfficeGroups(): List<Int> = if (protectedGroupName != null) {
        accountController.getGroups().filter {
            it.name.startsWith(protectedGroupName)
        }.map { it.id }
    } else {
        listOf()
    }

    fun remindInactiveUsers() {
        fun tooOldThreshold(instant: Instant) = instant.toLocalDate().minus(inactivityPeriodBeforReminder).toInstant()

        val now = clock.instant()
        val lastReminderDate = getLastRunDate() ?: Instant.EPOCH
        val tooOldThreshold = tooOldThreshold(now)
        val tooOldButAlreadyRemindedThreshold = tooOldThreshold(lastReminderDate)

        logger.info { "Remininding inactive users for timespan from $tooOldButAlreadyRemindedThreshold to $tooOldThreshold" }

        val centralOfficeGroups = getCentralOfficeGroups()

        accountController.getUsers() // for better performance, we could directly filter the users in SQL
            .filter { user ->
                !(
                    user.permissions.contains(Permission.ADMIN) ||
                        user.permissions.contains(Permission.FUNCTION_ACCOUNT) ||
                        user.groups.any { centralOfficeGroups.contains(it) }
                    )
            }
            .filter { user -> user.lastActiveDate().run { (equals(tooOldThreshold) or isBefore(tooOldThreshold)) and isAfter(tooOldButAlreadyRemindedThreshold) } }
            .forEach { user ->
                logger.info { "Remininding inactive user ${user.username}" }

                val mailData = InactivityReminder1Data(
                    firstname = user.firstname,
                    surname = user.surname,
                    username = user.username,
                    accountDeactivationDate = user.deactivationDate().let { dateFormatter.format(it) },
                    accountDeletionDate = user.deletionDate().let { dateFormatter.format(it) },
                )
                val mailSubject = emailTemplates.MAIL_INACTIVITY_REMINDER_1_SUBJECT
                val mailContent = emailTemplates.MAIL_INACTIVITY_REMINDER_1_CONTENT.execute(mailData)

                mailSender.submitMail(
                    Mail(
                        receiver = setOf(InternetAddress(user.mail, "${user.firstname} ${user.surname}")),
                        subject = mailSubject,
                        body = mailContent,
                    ),
                )
            }
        setLastRunDate(now)
    }

    fun disableInactiveUsers() {
        fun tooOldThreshold(instant: Instant) = instant.toLocalDate()
            .minus(deactivationPeriodAfterReminder).minus(inactivityPeriodBeforReminder)
            .toInstant()

        val now = clock.instant()
        val tooOldThreshold = tooOldThreshold(now)

        logger.info { "Generating reactivation actions for inactive users for timespan to $tooOldThreshold" }

        val centralOfficeGroups = getCentralOfficeGroups()

        accountController.getUsers()
            .filter { user ->
                !(
                    user.permissions.contains(Permission.ADMIN) ||
                        user.permissions.contains(Permission.FUNCTION_ACCOUNT) ||
                        user.groups.any { centralOfficeGroups.contains(it) }
                    )
            }.filter { user -> user.lastActiveDate().run { equals(tooOldThreshold) or isBefore(tooOldThreshold) } }
            .filter { user -> !(actionController.getOpenUserActions(user).any { it is ReactivateAction }) }
            .forEach { user ->
                logger.info { "Blocking inactive user ${user.username} by reactivation action" }

                actionController.insertAction(ReactivateAction(user.id))

                val mailData = InactivityReminder2Data(
                    firstname = user.firstname,
                    surname = user.surname,
                    username = user.username,
                    accountDeletionDate = user.deletionDate().let { dateFormatter.format(it) },
                )
                val mailSubject = emailTemplates.MAIL_INACTIVITY_REMINDER_2_SUBJECT.execute(SitenameData(mailSettings.sender.siteName))
                val mailContent = emailTemplates.MAIL_INACTIVITY_REMINDER_2_CONTENT.execute(mailData)

                mailSender.submitMail(
                    Mail(
                        receiver = setOf(InternetAddress(user.mail, "${user.firstname} ${user.surname}")),
                        subject = mailSubject,
                        body = mailContent,
                    ),
                )
            }
    }

    /**
     * Start a daemon to periodically check inactive users.
     */
    fun periodicallyRemindInactiveUsers() {
        fixedRateTimer(
            daemon = true,
            startAt = Date.from(Instant.now().plus(Duration.ofHours(1))),
            period = Duration.ofDays(1).toMillis(),
        ) {
            remindInactiveUsers()
            disableInactiveUsers()
        }
    }

    private fun getLastRunDate(): Instant? = persistentVariableStore[LAST_RUN_DATE_DB_KEY]?.let { Instant.parse(it) }
    private fun setLastRunDate(lastRunDate: Instant) {
        persistentVariableStore[LAST_RUN_DATE_DB_KEY] = lastRunDate.toString()
    }
}
