package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.multiservice.internals.inactiveusers.ReactivateAction
import dev.maximilian.feather.sessionOrMinisession
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.Context
import io.javalin.openapi.HttpMethod
import io.javalin.openapi.OpenApi
import io.javalin.openapi.OpenApiResponse

class ReactivateApi(
    app: Javalin,
    private val actionController: ActionController,
) {
    init {
        app.routes {
            path("reactivate") {
                post(::reactivateUser)
            }
        }
    }

    @OpenApi(
        summary = "Currently authenticated user reactivates their account",
        responses = [OpenApiResponse("201")],
        path = "/v1/reactivate",
        methods = [HttpMethod.POST],
    )
    private fun reactivateUser(ctx: Context) {
        val user = ctx.sessionOrMinisession().user
        actionController.deleteActionsByNameAndUser(ReactivateAction.NAME, user)
        ctx.status(201)
    }
}
