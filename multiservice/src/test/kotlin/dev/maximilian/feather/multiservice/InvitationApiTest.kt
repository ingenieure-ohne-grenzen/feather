package dev.maximilian.feather.multiservice

import dev.maximilian.feather.AppBuilder
import dev.maximilian.feather.Group
import dev.maximilian.feather.Permission
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.CredentialAuthorizer
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.multiservice.internals.Invitation
import dev.maximilian.feather.multiservice.mockserver.EmptyCheckUserEvent
import dev.maximilian.feather.multiservice.mockserver.GroupSyncMock
import dev.maximilian.feather.multiservice.mockserver.ServiceMock
import dev.maximilian.feather.multiservice.mockserver.SessionTerminatorMock
import dev.maximilian.feather.multiservice.mockserver.UserDeletionEventMock
import dev.maximilian.feather.multiservice.mockserver.UserRemovedCallbackChecker
import dev.maximilian.feather.multiservice.settings.MultiServiceConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.multiservice.settings.MultiserviceUserManagementSettings
import dev.maximilian.feather.multiservice.settings.TestMailSettings
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestGroup
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.HttpStatus
import kong.unirest.core.RequestBodyEntity
import java.util.UUID
import kotlin.test.Test

class InvitationApiTest {
    private val credentials = ServiceConfig.ACCOUNT_CONTROLLER
    private val apiUtilities = ApiTestUtilities()
    private val scenario = apiUtilities.startWithDummyService()
    private val group: Group

    init {
        val password = UUID.randomUUID().toString()
        val inviter = credentials.createUser(
            TestUser.generateTestUser(permissions = setOf(Permission.INVITE)),
            password = password,
        )
        apiUtilities.loginUser(inviter.username, password)

        group = credentials.createGroup(TestGroup.generateTestGroup(owners = setOf(inviter.id)))
    }

    private data class InvitationCreateRequest(
        val mail: String,
        val firstname: String,
        val surname: String,
        val groups: Set<Int>,
        val permissions: Set<String>,
        val verified: Boolean,
        val instructed: Boolean,
    )

    private fun sendInvitation(
        mail: String = "user@example.org",
        firstname: String = "Example",
        surname: String = "User",
        groups: Set<Int> = setOf(group.id),
        permissions: Set<String> = emptySet(),
        verified: Boolean = true,
        instructed: Boolean = true,
        basePath: String = scenario.basePath,
    ): RequestBodyEntity {
        println("sending invite with groups=$groups")
        return apiUtilities.restConnection.post("$basePath/invitations/")
            .body(InvitationCreateRequest(mail, firstname, surname, groups, permissions, verified, instructed))
    }

    private fun <B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})

    @Test
    fun `POST to invitations creates an invitation`() {
        val userEmail = UUID.randomUUID().toString() + "@example.org"

        val result = sendInvitation(mail = userEmail).asString()
        assert(result.status == HttpStatus.CREATED) { "${result.status} ${result.body}" }

        val invitations = apiUtilities.restConnection.get("${scenario.basePath}/invitations/")
            .asObject(object : GenericType<List<Invitation>>() {}).body
        assert(invitations.any { it.invitee.mail == userEmail })
    }

    @Test
    fun `POST to invitations requires verified = true`() {
        val response = sendInvitation(verified = false).asString()
        assert(!response.isSuccess)
    }

    @Test
    fun `POST to invitations requires instructed = true`() {
        val response = sendInvitation(instructed = false).asString()
        assert(!response.isSuccess)
    }

    @Test
    fun `Allow invitations without groups is configurable`() {
        // Copy the behavior of apiTestUtilities.startWithDummyServices
        val callbackEvents =
            MultiServiceEvents(
                GroupSyncMock(),
                listOf(UserDeletionEventMock()),
                emptyList(),
                SessionTerminatorMock(),
                EmptyCheckUserEvent(),
                emptyList(),
                emptyList(),
                listOf(UserRemovedCallbackChecker()),
            )
        val authorizationController = AuthorizationController(
            mutableSetOf(CredentialAuthorizer("credential", credentials, "http://127.0.0.1")),
            credentials,
            ApiTestUtilities.db,
            ActionController(ApiTestUtilities.db, credentials),
        )
        val originalConfig = MultiServiceConfig(
            credentials,
            BackgroundJobManager(ApiTestUtilities.pool),
            ApiTestUtilities.db,
            TestMailSettings.mailSetting,
            MultiserviceUserManagementSettings(invitationRequiresGroup = true),
            callbackEvents,
            GdprController(ApiTestUtilities.db, ActionController(ApiTestUtilities.db, credentials), credentials),
        )

        for (invitationRequiresGroup in arrayOf(true, false)) {
            val app = AppBuilder().createApp("test", "test-utilities", "http://127.0.0.1", emptySet(), 0)
            AuthorizerApi(app, authorizationController, true)
            val basePath = "${ApiTestUtilities.BASE_PATH_PREFIX}:${app.port()}${ApiTestUtilities.BASE_PATH_SUFFIX}"

            val config = originalConfig.copy(
                userSettings = originalConfig.userSettings.copy(
                    invitationRequiresGroup = invitationRequiresGroup,
                ),
            )

            val ms = Multiservice(config, credentials, HashMap())
            val testService = ServiceMock()
            ms.services.add(testService)
            ms.startApis(app)

            val response = sendInvitation(groups = setOf(), basePath = basePath).asEmpty()
            if (invitationRequiresGroup) {
                assert(response.status == HttpStatus.BAD_REQUEST) { response.status }
            } else {
                assert(response.isSuccess) { response.status }
            }
        }
    }
}
