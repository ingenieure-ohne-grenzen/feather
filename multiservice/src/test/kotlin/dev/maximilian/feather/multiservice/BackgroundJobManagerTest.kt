/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class BackgroundJobManagerTest {
    companion object {
        private const val TIMING_FACTOR = 20 // instead of 10 to avoid timing problems

        private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    }

    private suspend fun testBackgroundJob(
        id: UUID,
        value: Int,
    ): String {
        /* delays are chosen exorbitantly high because the Jedis mock server
         * is not multithreading-ready */
        delay(TIMING_FACTOR * 50L)
        backgroundJobManager.setJobStatus(id, "half-time")
        delay(TIMING_FACTOR * 50L)
        return value.toString()
    }

    @Test
    fun `Run simple background job`() {
        val job = backgroundJobManager.runBackgroundJob(this::testBackgroundJob, 5)
        assertEquals("started", job.status)
        assertNull(job.completed)

        val jobWorking = backgroundJobManager.getJobStatus(job.id)
        checkNotNull(jobWorking)
        assertEquals("started", jobWorking.status)
        assertNull(jobWorking.completed)

        runBlocking { delay(TIMING_FACTOR * 75L) }

        val jobHalfTime = backgroundJobManager.getJobStatus(job.id)
        checkNotNull(jobHalfTime)
        assertEquals("half-time", jobHalfTime.status)
        assertNull(jobHalfTime.completed)

        runBlocking { delay(TIMING_FACTOR * 50L) }

        val jobCompleted = backgroundJobManager.getJobStatus(job.id)
        checkNotNull(jobCompleted)
        assertEquals("completed", jobCompleted.status)
        assertNotNull(jobCompleted.completed)

        val jobResults = backgroundJobManager.getJobResults<String>(jobCompleted)
        checkNotNull(jobResults)
        assertEquals("5", jobResults)

        val jobResults2 = backgroundJobManager.getJobResultsAsString(jobCompleted)
        checkNotNull(jobResults2)
        assertEquals("\"5\"", jobResults2)
    }
}
