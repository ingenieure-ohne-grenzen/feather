/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.multiservice.api.internal.ChangePasswordRequest
import dev.maximilian.feather.testutils.ServiceConfig
import kong.unirest.core.ContentType
import kong.unirest.core.Empty
import kong.unirest.core.HttpResponse
import kong.unirest.core.MultipartBody
import org.eclipse.jetty.http.HttpStatus
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.util.UUID
import javax.imageio.ImageIO
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertNull

class AccountAPITest {
    private val apiUtilities = ApiTestUtilities()
    private val scenario = apiUtilities.startWithDummyService()
    private val credentialProvider = ServiceConfig.ACCOUNT_CONTROLLER

    @Test
    fun `POST to account-changepass with modified password leads to 204 (NO CONTENT)`() {
        val (_, password) = apiUtilities.createAndLoginUser()
        val newPassword = UUID.randomUUID().toString()
        val result = executePasswordChange(password, newPassword, newPassword)

        assertEquals(HttpStatus.NO_CONTENT_204, result.status)
    }

    @Test
    fun `POST to account-changepass with modified password leads to real password change`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val newPassword = UUID.randomUUID().toString()
        executePasswordChange(password, newPassword, newPassword)

        assertNull(credentialProvider.authenticateUserByUsernameOrMail(user.username, password))
        assertEquals(user.id, credentialProvider.authenticateUserByUsernameOrMail(user.username, newPassword)?.id)
    }

    @Test
    fun `POST to account-changepass with 7 signs leads to 400 (BAD REQUEST)`() {
        val (_, password) = apiUtilities.createAndLoginUser()
        val result = executePasswordChange(password, "1234567", "1234567")
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST to account-changepass with different passwords leads to 400 (BAD REQUEST)`() {
        val (_, password) = apiUtilities.createAndLoginUser()
        val result = executePasswordChange(password, UUID.randomUUID().toString(), UUID.randomUUID().toString())
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST to account-changepass with different passwords doesn't change password`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val newPassword = UUID.randomUUID().toString()
        executePasswordChange(password, newPassword, UUID.randomUUID().toString())

        assertNull(credentialProvider.authenticateUserByUsernameOrMail(user.username, newPassword))
        assertEquals(user.id, credentialProvider.authenticateUserByUsernameOrMail(user.username, password)?.id)
    }

    @Test
    fun `POST to account-changepass with wrong passwords doesn't change password`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val newPassword = UUID.randomUUID().toString()
        executePasswordChange(UUID.randomUUID().toString(), newPassword, newPassword)

        assertNull(credentialProvider.authenticateUserByUsernameOrMail(user.username, newPassword))
        assertEquals(user.id, credentialProvider.authenticateUserByUsernameOrMail(user.username, password)?.id)
    }

    @Test
    fun `POST to account-changepass with wrong passwords leads to 403 (FORBIDDEN)`() {
        apiUtilities.createAndLoginUser()
        val newPassword = UUID.randomUUID().toString()
        val result = executePasswordChange(UUID.randomUUID().toString(), newPassword, newPassword)
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `POST to account-setimage changes the image`() {
        val (user, _) = apiUtilities.createAndLoginUser()
        val image: ByteArray = makeImage(2, 2, "JPEG")

        val result = executeChangeImage(image, ContentType.IMAGE_JPEG).asString()
        assert(result.isSuccess) { result.body }

        // Large images will get scaled down and the content might not be equal. But for our 2x2 pixel image, that doesn't matter
        assertContentEquals(image, credentialProvider.getPhotoForUser(user))
        assertContentEquals(apiUtilities.restConnection.get("${scenario.basePath}/users/${user.id}/image").asBytes().body, image)
    }

    @Test
    fun `POST to account-setimage requires an image`() {
        apiUtilities.createAndLoginUser()
        val result: HttpResponse<String> =
            apiUtilities.restConnection.post("${scenario.basePath}/account/setimage")
                .asString()
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST to account-setimage accepts PNG images`() {
        apiUtilities.createAndLoginUser()
        val image = makeImage(1, 1, "PNG")
        val result = executeChangeImage(image, ContentType.IMAGE_PNG).asString()
        assert(result.isSuccess) { result.body }
    }

    @Test
    fun `POST to account-setimage with a too large image fails`() {
        apiUtilities.createAndLoginUser()
        val image = ByteArray(1024 * 1024) // 1 MB
        val result = executeChangeImage(image, ContentType.IMAGE_JPEG).asString()
        assert(!result.isSuccess)
        assert(
            result.body.contains("size", true)
                or result.body.contains("large", true)
                or (result.status == HttpStatus.PAYLOAD_TOO_LARGE_413),
        )
    }

    @Test
    fun `DELETE to account-setimage deletes the image`() {
        val (user, _) = apiUtilities.createAndLoginUser()
        val image: ByteArray = makeImage(1, 1, "JPEG")
        executeChangeImage(image, ContentType.IMAGE_JPEG).asString()
            .let { assert(it.isSuccess) { it.body } }

        apiUtilities.restConnection.delete("${scenario.basePath}/account/image").asEmpty()
            .let { assert(it.isSuccess) { it.body } }

        assertEquals(
            apiUtilities.restConnection.get("${scenario.basePath}/users/${user.id}/image").asEmpty().status,
            HttpStatus.NOT_FOUND_404,
        )
        assertNull(credentialProvider.getPhotoForUser(user))
    }

    @Test
    fun `DELETE non-existent profile photo fails`() {
        apiUtilities.createAndLoginUser()
        val result = apiUtilities.restConnection.delete("${scenario.basePath}/account/image").asEmpty()
        assertEquals(result.status, HttpStatus.NOT_FOUND_404)
    }

    private fun executePasswordChange(
        oldPassword: String,
        newPassword1: String,
        newPassword2: String,
    ): HttpResponse<Empty> {
        val changePasswordRequest =
            ChangePasswordRequest(
                oldPassword,
                newPassword1,
                newPassword2,
            )
        return apiUtilities.restConnection.post("${scenario.basePath}/account/changepass").body(changePasswordRequest).asEmpty()
    }

    /** Creates an image with suitable magic bytes */
    private fun makeImage(height: Int, width: Int, format: String) = ByteArrayOutputStream().also {
        ImageIO.write(
            BufferedImage(height, width, BufferedImage.TYPE_INT_RGB),
            format,
            it,
        )
    }.toByteArray()

    private fun executeChangeImage(
        image: ByteArray,
        contentType: ContentType,
    ): MultipartBody = apiUtilities.restConnection.post("${scenario.basePath}/account/setimage")
        .multiPartContent()
        .field("profileImage", image, contentType, "profileImage")
}
