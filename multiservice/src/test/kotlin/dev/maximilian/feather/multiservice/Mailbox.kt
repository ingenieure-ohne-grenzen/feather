package dev.maximilian.feather.multiservice

import dev.maximilian.feather.multiservice.internals.IMailSender
import dev.maximilian.feather.multiservice.internals.Mail
import dev.maximilian.feather.multiservice.settings.ChangeMailSetting
import dev.maximilian.feather.multiservice.settings.MailContentSettings
import dev.maximilian.feather.multiservice.settings.SendMailContent
import jakarta.mail.internet.InternetAddress

class Mailbox : IMailSender {
    private val unreadMail: MutableList<Mail> = mutableListOf()

    override fun submitMail(mail: Mail) {
        unreadMail.add(mail)
    }

    fun getUnreadMail(recipient: InternetAddress): List<Mail> {
        // TODO: this will probably not work as intended for mails with multiple recipients
        val result = unreadMail.filter { it.receiver.contains(recipient) }
        unreadMail.removeAll(result)
        return result
    }
}

fun mockEmailContent() = MailContentSettings(
    SendMailContent("You forgot your password", "{{ sitename }} -- Forgot password"),
    SendMailContent("You are invited", "{{ sitename }} -- Invitation"),
    ChangeMailSetting("Change mail", "{{ sitename }} -- Change mail", "notif1", "notif2"),
    SendMailContent(
        "Hallo {{ firstname }} {{ surname }} ({{ username }}), logge Dich bitte demnächst unter {{ baseurl }}/login ein. Dates: {{ accountDeactivationDate }}, {{ accountDeletionDate }}. MfG, {{ botname }}",
        "{{ sitename }} -- Inactivity",
    ) to SendMailContent(
        "Hallo {{ firstname }} {{ surname }} ({{ username }}), du wurdest deaktiviert; bitte gehe zu {{ baseurl }}/reactivate ein. Dates: {{ accountDeactivationDate }}, {{ accountDeletionDate }}. MfG, {{ botname }}",
        "{{ sitename }} -- Deactivation",
    ),
)
