package dev.maximilian.feather.multiservice

import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.multiservice.internals.EmailTemplates
import dev.maximilian.feather.multiservice.internals.inactiveusers.InactiveAccountController
import dev.maximilian.feather.multiservice.internals.inactiveusers.ReactivateAction
import dev.maximilian.feather.multiservice.internals.inactiveusers.dateFormatter
import dev.maximilian.feather.multiservice.internals.inactiveusers.toInstant
import dev.maximilian.feather.multiservice.internals.inactiveusers.toLocalDate
import dev.maximilian.feather.multiservice.settings.ChangeMailSetting
import dev.maximilian.feather.multiservice.settings.InactiveUsersSettings
import dev.maximilian.feather.multiservice.settings.MailContentSettings
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import dev.maximilian.feather.multiservice.settings.SMTPSetting
import dev.maximilian.feather.multiservice.settings.SendMailContent
import dev.maximilian.feather.multiservice.settings.WriteMailSettings
import dev.maximilian.feather.testutils.InMemoryExternalCredentialProvider
import dev.maximilian.feather.testutils.TestGroup
import dev.maximilian.feather.testutils.TestUser
import jakarta.mail.internet.InternetAddress
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.DriverManager
import java.time.Duration
import java.time.Instant
import java.time.InstantSource
import java.time.LocalDate
import java.time.Period
import java.util.UUID
import kotlin.test.Test

class MockClock(
    var time: Instant = Instant.EPOCH,
) : InstantSource {
    override fun instant() = time
}

class InactiveUserTest {
    private val dbName = UUID.randomUUID().toString()
    private val database =
        Database.connect(
            getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") },
            DatabaseConfig.invoke { defaultRepetitionAttempts = 0 },
        )
    private val accountController =
        AccountController(database, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))
    private val actionController = ActionController(database, accountController)
    private val mailbox = Mailbox()

    private val mailSettings = MultiServiceMailSettings(
        sender = WriteMailSettings(
            botname = "Feather Bot",
            siteName = "Feather - Test",
            baseUrl = "http://feather.example.org",
        ),
        smtp = SMTPSetting("", "", "", "", "", ""),
        senderAddress = InternetAddress("bot@example.org", "Test bot"),
        content = MailContentSettings(
            forgot = SendMailContent("", ""),
            invite = SendMailContent("", ""),
            change = ChangeMailSetting("", "", "", ""),
            inactivityReminders = SendMailContent(
                "Hello {{ firstname }} {{ surname }} ({{ username }}), you will be deactivated on <{{ accountDeactivationDate }}> and deleted on <{{ accountDeletionDate }}>, unless you go to {{ baseUrl }}/login. Yours, {{ botname }}",
                "{{ sitename }} - Inactivity Reminder",
            ) to SendMailContent(
                "Hello {{ firstname }} {{ surname }} ({{ username }}), you have been deactivated and will be deleted on <{{ accountDeletionDate }}>, unless you go to {{ baseUrl }}/reactivate. Yours, {{ botname }}",
                "{{ sitename }} - Account deactivated",
            ),
            // dates are enclosed in brackets to help parsing
        ),
    )
    private val protectedGroup = accountController.createGroup(TestGroup.generateTestGroup(name = "protectedGroup"))
    private val settings = InactiveUsersSettings(protectedGroup = protectedGroup.name)
    private val persistentProperties = HashMap<String, String>()
    private val clock = MockClock(Instant.now())
    private val controller = InactiveAccountController(
        settings,
        accountController,
        mailbox,
        mailSettings,
        EmailTemplates(mailSettings.content, mailSettings.sender),
        persistentProperties,
        actionController,
        clock,
    )

    private fun createUser(lastLoginAt: Instant, createdAt: Instant = Instant.EPOCH): User {
        val user = accountController.createUser(TestUser.generateTestUser())
        // created users always have a timestamp of (never), so we need to fix them manually
        // but the wrapper class AccountDatabase is not public, so we have to do this directly in SQL
        transaction(database) {
            exec("UPDATE users SET last_login_at = '$lastLoginAt', created_at = '$createdAt' WHERE ID = ${user.id};")
        }
        return user
    }

    private fun createUsers(): Pair<User, User> {
        val thresholdDate = LocalDate.now().minus(Period.ofMonths(6)).toInstant()
        return createUser(thresholdDate.minus(Duration.ofHours(6))) to createUser(thresholdDate.plus(Period.ofDays(1)))
    }

    private fun User.address() = InternetAddress(mail, username)

    @Test
    fun `Users receive an email after 6 months of inactivity`() {
        val (oldUser, newUser) = createUsers()

        controller.remindInactiveUsers()
        assert(mailbox.getUnreadMail(oldUser.address()).size == 1)
        assert(mailbox.getUnreadMail(newUser.address()).isEmpty())
    }

    @Test
    fun `Regular user is deactivated`() {
        val lastLoginAt = Instant.EPOCH
        val createdAt = Instant.EPOCH
        val oldUser = createUser(lastLoginAt, createdAt)
        accountController.updateUser(oldUser)
        // fix user last login again
        transaction(database) {
            exec("UPDATE users SET last_login_at = '$lastLoginAt', created_at = '$createdAt' WHERE ID = ${oldUser.id};")
        }
        controller.disableInactiveUsers()
        assert(actionController.getOpenUserActions(oldUser).any { it is ReactivateAction })
    }

    fun check_if_user_does_not_get_removed(do_something_with_user: (User) -> User) {
        val lastLoginAt = Instant.EPOCH
        val createdAt = Instant.EPOCH
        val oldUser = createUser(lastLoginAt, createdAt)
        accountController.updateUser(do_something_with_user(oldUser))
        // fix user last login again
        transaction(database) {
            exec("UPDATE users SET last_login_at = '$lastLoginAt', created_at = '$createdAt' WHERE ID = ${oldUser.id};")
        }
        controller.disableInactiveUsers()
        assert(actionController.getOpenUserActions(oldUser).isEmpty())
    }

    @Test
    fun `Function accounts do not get deactivated`() {
        check_if_user_does_not_get_removed { user ->
            user.copy(permissions = user.permissions.plusElement(Permission.FUNCTION_ACCOUNT))
        }
    }

    @Test
    fun `Admins do not get deactivated`() {
        check_if_user_does_not_get_removed { user ->
            user.copy(permissions = user.permissions.plusElement(Permission.ADMIN))
        }
    }

    @Test
    fun `Central office members do not get deactivated`() {
        check_if_user_does_not_get_removed { user ->
            user.copy(groups = user.groups.plusElement(protectedGroup.id))
        }
    }

    @Test
    fun `Users don't receive reminder twice`() {
        val (oldUser, _) = createUsers()

        controller.remindInactiveUsers()
        mailbox.getUnreadMail(oldUser.address())
        controller.remindInactiveUsers()
        assert(mailbox.getUnreadMail(oldUser.address()).isEmpty())
    }

    @Test
    fun `Reminder contains the correct dates`() {
        val (oldUser, _) = createUsers()

        controller.remindInactiveUsers()
        val mail = mailbox.getUnreadMail(oldUser.address()).single()

        val givenDeactivationDate = mail.body.getDatePrecededBy("deactivated on")
        val expectedDeactivationDate = LocalDate.now().plus(Period.ofMonths(settings.deactivationAfterReminderInMonths))
        assert(expectedDeactivationDate == givenDeactivationDate)

        val expectedDeletionDate = expectedDeactivationDate.plus(Period.ofMonths(settings.deletionAfterDeactivationInMonths))
        val givenDeletionDate = mail.body.getDatePrecededBy("deleted on")
        assert(expectedDeletionDate == givenDeletionDate)
    }

    @Test
    fun `Users are deactivated after 8 months of inactivity`() {
        val oldUser = createUser(LocalDate.now().minus(Period.ofMonths(8)).toInstant())

        controller.disableInactiveUsers()
        val mail = mailbox.getUnreadMail(oldUser.address()).single()
        assert(mail.body.contains("/reactivate"))
        assert(actionController.getOpenUserActions(oldUser).any { it is ReactivateAction })
    }

    @Test
    fun `Users don't receive second reminder twice`() {
        val oldUser = createUser(LocalDate.now().minus(Period.ofMonths(8)).toInstant())

        controller.disableInactiveUsers()
        mailbox.getUnreadMail(oldUser.address()).single()
        controller.disableInactiveUsers()
        assert(mailbox.getUnreadMail(oldUser.address()).isEmpty())
    }

    // see test mail body above; dates are always enclosed in <brackets>
    private fun String.getDatePrecededBy(pattern: String): LocalDate? =
        Regex("$pattern <(.+?)>")
            .find(this).let {
                if (it == null) {
                    throw Error("$pattern <(.+?)> not found in `$this`")
                } else {
                    it
                }
            }.groupValues[1] // 0 is the whole substring
            .let { LocalDate.parse(it, dateFormatter) }

    @Test
    fun `Second reminder contains the correct dates`() {
        val oldUser = createUser(LocalDate.now().minus(Period.ofMonths(8)).minus(Period.ofDays(1)).toInstant())

        controller.disableInactiveUsers()
        val mail = mailbox.getUnreadMail(oldUser.address()).single()

        val expectedDeletionDate = LocalDate.now().plus(Period.ofMonths(settings.deletionAfterDeactivationInMonths))
        val givenDeletionDate = mail.body.getDatePrecededBy("deleted on")
        assert(expectedDeletionDate == givenDeletionDate)

        assert(mail.body.contains("${mailSettings.sender.baseUrl}/reactivate")) { mail.body }
    }

    @Test
    fun `User gets deactivated on date given in email`() {
        // This is a bit of a duplicate of all the tests above
        val user = createUser(clock.instant())

        val beforeReminderDate = clock.instant().toLocalDate().plus(Period.ofMonths(6))
        val beforeDeactivationDate = beforeReminderDate.plus(Period.ofMonths(2))

        clock.time = beforeReminderDate.toInstant()
        controller.remindInactiveUsers()
        controller.disableInactiveUsers()
        assert(mailbox.getUnreadMail(user.address()).isEmpty())

        clock.time += Period.ofDays(1)
        controller.remindInactiveUsers()
        controller.disableInactiveUsers()
        val mail = mailbox.getUnreadMail(user.address()).single()
        val givenDeactivationDate = mail.body.getDatePrecededBy("deactivated on")
        assert(givenDeactivationDate == beforeDeactivationDate.plus(Period.ofDays(1)))

        clock.time = beforeDeactivationDate.toInstant()
        controller.remindInactiveUsers()
        controller.disableInactiveUsers()
        assert(mailbox.getUnreadMail(user.address()).isEmpty())
        assert(actionController.getOpenUserActions(user).isEmpty())

        clock.time += Period.ofDays(1)
        controller.remindInactiveUsers()
        controller.disableInactiveUsers()
        assert(mailbox.getUnreadMail(user.address()).singleOrNull() != null)
        assert(actionController.getOpenUserActions(user).any { it is ReactivateAction })
    }
}
