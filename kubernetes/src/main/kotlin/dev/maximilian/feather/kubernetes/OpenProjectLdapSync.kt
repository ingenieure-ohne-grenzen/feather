package dev.maximilian.feather.kubernetes

import mu.KotlinLogging

public class OpenProjectLdapSync(private val kubernetes: IKubernetes, namespaceName: String, deploymentName: String, private val containerName: String) {
    private val logger = KotlinLogging.logger {}

    private val namespace =
        requireNotNull(kubernetes.getNamespaceByName(namespaceName)) {
            "Cannot find k8s namespace $namespaceName for OpenProject sync"
        }

    private val deployment =
        requireNotNull(kubernetes.getDeploymentByName(namespace, deploymentName)) {
            "Cannot find k8s deployment $deploymentName in namespace $namespaceName for OpenProject sync"
        }

    public fun synchronizeOpenProjectGroupsWithLDAP() {
        // NOTE: this will take more than 10 seconds, maybe even up to a minute
        // Simple retry if fails in first try
        runCatching { executeShellCommand() }.getOrElse { executeShellCommand() }
    }

    private fun executeShellCommand() {
        val replicaSet =
            checkNotNull(
                kubernetes.getReplicaSetsByDeployment(deployment).maxByOrNull {
                    it.deploymentRevision
                },
            ) { "Newest replica sets from OpenProject deployment not found" }
        val pods = kubernetes.getPodsByReplicaSet(replicaSet)
        val readyPod =
            checkNotNull(pods.firstOrNull { it.ready }) {
                "No ready pod found for OpenProject deployment"
            }

        // This command can consume time, wait 5 minutes maximum.
        val result = kubernetes.executeCommand(readyPod, containerName, arrayOf("bundle", "exec", "rails", "ldap_groups:synchronize"), 300)

        require(!result.failure) {
            "There was a failure while synchronizing OpenProject. Logs:${System.lineSeparator()}${result.messages.joinToString(
                separator = System.lineSeparator(),
            )}"
        }

        if (!result.failure) {
            logger.info { "OpenProject successfully synchronized" }
        }
    }
}
