package dev.maximilian.feather.kubernetes

public data class Deployment(
    val name: String,
    val namespace: Namespace,
)
