package dev.maximilian.feather.kubernetes

public data class Namespace(
    val name: String,
)
