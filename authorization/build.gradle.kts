/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

plugins {
    alias(libs.plugins.kotlin.serialization)
}

dependencies {
    // Library
    implementation(project(":lib"))

    // Database ORM Framework
    implementation(libs.bundles.exposed)

    // Rest framework
    implementation(libs.javalin.core)
    implementation(libs.javalin.openapi.core)
    kapt(libs.javalin.openapi.kapt)

    // Rest Client
    implementation(libs.bundles.jacksonAndUnirest)

    // ktor client core
    implementation(libs.ktor.client.core)

    // OIDC driver
    implementation(libs.oidc)

    // Redis driver
    implementation(libs.jedis)

    // LDAP driver
    implementation(libs.directory)

    // Text template compiler
    implementation(libs.mustache)

    // Im memory database
    testImplementation(libs.h2)
}
