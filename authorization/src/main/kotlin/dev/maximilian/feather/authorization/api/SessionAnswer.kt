package dev.maximilian.feather.authorization.api

import dev.maximilian.feather.User
import java.time.Instant
import java.util.UUID

data class SessionAnswer(
    val id: UUID,
    val validUntil: Instant,
    val user: User,
    val authorizer: String,
    val miniSession: Boolean,
)
