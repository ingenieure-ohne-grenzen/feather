# Feather backend

A web application API microservice ("backend") for the lightweight user administration tool named "Feather".

## Project setup

Install OpenJDK 17 as a prerequisite to compile and use this Kotlin microservice.

### Compile

```[bash]
./gradlew clean assemble testClasses installDist
```

### Run and test

If a postgres DB and redis cache are available, you can run the microservice locally. The environment variable `DATABASE_URL=jdbc:postgresql://<host>:5432/<db>?user=<user>&password=<password>` should point to the postgres DB and the DB will be filled with default properties. Adjust these as necesary.

```[bash]
build/install/feather/bin/feather
```

Best run the [Feather frontend](https://gitlab.com/maxemann96/featherFrontend) parallel with and appropriate setting in order to perform live tests.

Look at [.gitlab-ci.yml](.gitlab-ci.yml) to see the automatic tests that are run for each commit using Gitlab's CI/CD pipeline.

For more information on the tests, see the [test description](./integration/README.md).

### Lints and fixes files

Download 'ktlint' from <https://github.com/pinterest/ktlint/releases/latest/download/ktlint> if not yet done.

```[bash]
ktlint -F
```

**Install the pre-commit-hook** to mitigate commits with message `ktlint -F`. This is done by `ktlint installGitPreCommitHook`.

## License

Licensed under [Apache 2.0](LICENSE)

## Contribution

Very welcome!

1. [Fork it](https://gitlab.com/maxemann96/feather/-/forks/new)
1. Create your feature branch (git checkout -b my-new-feature)
1. Commit your changes (git commit -am 'Add some feature')
   * In your first commit, add your name and email address (if you like) to `AUTHORS.md`.
1. Push to the branch (git push origin my-new-feature)
1. Create a new Pull Request
