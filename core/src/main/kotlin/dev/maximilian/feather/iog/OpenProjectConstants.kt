/*
 *    Copyright [2021] Maximilian Hippler <hello@maximilian.dev>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

internal object OpenProjectConstants {
    const val BASEURL: String = "https://openproject.maximilian.dev"
    const val AUTOMATION_USERNAME: String = ""
    const val APIKEY: String = ""
    const val APIKEYUSER = "apikey"
    const val SSO_SECRET: String = ""
    const val USER_BINDING: String = "id"
    const val PUBLICURL: String = ""
}
