package dev.maximilian.feather.api

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.Permission
import dev.maximilian.feather.iog.ServiceFactory
import dev.maximilian.feather.multiservice.Multiservice
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.sessionOrNull
import io.javalin.Javalin
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.UnauthorizedResponse
import io.micrometer.prometheusmetrics.PrometheusMeterRegistry
import kotlin.reflect.KClass

internal class MetricsApi(
    app: Javalin,
    private val prometheusRegistry: PrometheusMeterRegistry,
    private val basicAuthPassword: String? = null,
    multiservice: Multiservice,
) {
    init {
        data class Service(val name: String, val clazz: KClass<out IControllableService>, val enabled: Boolean)
        for (service in arrayOf(
            Service("openproject", OpenProjectService::class, ServiceFactory.isOpenProjectEnabled()),
            Service("nextcloud", NextcloudService::class, ServiceFactory.isNextcloudEnabled()),
        )) {
            fun Boolean.toInt() = if (this) 1 else 0

            prometheusRegistry.gauge("feather_${service.name}_enabled", service.enabled.toInt())
            prometheusRegistry.gauge("feather_${service.name}_connected", multiservice.services.any { service.clazz.isInstance(it) }.toInt())
        }

        app.get("/metrics", ::getMetrics)
    }

    private fun getMetrics(ctx: Context) {
        val session = ctx.sessionOrNull()

        // Admin user is allowed to see metrics
        // Also plain basic auth with metrics user and predefined token is allowed
        if (session?.user?.permissions?.contains(Permission.ADMIN) != true) {
            val credentials = ctx.basicAuthCredentials()

            if (basicAuthPassword == null || credentials == null) {
                if (session != null) {
                    throw ForbiddenResponse()
                } else {
                    throw UnauthorizedResponse()
                }
            }

            if (credentials.username != "metrics" || credentials.password != basicAuthPassword) {
                throw UnauthorizedResponse()
            }
        }

        ctx.contentType("text/plain; version=0.0.4; charset=utf-8").result(prometheusRegistry.scrape())
    }
}
