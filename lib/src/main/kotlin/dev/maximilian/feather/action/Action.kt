package dev.maximilian.feather.action

public interface Action<T> {
    public val name: String // the user has to respond to that action by navigating to /{name} in the front-end
    public val userId: Int
    public val description: String
    public val payload: T

    public fun payloadToString(): String
}
