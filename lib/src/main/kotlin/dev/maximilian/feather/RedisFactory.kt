/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

// import mu.KLogging
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig
import redis.clients.jedis.Protocol

public class RedisFactory(propertyMap: MutableMap<String, String>) {
    private val host = propertyMap.getOrPut("redis.host") { RedisConstants.HOST }
    private val port = propertyMap.getOrPut("redis.port") { RedisConstants.PORT }
    private val useTls = propertyMap.getOrPut("redis.tls") { RedisConstants.TLS }
    private val password = propertyMap.getOrPut("redis.password") { RedisConstants.PASSWORD }
    private val timeout = propertyMap.getOrPut("redis.timeout") { RedisConstants.TIMEOUT }

    public fun create(): JedisPool {
        val validatedHost = host.trim()
        require(validatedHost.isNotBlank()) { "Redis host must not be blank" }
        require(port.trim().isNotBlank()) { "Redis port must not be blank" }
        val validatedPort = port.trim().toIntOrNull()
        require(validatedPort != null) { "Redis port is not numeric" }
        require(0 < validatedPort && validatedPort < 65536) { "Redis port is not in range (1 - 65535)" }

        val validatedUseTls = validateBoolean(useTls)
        require(validatedUseTls != null) { "TLS should be enabled (true or 1, recommended) or disabled (false or 0)" }

        require(timeout.trim().isNotBlank()) { "Redis timeout must not be blank" }
        val validatedTimeout = timeout.trim().toIntOrNull()
        require(validatedTimeout != null) { "Redis timeout is not numeric" }

        return JedisPool(
            JedisPoolConfig(),
            validatedHost,
            validatedPort,
            validatedTimeout,
            password.takeIf { it.isNotBlank() },
            validatedUseTls,
        )
    }

    private fun validateBoolean(unvalidated: String): Boolean? =
        when (unvalidated.trim()) {
            "true", "1" -> true
            "false", "0" -> false
            else -> null
        }
}

internal object RedisConstants {
    const val HOST: String = Protocol.DEFAULT_HOST
    const val PORT: String = Protocol.DEFAULT_PORT.toString()
    const val TIMEOUT: String = Protocol.DEFAULT_TIMEOUT.toString()
    const val PASSWORD: String = ""
    const val TLS: String = "false"
}
