/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.account

import dev.maximilian.feather.Group
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import mu.KotlinLogging
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Sequence
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.CurrentTimestamp
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.javatime.timestampLiteral
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.jgrapht.alg.cycle.TarjanSimpleCycles
import org.jgrapht.graph.SimpleDirectedGraph
import java.time.Instant

internal class AccountDatabase(private val db: Database) {
    private val logger = KotlinLogging.logger { }
    internal val userIdColumn: Column<EntityID<Int>> = UserTable.id
    private val sequence = Sequence("user_group_id_sequence")

    init {
        transaction(db) {
            SchemaUtils.createSequence(sequence)
            SchemaUtils.createMissingTablesAndColumns(
                UserTable,
                PermissionTable,
                GroupTable,
                GroupUserMembershipsTable,
                GroupGroupMembershipsTable,
            )
        }
    }

    private fun getUsers(where: SqlExpressionBuilder.() -> Op<Boolean>): List<User> = transaction(db) {
        val columns = UserTable.columns.filter { it != UserTable.jpegPhoto }
        UserTable.slice(columns).select(where).toUsers()
    }

    internal fun getUsers(): List<User> = transaction(db) {
        val columns = UserTable.columns.filter { it != UserTable.jpegPhoto }
        UserTable.slice(columns).selectAll().toUsers()
    }

    internal fun getUserById(id: Int): User? = getUsers { UserTable.id eq id }.singleOrNull()

    internal fun getUserByExternalId(id: String): User? = getUsers { UserTable.externalProviderId eq id }.singleOrNull()

    internal fun getUserExternalId(user: User): String? = transaction(db) {
        UserTable.slice(UserTable.id, UserTable.externalProviderId)
            .select { UserTable.id eq user.id }
            .singleOrNull()
            ?.let { it[UserTable.externalProviderId] }
    }

    internal fun updatePermissionsByExternalId(input: Map<String, Set<Permission>>) {
        transaction(db) {
            val mappedUserIds = getUserExternalIdIdMap()
            PermissionTable.deleteAll()
            PermissionTable.batchInsert(input.mapNotNull { (externalId, permissions) -> mappedUserIds[externalId]?.let { it to permissions } }.flatMap { it.second.map { p -> it.first to p } }) {
                this[PermissionTable.userId] = it.first
                this[PermissionTable.permission] = it.second.toString()
            }
        }
    }

    @JvmName("associateUsersByExternalIds")
    internal fun associateByExternalIds(users: Set<User>): Map<String, User> = transaction(db) {
        val idExternalIdMap: Map<Int, String> = UserTable.slice(UserTable.id, UserTable.externalProviderId).select {
            UserTable.id inList users.map { it.id }
        }.associate {
            it[UserTable.id].value to it[UserTable.externalProviderId]
        }

        users.mapNotNull { u -> idExternalIdMap[u.id]?.let { it to u } }.toMap()
    }

    @JvmName("associateGroupsByExternalIds")
    internal fun associateByExternalIds(groups: Set<Group>): Map<String, Group> = transaction(db) {
        val idExternalIdMap: Map<Int, String> = GroupTable.slice(GroupTable.id, GroupTable.externalProviderId).select {
            GroupTable.id inList groups.map { it.id }
        }.associate {
            it[GroupTable.id].value to it[GroupTable.externalProviderId]
        }

        groups.mapNotNull { g -> idExternalIdMap[g.id]?.let { it to g } }.toMap()
    }

    internal fun getUserByMail(mail: String): User? = getUsers { UserTable.mail eq mail }.singleOrNull()

    internal fun getUserByUsername(username: String): User? = getUsers { UserTable.username eq username }.singleOrNull()

    internal fun getPhotoForUser(user: User): ByteArray? = transaction(db) {
        UserTable.slice(UserTable.jpegPhoto).select { UserTable.id eq user.id }
            .singleOrNull()?.get(UserTable.jpegPhoto)?.bytes
    }

    /**
     * Returns the credential provider id of the user or null if the user cannot be found in the database
     */
    internal fun getExternalProviderId(user: User): String? = transaction(db) {
        UserTable.slice(UserTable.externalProviderId).select { UserTable.id eq user.id }
            .map { it[UserTable.externalProviderId] }
    }.singleOrNull()

    internal fun createUser(user: User, externalProviderId: String): User {
        require(user.id == 0) {
            "Could not create user. Id already set. $user"
        }

        val validatedUser = user.copy(groups = user.groups + user.ownedGroups)

        return try {
            transaction(db) {
                val userResult = UserTable.insert {
                    it[username] = validatedUser.username
                    it[displayName] = validatedUser.displayName
                    it[firstname] = validatedUser.firstname
                    it[surname] = validatedUser.surname
                    it[mail] = validatedUser.mail
                    it[disabled] = validatedUser.disabled
                    it[createdAt] = validatedUser.registeredSince
                    it[UserTable.externalProviderId] = externalProviderId
                }
                val generatedResults = UserTable.slice(UserTable.id, UserTable.lastLoginAt, UserTable.createdAt)
                    .select { UserTable.id eq userResult[UserTable.id].value }.single()

                val permissionResult = updateUserPermissions(userResult[UserTable.id].value, user.permissions)
                val groupResult = updateGroupMembers(
                    userResult[UserTable.id].value,
                    validatedUser.groups.map { it }.toSet(),
                    validatedUser.ownedGroups.map { it }.toSet(),
                )

                User(
                    id = userResult[UserTable.id].value,
                    username = userResult[UserTable.username],
                    displayName = userResult[UserTable.displayName],
                    firstname = userResult[UserTable.firstname],
                    surname = userResult[UserTable.surname],
                    mail = userResult[UserTable.mail],
                    registeredSince = generatedResults[UserTable.createdAt],
                    lastLoginAt = generatedResults[UserTable.lastLoginAt],
                    permissions = permissionResult,
                    groups = groupResult.first,
                    ownedGroups = groupResult.second,
                    disabled = userResult[UserTable.disabled],
                ).also {
                    logger.debug("Created User $it")
                }
            }
        } catch (e: ExposedSQLException) {
            if (e.errorCode == 23505) {
                throw IllegalArgumentException(
                    "The user has a username, an email or externalProviderId which is already taken. $user",
                    e,
                )
            } else {
                throw e
            }
        }
    }

    /**
     * Deletes the user.
     * If the user does not exist (anymore) nothing will be changed and no exception will be thrown.
     */
    internal fun deleteUser(user: User) {
        transaction(db) {
            UserTable.deleteWhere { UserTable.id eq user.id }
        }

        logger.debug { "Deleted user $user" }
    }

    /**
     * Updates a user in out database.
     * It is checked that the user exists.
     * The createdAt field is ignored and only set at creation time.
     * the fields lastLoginAt and externalProviderId can only be updated with their respective methods and are also ignored.
     */
    internal fun updateUser(user: User): User {
        val freshUser = requireNotNull(getUserById(user.id)) {
            "User with id ${user.id} does not exist and cannot be updated."
        }

        val validatedUser = user.copy(
            groups = user.groups + user.ownedGroups,
        )

        return try {
            transaction {
                val permissionResult = if (freshUser.permissions != validatedUser.permissions) {
                    updateUserPermissions(validatedUser.id, validatedUser.permissions)
                } else {
                    null
                }

                val groupResult =
                    if (freshUser.groups != validatedUser.groups || freshUser.ownedGroups != validatedUser.ownedGroups) {
                        updateGroupMembers(
                            validatedUser.id,
                            validatedUser.groups.map { it }.toSet(),
                            validatedUser.ownedGroups.map { it }.toSet(),
                        )
                    } else {
                        null
                    }

                if (freshUser.copy(
                        permissions = emptySet(),
                        groups = emptySet(),
                        ownedGroups = emptySet(),
                    ) != validatedUser.copy(permissions = emptySet(), groups = emptySet(), ownedGroups = emptySet())
                ) {
                    UserTable.update(where = { UserTable.id eq validatedUser.id }) {
                        it[username] = validatedUser.username
                        it[displayName] = validatedUser.displayName
                        it[firstname] = validatedUser.firstname
                        it[surname] = validatedUser.surname
                        it[mail] = validatedUser.mail
                        it[disabled] = validatedUser.disabled
                        it[createdAt] = validatedUser.registeredSince
                    }
                }

                validatedUser.copy(
                    permissions = permissionResult ?: freshUser.permissions,
                    groups = groupResult?.first ?: freshUser.groups,
                    ownedGroups = groupResult?.second ?: freshUser.ownedGroups,
                    lastLoginAt = freshUser.lastLoginAt,
                ).also {
                    logger.debug { "Updated user $it" }
                }
            }
        } catch (e: ExposedSQLException) {
            if (e.errorCode == 23505) {
                throw IllegalArgumentException(
                    "The user cannot be updated because the username or email is already taken. $user",
                    e,
                )
            } else {
                throw e
            }
        }
    }

    /**
     * Updates the user last login time to current time.
     * If the user does not exist (anymore) no exception is thrown and null is returned
     */
    internal fun updateLastLoginTimestamp(user: User): User? = transaction(db) {
        UserTable.update(where = { UserTable.id eq user.id }) {
            it[lastLoginAt] = Instant.now()
        }
    }.let { getUserById(user.id) }

    /**
     * Updates the user credential provider id to the specified string
     * If the user does not exist (anymore) nothing will be changed and no exception will be thrown.
     */
    internal fun updateExternalProviderId(user: User, externalProviderId: String): User = try {
        transaction(db) {
            UserTable.update(where = { UserTable.id eq user.id }) {
                it[UserTable.externalProviderId] = externalProviderId
            }
        }
    } catch (e: ExposedSQLException) {
        if (e.errorCode == 23505) {
            throw IllegalArgumentException(
                "The user externalProviderId cannot be updated because it is already taken by another user. $externalProviderId. $user",
                e,
            )
        } else {
            throw e
        }
    }.let { user.also { logger.debug { "Updated external id of user ${user.id}" } } }

    internal fun updatePhotoForUser(user: User, jpegPhoto: ByteArray) = transaction(db) {
        UserTable.update(where = { UserTable.id eq user.id }) {
            it[UserTable.jpegPhoto] = ExposedBlob(jpegPhoto)
        }
    }

    internal fun deletePhotoForUser(user: User) = transaction(db) {
        UserTable.update(where = { UserTable.id eq user.id }) {
            it[jpegPhoto] = null
        }
    }

    internal fun createGroup(group: Group, externalProviderId: String): Group {
        require(group.id == 0) {
            "Could not create group. Id already set. $group"
        }

        val validatedGroup = group.copy(
            userMembers = group.userMembers + group.owners,
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups,
        )

        ensureNoCycles(validatedGroup)

        val groupId = try {
            transaction(db) {
                GroupTable.insertAndGetId {
                    it[name] = validatedGroup.name
                    it[description] = validatedGroup.description
                    it[GroupTable.externalProviderId] = externalProviderId
                }.value.also { updateGroupMembers(validatedGroup.copy(id = it)) }
            }
        } catch (e: ExposedSQLException) {
            if (e.errorCode == 23505) {
                throw IllegalArgumentException(
                    "The group has a name or externalProviderId which is already taken. $group",
                    e,
                )
            } else {
                throw e
            }
        }

        return validatedGroup.copy(id = groupId).also {
            logger.debug { "Created group $it" }
        }
    }

    internal fun getUserExternalIdIdMap(): Map<String, Int> = transaction(db) {
        UserTable.slice(UserTable.id, UserTable.externalProviderId).selectAll().associate {
            it[UserTable.externalProviderId] to it[UserTable.id].value
        }
    }

    internal fun getUserIdExternalIdMap(): Map<Int, String> = transaction(db) {
        UserTable.slice(UserTable.id, UserTable.externalProviderId).selectAll().associate {
            it[UserTable.id].value to it[UserTable.externalProviderId]
        }
    }

    internal fun getGroupExternalIdIdMap(): Map<String, Int> = transaction(db) {
        GroupTable.slice(GroupTable.id, GroupTable.externalProviderId).selectAll().associate {
            it[GroupTable.externalProviderId] to it[GroupTable.id].value
        }
    }

    internal fun getGroupIdExternalIdMap(): Map<Int, String> = transaction(db) {
        GroupTable.slice(GroupTable.id, GroupTable.externalProviderId).selectAll().associate {
            it[GroupTable.id].value to it[GroupTable.externalProviderId]
        }
    }

    internal fun getGroup(id: Int): Group? = getGroups { GroupTable.id eq id }.singleOrNull()

    /**
     * Returns the credential provider id of the user or null if the user cannot be found in the database
     */
    internal fun getExternalProviderId(group: Group): String? = transaction(db) {
        GroupTable.slice(GroupTable.externalProviderId).select { GroupTable.id eq group.id }
            .map { it[GroupTable.externalProviderId] }
    }.singleOrNull()

    internal fun getGroupByExternalId(id: String): Group? =
        getGroups { GroupTable.externalProviderId eq id }.singleOrNull()

    internal fun getGroupByName(name: String): Group? = getGroups { GroupTable.name eq name }.singleOrNull()

    internal fun updateExternalProviderId(group: Group, externalProviderId: String): Group = try {
        transaction(db) {
            GroupTable.update(where = { GroupTable.id eq group.id }) {
                it[GroupTable.externalProviderId] = externalProviderId
            }
        }
    } catch (e: ExposedSQLException) {
        if (e.errorCode == 23505) {
            throw IllegalArgumentException(
                "The group externalProviderId cannot be updated because it is already taken by another group. $externalProviderId. $group",
                e,
            )
        } else {
            throw e
        }
    }.let { group.also { logger.debug { "Updated external id from group ${group.id}" } } }

    internal fun getGroups(): List<Group> = getGroups { Op.TRUE }

    internal fun updateGroup(group: Group): Group {
        val freshGroup = requireNotNull(getGroup(group.id)) {
            "User with id ${group.id} does not exist and cannot be updated."
        }
        val validatedGroup = group.copy(
            userMembers = group.userMembers + group.owners,
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups,
        )

        ensureNoCycles(validatedGroup)

        transaction(db) {
            // We need to update basic group data
            try {
                if (validatedGroup.name != freshGroup.name || validatedGroup.description != freshGroup.description) {
                    GroupTable.update(where = { GroupTable.id eq validatedGroup.id }) {
                        it[name] = validatedGroup.name
                        it[description] = validatedGroup.description
                    }
                }
            } catch (e: ExposedSQLException) {
                if (e.errorCode == 23505) {
                    throw IllegalArgumentException(
                        "The group cannot be updated because the name is already taken. $group",
                        e,
                    )
                } else {
                    throw e
                }
            }

            // We need to update memberships
            if (validatedGroup.copy(name = freshGroup.name, description = freshGroup.description) != freshGroup) {
                updateGroupMembers(validatedGroup)
            }
        }

        return validatedGroup.also {
            logger.debug { "Updated group $it" }
        }
    }

    internal fun deleteGroup(group: Group) {
        transaction(db) {
            // All memberships are cascading deleted
            GroupTable.deleteWhere { GroupTable.id eq group.id }
        }

        logger.debug { "Deleted group $group" }
    }

    private fun getGroups(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Group> = transaction(db) {
        val basicData: List<Triple<Int, String, String>> = GroupTable.select(where)
            .map { Triple(it[GroupTable.id].value, it[GroupTable.name], it[GroupTable.description]) }
        val userMemberships =
            GroupUserMembershipsTable.select { GroupUserMembershipsTable.group inList basicData.map { it.first } }.map {
                Triple(
                    it[GroupUserMembershipsTable.group].value,
                    it[GroupUserMembershipsTable.user].value,
                    it[GroupUserMembershipsTable.owner],
                )
            }
        val groupMemberships =
            GroupGroupMembershipsTable.select { (GroupGroupMembershipsTable.group inList basicData.map { it.first }) or (GroupGroupMembershipsTable.memberGroup inList basicData.map { it.first }) }
                .map {
                    Triple(
                        it[GroupGroupMembershipsTable.group].value,
                        it[GroupGroupMembershipsTable.memberGroup].value,
                        it[GroupGroupMembershipsTable.owner],
                    )
                }

        basicData.map { (groupId, name, description) ->
            Group(
                id = groupId,
                name = name,
                description = description,
                userMembers = userMemberships.filter { (id, _, owner) -> groupId == id && !owner }
                    .map { (_, id, _) -> id }.toSet(),
                groupMembers = groupMemberships.filter { (id, _, owner) -> groupId == id && !owner }
                    .map { (_, id, _) -> id }.toSet(),
                owners = userMemberships.filter { (id, _, owner) -> groupId == id && owner }
                    .map { (_, id, _) -> id }.toSet(),
                ownedGroups = groupMemberships.filter { (id, _, owner) -> groupId == id && owner }
                    .map { (_, id, _) -> id }.toSet(),
                parentGroups = groupMemberships.filter { (_, id, owner) -> groupId == id && !owner }
                    .map { (id, _, _) -> id }.toSet(),
                ownerGroups = groupMemberships.filter { (_, id, owner) -> groupId == id && owner }
                    .map { (id, _, _) -> id }.toSet(),
            )
        }
    }

    private data class Edge(
        val source: Int,
        val target: Int,
    )

    private fun updateUserPermissions(userId: Int, permissions: Set<Permission>): Set<Permission> {
        PermissionTable.deleteWhere { PermissionTable.userId eq userId }
        return PermissionTable.batchInsert(permissions) {
            this[PermissionTable.userId] = userId
            this[PermissionTable.permission] = it.name
        }.map {
            Permission.valueOf(it[PermissionTable.permission])
        }.toSet()
    }

    private fun Group.groupMembershipTripleSet() = (
        groupMembers.map {
            Triple(id, it, false)
        } + ownedGroups.map {
            Triple(id, it, true)
        } + parentGroups.map {
            // I am the child, so for the other group it is an owner connection
            Triple(it, id, false)
        } + ownerGroups.map {
            // I am the owner, so for the other group it is a child connection
            Triple(it, id, true)
        }
        ).toSet()

    private fun updateGroupMembers(userId: Int, memberIn: Set<Int>, ownerIn: Set<Int>): Pair<Set<Int>, Set<Int>> {
        GroupUserMembershipsTable.deleteWhere { GroupUserMembershipsTable.user eq userId }

        // Every group a user/group is admin, the entity is also member
        val userMemberships = memberIn.map { it to false } + ownerIn.map { it to true }

        val result = GroupUserMembershipsTable.batchInsert(userMemberships) {
            this[GroupUserMembershipsTable.group] = it.first
            this[GroupUserMembershipsTable.user] = userId
            this[GroupUserMembershipsTable.owner] = it.second
        }.map {
            it[GroupUserMembershipsTable.group].value to it[GroupUserMembershipsTable.owner]
        }

        return result.filter { !it.second }.map { it.first }.toSet() to result.filter { it.second }.map { it.first }
            .toSet()
    }

    private fun updateGroupMembers(group: Group) {
        GroupGroupMembershipsTable.deleteWhere { GroupGroupMembershipsTable.group eq group.id }
        GroupGroupMembershipsTable.deleteWhere { GroupGroupMembershipsTable.memberGroup eq group.id }
        GroupUserMembershipsTable.deleteWhere { GroupUserMembershipsTable.group eq group.id }

        // Every group a user/group is admin, the entity is also member
        val userMemberships = group.userMembers.map {
            it to false
        } + group.owners.map {
            it to true
        }

        val groupMemberships: Set<Triple<Int, Int, Boolean>> = group.groupMembershipTripleSet()

        GroupUserMembershipsTable.batchInsert(userMemberships) {
            this[GroupUserMembershipsTable.group] = group.id
            this[GroupUserMembershipsTable.user] = it.first
            this[GroupUserMembershipsTable.owner] = it.second
        }

        GroupGroupMembershipsTable.batchInsert(groupMemberships) {
            this[GroupGroupMembershipsTable.group] = it.first
            this[GroupGroupMembershipsTable.memberGroup] = it.second
            this[GroupGroupMembershipsTable.owner] = it.third
        }
    }

    private fun ensureNoCycles(groupToChange: Group) {
        transaction(db) {
            val groupBasicInfoMap = GroupTable.selectAll().associate {
                it[GroupTable.id].value to it[GroupTable.name]
            }.toMutableMap()

            if (groupToChange.id == 0) {
                // Special case, group not created yet
                groupBasicInfoMap[0] = groupToChange.name
            }

            val allMemberships: Set<Triple<Int, Int, Boolean>> = (
                GroupGroupMembershipsTable.select {
                    (GroupGroupMembershipsTable.group neq groupToChange.id) and (GroupGroupMembershipsTable.memberGroup neq groupToChange.id)
                }.map {
                    Triple(
                        it[GroupGroupMembershipsTable.group].value,
                        it[GroupGroupMembershipsTable.memberGroup].value,
                        it[GroupGroupMembershipsTable.owner],
                    )
                } + groupToChange.groupMembershipTripleSet()
                ).toSet()

            require(allMemberships.all { (group, memberGroup, _) -> group != memberGroup }) {
                "No self loops in group memberships allowed"
            }

            // We need to make a cycle search two times
            // One round for the owner connections
            // and the other round for the member connections
            val ownersGraph = SimpleDirectedGraph<Int, Edge>(null, null, false).apply {
                groupBasicInfoMap.keys.forEach { addVertex(it) }
                allMemberships.filter { it.third }.forEach {
                    require(groupBasicInfoMap.contains(it.first)) {
                        "Cannot find group with id ${it.first} in group map while building dependency graph for $groupToChange"
                    }
                    require(groupBasicInfoMap.contains(it.second)) {
                        "Cannot find group with id ${it.second} in group map while building dependency graph for $groupToChange"
                    }
                    addEdge(it.first, it.second, Edge(it.first, it.second))
                }
            }
            val membersGraph = SimpleDirectedGraph<Int, Edge>(null, null, false).apply {
                groupBasicInfoMap.keys.forEach { addVertex(it) }
                allMemberships.filter { !it.third }.forEach {
                    require(groupBasicInfoMap.contains(it.first)) {
                        "Cannot find group with id ${it.first} in group map while building dependency graph for $groupToChange"
                    }
                    require(groupBasicInfoMap.contains(it.second)) {
                        "Cannot find group with id ${it.second} in group map while building dependency graph for $groupToChange"
                    }
                    addEdge(it.first, it.second, Edge(it.first, it.second))
                }
            }
            val cycles =
                TarjanSimpleCycles(ownersGraph).findSimpleCycles() + TarjanSimpleCycles(membersGraph).findSimpleCycles()

            require(cycles.isEmpty()) {
                val formatCycle: (List<Int>) -> String = {
                    it.joinToString(prefix = "[", postfix = "]") { s -> groupBasicInfoMap[s] ?: s.toString() }
                }
                "Group memberships are cyclic, which is not allowed. Cycles found: ${
                    cycles.joinToString(
                        prefix = "[",
                        postfix = "]",
                        transform = formatCycle,
                    )
                }"
            }
        }
    }

    private fun Iterable<ResultRow>.toUsers(): List<User> {
        val idSet = this.map { it[UserTable.id] }.toSet()
        val mappedPermissions: Map<Int, List<String>> = PermissionTable.select { PermissionTable.userId inList idSet }
            .map { it[PermissionTable.userId] to it[PermissionTable.permission] }.groupBy { it.first.value }
            .mapValues { it.value.map { e -> e.second } }
        val mappedGroups: List<Triple<Int, Int, Boolean>> =
            GroupUserMembershipsTable.select { GroupUserMembershipsTable.user inList idSet }
                .map { Triple(it[GroupUserMembershipsTable.user].value, it[GroupUserMembershipsTable.group].value, it[GroupUserMembershipsTable.owner]) }

        return map { row ->
            row.toUser(
                mappedPermissions,
                mappedGroups.filter { row[UserTable.id].value == it.first && !it.third }.map { it.second }.toSet(),
                mappedGroups.filter { row[UserTable.id].value == it.first && it.third }.map { it.second }.toSet(),
            )
        }
    }

    private fun ResultRow.toUser(permissions: Map<Int, List<String>>, groups: Set<Int>, ownedGroups: Set<Int>) = User(
        id = this[UserTable.id].value,
        username = this[UserTable.username],
        displayName = this[UserTable.displayName],
        firstname = this[UserTable.firstname],
        surname = this[UserTable.surname],
        mail = this[UserTable.mail],
        registeredSince = this[UserTable.createdAt],
        disabled = this[UserTable.disabled],
        groups = groups,
        ownedGroups = ownedGroups,
        permissions = permissions[this[UserTable.id].value]?.map { Permission.valueOf(it) }?.toSet() ?: emptySet(),
        lastLoginAt = this[UserTable.lastLoginAt],
    )

    private object UserTable : IdTable<Int>("users") {
        override val id: Column<EntityID<Int>> = integer("id").autoIncrement("user_group_id_sequence").entityId()
        override val primaryKey: PrimaryKey = PrimaryKey(id)
        val username: Column<String> = varchar("username", 255).uniqueIndex()
        val displayName: Column<String> = varchar("display_name", 255)
        val firstname: Column<String> = varchar("firstname", 255)
        val surname: Column<String> = varchar("surname", 255)
        val mail: Column<String> = varchar("mail", 255).uniqueIndex()
        val createdAt: Column<Instant> = timestamp("created_at").defaultExpression(CurrentTimestamp())
        val lastLoginAt: Column<Instant> =
            timestamp("last_login_at").defaultExpression(timestampLiteral(Instant.ofEpochSecond(0)))
        val disabled: Column<Boolean> = bool("disabled")
        val externalProviderId: Column<String> = varchar("external_provider_id", 255).uniqueIndex()
        val jpegPhoto: Column<ExposedBlob?> = blob("jpegPhoto").nullable()

        init {
            uniqueIndex(username, mail)
        }
    }

    private object GroupTable : IdTable<Int>("groups") {
        override val id: Column<EntityID<Int>> = integer("id").autoIncrement("user_group_id_sequence").entityId()
        override val primaryKey: PrimaryKey = PrimaryKey(id)
        val name: Column<String> = varchar("name", 255).uniqueIndex()
        val description: Column<String> = text("description")
        val externalProviderId: Column<String> = varchar("external_provider_id", 255).uniqueIndex()
    }

    private object GroupUserMembershipsTable : Table("groups_memberships_users") {
        val group: Column<EntityID<Int>> = reference("group_id", GroupTable.id, onDelete = ReferenceOption.CASCADE)
        val user: Column<EntityID<Int>> = reference("member_id", UserTable.id, onDelete = ReferenceOption.CASCADE)
        val owner: Column<Boolean> = bool("is_owner")

        init {
            uniqueIndex(group, user, owner)
        }
    }

    private object GroupGroupMembershipsTable : Table("groups_memberships_groups") {
        val group: Column<EntityID<Int>> = reference("group_id", GroupTable.id, onDelete = ReferenceOption.CASCADE)
        val memberGroup: Column<EntityID<Int>> =
            reference("member_group_id", GroupTable.id, onDelete = ReferenceOption.CASCADE)
        val owner: Column<Boolean> = bool("is_owner")

        init {
            uniqueIndex(group, memberGroup, owner)
        }
    }

    private object PermissionTable : Table("users_permissions") {
        val userId: Column<EntityID<Int>> = reference("user_id", UserTable.id, onDelete = ReferenceOption.CASCADE)
        val permission: Column<String> = varchar("permission", 40)

        override val primaryKey: PrimaryKey = PrimaryKey(userId, permission)
    }
}
