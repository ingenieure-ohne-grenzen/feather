/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.account

import dev.maximilian.feather.Permission
import dev.maximilian.feather.TableUserGroupIdMigrator
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import kotlin.system.exitProcess

internal class AccountMigration(
    private val db: Database,
    private val accountDatabase: AccountDatabase,
    private val properties: MutableMap<String, String>,
) {
    private val logger = KotlinLogging.logger { }

    internal fun checkAndDoMigration(externalPermissions: Map<String, Set<Permission>>) {
        // All users and groups are imported now, migrate old ldap id stuff
        // This is a dangerous operation, so starting with feather 1.3.0 a database property must be set and is checked
        // The migration is handled in 2 parts
        // MIGRATE_OLD_IDS = 0 only creates the temporary columns with the corrected numerical ids
        // MIGRATE_OLD_IDS = 1 does the dangerous operation of removing all columns with a null user id (aka user/group was deleted before this update happens)
        // MIGRATE_OLD_IDS = 42 means that the migration is completed and feather starts normally. DO NOT MANUALLY SET THIS VALUE!

        if (properties["MIGRATE_OLD_IDS"] == "42") return
        val dryRun = properties["MIGRATE_OLD_IDS"] != "1"

        val userExternalIdIdMap = accountDatabase.getUserExternalIdIdMap()
        val groupExternalIdIdMap = accountDatabase.getGroupExternalIdIdMap()

        TableUserGroupIdMigrator.migrateTable(db, "actions", "id", "user_id", userExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateTable(db, "change_mail_address", "id", "userId", userExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateTable(db, "gdpr_user_acceptance", "user_id", "user_id", userExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateTable(db, "sessions", "id", "userId", userExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateTable(db, "lost_password", "id", "inviter", userExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateTable(db, "requests", "id", "requesterId", userExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateTable(db, "keycloak_actions", "id", "user_id", userExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateInviteTable(db, userExternalIdIdMap, groupExternalIdIdMap, dryRun)
        TableUserGroupIdMigrator.migrateTrialAndSupportMemberTable(db, userExternalIdIdMap, groupExternalIdIdMap, dryRun)

        accountDatabase.updatePermissionsByExternalId(externalPermissions)

        if (dryRun) {
            logger.error { "MIGRATION NEEDED, CHECK DATABASE FOR WEIRD RESULTS AND START MIGRATION WITH SETTING MIGRATE_OLD_IDS = 1" }
            exitProcess(0)
        }

        properties["MIGRATE_OLD_IDS"] = "42"
    }
}
