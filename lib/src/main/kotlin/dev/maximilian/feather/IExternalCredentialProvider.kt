/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

import java.io.Closeable

public interface IExternalCredentialProvider : Closeable {
    public fun getPermissions(): Map<String, Set<Permission>>

    public fun createUser(
        user: ExternalUser,
        password: String? = null,
    ): ExternalUser

    public fun getUserById(userId: String): ExternalUser?

    public fun getUsersAndGroups(): Pair<Collection<ExternalUser>, Collection<ExternalGroup>>

    /**
     * Returns the id of the authenticated user
     */
    public fun authenticateUserByUsernameOrMail(
        usernameOrMail: String,
        password: String,
    ): String?

    public fun updateUserPassword(
        externalId: String,
        newPassword: String,
    ): Boolean

    public fun getPhotoForUser(externalId: String): ByteArray?

    public fun updatePhotoForUser(
        externalId: String,
        image: ByteArray,
    )

    public fun deletePhotoForUser(externalId: String)

    public fun deleteUser(externalId: String)

    public fun updateUser(user: ExternalUser): ExternalUser?

    public fun createGroup(group: ExternalGroup): ExternalGroup

    public fun getGroupById(id: String): ExternalGroup?

    public fun updateGroup(group: ExternalGroup): ExternalGroup?

    public fun deleteGroup(externalId: String)
}
