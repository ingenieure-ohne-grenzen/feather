/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

public data class Group(
    val id: Int,
    val name: String,
    val description: String,
    val userMembers: Set<Int>,
    val groupMembers: Set<Int>,
    val parentGroups: Set<Int>,
    val owners: Set<Int>,
    val ownerGroups: Set<Int>,
    val ownedGroups: Set<Int>,
) {
    /* LDAP inheritance: a user inherits access from the groups that it is member of
     * and those groups inherit access from their parent group */

    // Cyclic membership protection is ensured in account controller
    public fun hasOwnerRights(
        userId: Int,
        credentialProvider: ICredentialProvider,
    ): Boolean =
        owners.contains(userId) ||
            ownerGroups.any {
                val group = credentialProvider.getGroup(it)
                group?.isUserMemberRecursive(userId, credentialProvider) ?: false
            }

    public fun isParentGroup(
        groupId: Int,
        credentialProvider: ICredentialProvider,
    ): Boolean =
        parentGroups.contains(groupId) ||
            parentGroups.any {
                val group = credentialProvider.getGroup(it)
                group?.isParentGroup(groupId, credentialProvider) ?: false
            }

    public fun isOwnerGroup(
        groupId: Int,
        credentialProvider: ICredentialProvider,
    ): Boolean =
        ownerGroups.contains(groupId) ||
            ownerGroups.any {
                val group = credentialProvider.getGroup(it)
                group?.isOwnerGroup(groupId, credentialProvider) ?: false
            }

    public fun isGroupMemberRecursive(
        groupId: Int,
        credentialProvider: ICredentialProvider,
        depthCount: Int = 0,
    ): Boolean =
        groupMembers.contains(groupId) ||
            groupMembers.any {
                val group = credentialProvider.getGroup(it)
                if (depthCount > 15) {
                    throw Exception("Cycling dependency in groupMembers by $groupId!")
                }
                group?.isGroupMemberRecursive(groupId, credentialProvider, depthCount + 1) ?: false
            }

    public fun isOwnedGroupRecursive(
        groupId: Int,
        credentialProvider: ICredentialProvider,
        depthCount: Int = 0,
    ): Boolean =
        ownedGroups.contains(groupId) ||
            ownedGroups.any {
                val group = credentialProvider.getGroup(it)
                if (depthCount > 15) {
                    throw Exception("Cycling dependency in ownedGroups by $groupId!")
                }
                group?.isOwnedGroupRecursive(groupId, credentialProvider, depthCount + 1) ?: false
            }

    public fun isUserMemberRecursive(
        userId: Int,
        credentialProvider: ICredentialProvider,
        depthCount: Int = 0,
    ): Boolean =
        userMembers.contains(userId) ||
            // implicit membership via the child groups
            groupMembers.any {
                val group = credentialProvider.getGroup(it)
                if (depthCount > 15) {
                    throw Exception("Cycling dependency in userMember by $userId!")
                }
                group?.isUserMemberRecursive(userId, credentialProvider, depthCount + 1) ?: false
            }
}
