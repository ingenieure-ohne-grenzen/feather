package dev.maximilian.feather.gdpr

import dev.maximilian.feather.User
import java.time.Instant

public data class GdprAcceptance(
    val user: User,
    val timestamp: Instant,
    val document: GdprDocument,
    val type: GdprAcceptanceType,
)
