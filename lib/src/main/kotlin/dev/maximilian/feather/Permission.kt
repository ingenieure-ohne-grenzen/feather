/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

import io.javalin.security.RouteRole

public enum class Permission : RouteRole {
    ADMIN,
    INVITE,
    USER,
    RESET_SANDBOX,
    UPLOAD_SUPPORT_MEMBER,
    MANAGE_GDPR,
    FUNCTION_ACCOUNT,
}

public fun User.requireAdminPermission(action: String = "unknown") {
    if (!this.permissions.contains(Permission.ADMIN)) {
        throw PermissionException(this, action)
    }
}

public fun User.requirePermission(
    action: String,
    oneOf: Set<Permission>,
) {
    if (oneOf.isNotEmpty() && this.permissions.intersect(oneOf).isEmpty()) {
        throw PermissionException(this, action)
    }
}

public fun User.requireManageGDPRPermission(action: String = "unknown") {
    if (this.permissions.intersect(setOf(Permission.ADMIN, Permission.MANAGE_GDPR)).isEmpty()) {
        throw PermissionException(this, action)
    }
}
