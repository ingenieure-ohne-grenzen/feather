/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.gdpr

import java.time.Duration
import java.time.Instant
import java.util.UUID

internal fun generateTestGdprDocument(
    id: Int = 0,
    creationDate: Instant = Instant.ofEpochSecond(0),
    forceDate: Instant = Instant.now().plus(Duration.ofDays(35)),
    validDate: Instant = Instant.now().plus(Duration.ofDays(70)),
    content: String =
        """
        # Some Markdown
        formatted text with a random string to distinguish between the generated documents: **${UUID.randomUUID()}**
        """.trimIndent(),
) = GdprDocument(
    id = id,
    creationDate = creationDate,
    forceDate = forceDate,
    validDate = validDate,
    content = content,
)
