/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.account

import dev.maximilian.feather.testutils.InMemoryExternalCredentialProvider
import dev.maximilian.feather.testutils.TestGroup
import dev.maximilian.feather.testutils.TestUser
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import java.sql.DriverManager
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class GroupTest {
    private val dbName = UUID.randomUUID().toString()
    private val database =
        Database.connect(
            getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") },
            DatabaseConfig.invoke { defaultRepetitionAttempts = 0 },
        )

    private val accountController =
        AccountController(database, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))
    private val credentialProvider = accountController

    @Test
    fun `Test implicit membership`() {
        val users = (0..3).map { credentialProvider.createUser(TestUser.generateTestUser()) }
        val createdParentGroup =
            credentialProvider.createGroup(
                TestGroup.generateTestGroup(
                    userMembers = setOf(users[0].id),
                    owners = setOf(users[1].id),
                ),
            )

        val createdChildGroup =
            credentialProvider.createGroup(
                TestGroup.generateTestGroup().copy(
                    userMembers = setOf(users[2].id),
                    owners = setOf(users[3].id),
                    parentGroups = setOf(createdParentGroup.id),
                ),
            )

        val getParentGroup = credentialProvider.getGroup(createdParentGroup.id)
        val getChildGroup = credentialProvider.getGroup(createdChildGroup.id)
        assertNotNull(getParentGroup)
        assertNotNull(getChildGroup)

        assertFalse(getParentGroup.hasOwnerRights(users[0].id, credentialProvider))
        assertTrue(getParentGroup.hasOwnerRights(users[1].id, credentialProvider))
        assertFalse(getParentGroup.hasOwnerRights(users[3].id, credentialProvider))
        assertFalse(getChildGroup.hasOwnerRights(users[1].id, credentialProvider))
        assertFalse(getChildGroup.hasOwnerRights(users[2].id, credentialProvider))
        assertTrue(getChildGroup.hasOwnerRights(users[3].id, credentialProvider))

        assertTrue(getChildGroup.isParentGroup(createdParentGroup.id, credentialProvider))
        assertFalse(getParentGroup.isParentGroup(createdChildGroup.id, credentialProvider))

        assertTrue(getParentGroup.isGroupMemberRecursive(createdChildGroup.id, credentialProvider))
        assertFalse(getChildGroup.isGroupMemberRecursive(createdParentGroup.id, credentialProvider))

        assertTrue(getParentGroup.isUserMemberRecursive(users[0].id, credentialProvider))
        assertTrue(getParentGroup.isUserMemberRecursive(users[2].id, credentialProvider))
        assertFalse(getChildGroup.isUserMemberRecursive(users[0].id, credentialProvider))
        assertTrue(getChildGroup.isUserMemberRecursive(users[2].id, credentialProvider))
    }

    @Test
    fun `Test implicit ownership`() {
        val users = (0..1).map { credentialProvider.createUser(TestUser.generateTestUser()) }
        val owningGroup =
            TestGroup.generateTestGroup().copy(
                userMembers = setOf(users[0].id),
            )
        val createdOwningGroup = credentialProvider.createGroup(owningGroup)

        val ownedGroup =
            TestGroup.generateTestGroup().copy(
                userMembers = setOf(users[1].id),
                ownerGroups = setOf(createdOwningGroup.id),
            )
        val createdOwnedGroup = credentialProvider.createGroup(ownedGroup)

        val getOwningGroup = credentialProvider.getGroup(createdOwningGroup.id)
        val getOwnedGroup = credentialProvider.getGroup(createdOwnedGroup.id)
        assertNotNull(getOwningGroup)
        assertNotNull(getOwnedGroup)

        assertTrue(getOwningGroup.isUserMemberRecursive(users[0].id, credentialProvider))
        assertFalse(getOwningGroup.isUserMemberRecursive(users[1].id, credentialProvider))
        assertFalse(getOwnedGroup.userMembers.contains(users[0].id))
        assertTrue(getOwnedGroup.isUserMemberRecursive(users[0].id, credentialProvider))
        assertTrue(getOwnedGroup.isUserMemberRecursive(users[1].id, credentialProvider))

        assertFalse(getOwningGroup.hasOwnerRights(users[0].id, credentialProvider))
        assertFalse(getOwningGroup.hasOwnerRights(users[1].id, credentialProvider))
        assertTrue(getOwnedGroup.hasOwnerRights(users[0].id, credentialProvider))
        assertFalse(getOwnedGroup.hasOwnerRights(users[1].id, credentialProvider))
    }
}
