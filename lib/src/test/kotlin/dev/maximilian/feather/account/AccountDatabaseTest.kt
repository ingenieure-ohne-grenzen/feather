/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.account

import dev.maximilian.feather.Group
import dev.maximilian.feather.Permission
import dev.maximilian.feather.testutils.TestGroup
import dev.maximilian.feather.testutils.TestUser
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import java.sql.DriverManager
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class AccountDatabaseTest {
    private val dbName = UUID.randomUUID().toString()
    private val database =
        Database.connect(
            getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") },
            DatabaseConfig.invoke { defaultRepetitionAttempts = 0 },
        )
    private val accountDatabase = AccountDatabase(database)

    @Test
    fun `Create user works and ignores lastLoginAt property`() {
        val memberGroup = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val ownedGroup = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        val setTimestamp = Instant.now().truncatedTo(ChronoUnit.MINUTES).minusSeconds(180)
        val testUser =
            TestUser.generateTestUser(
                registeredSince = setTimestamp,
                lastLoginAt = setTimestamp,
                permissions = setOf(Permission.INVITE, Permission.USER),
                groups = setOf(memberGroup.id),
                ownedGroups = setOf(ownedGroup.id),
            )
        val created = accountDatabase.createUser(testUser, UUID.randomUUID().toString())
        val fresh = accountDatabase.getUserById(created.id)

        assert(created.id > 0) { "User id was not set on user creation" }
        assertEquals(
            // User is member in all groups owned
            testUser.copy(groups = setOf(memberGroup.id, ownedGroup.id)),
            created.copy(id = 0, registeredSince = setTimestamp, lastLoginAt = setTimestamp),
            "Created user is not equal to user to create",
        )
        assertEquals(Instant.ofEpochSecond(0), created.lastLoginAt, "User last login was not set to 0 on user creation")
        assertEquals(created, fresh)
    }

    @Test
    fun `Create user with set id fails`() {
        val existing = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createUser(existing, UUID.randomUUID().toString())
        }.apply {
            assertEquals("Could not create user. Id already set. $existing", message)
        }
    }

    @Test
    fun `Create user with same mail, username or credentialProviderId address fails`() {
        val existingExternalId = UUID.randomUUID().toString()
        val existing = accountDatabase.createUser(TestUser.generateTestUser(), existingExternalId)

        val usernameTakenUser = TestUser.generateTestUser(mail = existing.mail)
        val mailTakenUser = TestUser.generateTestUser(username = existing.username)
        val externalProviderIdTakenUser = TestUser.generateTestUser()

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createUser(usernameTakenUser, UUID.randomUUID().toString())
        }.apply {
            assertEquals(
                "The user has a username, an email or externalProviderId which is already taken. $usernameTakenUser",
                message,
            )
        }

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createUser(mailTakenUser, UUID.randomUUID().toString())
        }.apply {
            assertEquals(
                "The user has a username, an email or externalProviderId which is already taken. $mailTakenUser",
                message,
            )
        }

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createUser(externalProviderIdTakenUser, existingExternalId)
        }.apply {
            assertEquals(
                "The user has a username, an email or externalProviderId which is already taken. $externalProviderIdTakenUser",
                message,
            )
        }
    }

    @Test
    fun `Get User by id works`() {
        val created =
            accountDatabase.createUser(
                TestUser.generateTestUser(
                    permissions = setOf(Permission.INVITE, Permission.USER),
                ),
                UUID.randomUUID().toString(),
            )
        val read = accountDatabase.getUserById(created.id)

        assertEquals(created, read)
        assertNull(accountDatabase.getUserById(-42))
    }

    @Test
    fun `Get User by username works`() {
        val created = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())
        val read = accountDatabase.getUserByUsername(created.username)

        assertEquals(created, read)
        assertNull(accountDatabase.getUserByUsername("not_existing"))
    }

    @Test
    fun `Get User by mail works`() {
        val created = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())
        val read = accountDatabase.getUserByMail(created.mail)

        assertEquals(created, read)
        assertNull(accountDatabase.getUserByMail("not_existing@example.org"))
    }

    @Test
    fun `Get User by externalId works`() {
        val externalId = UUID.randomUUID().toString()
        val created = accountDatabase.createUser(TestUser.generateTestUser(), externalId)
        val read = accountDatabase.getUserByExternalId(externalId)

        assertEquals(created, read)
        assertNull(accountDatabase.getUserByExternalId("not_existing"))
    }

    @Test
    fun `Get Users works`() {
        val created =
            (0 until 5).map {
                accountDatabase.createUser(
                    TestUser.generateTestUser(
                        permissions = setOf(if (it <= 2) Permission.INVITE else Permission.USER),
                    ),
                    UUID.randomUUID().toString(),
                )
            }.toSet()
        val read = accountDatabase.getUsers().toSet()

        assertEquals(created, created.intersect(read))
    }

    @Test
    fun `Get User credentialProviderId works`() {
        val externalProviderId = UUID.randomUUID().toString()
        val created = accountDatabase.createUser(TestUser.generateTestUser(), externalProviderId)

        assertEquals(externalProviderId, accountDatabase.getExternalProviderId(created))
        accountDatabase.deleteUser(created)
        assertNull(accountDatabase.getExternalProviderId(created))
    }

    @Test
    fun `Delete User works`() {
        val created = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())
        accountDatabase.deleteUser(created)
        assertNull(accountDatabase.getUserById(created.id))
    }

    @Test
    fun `Update lastLoginTimestamp works`() {
        val created = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())

        val now = Instant.now()
        val updatedUser = accountDatabase.updateLastLoginTimestamp(created)
        val after = Instant.now()

        assertNotNull(updatedUser)
        assertEquals(
            created,
            updatedUser.copy(lastLoginAt = created.lastLoginAt),
            "Update user last login time changed more user fields than expected",
        )
        assert(now <= updatedUser.lastLoginAt && updatedUser.lastLoginAt <= after)

        accountDatabase.deleteUser(updatedUser)
        assertNull(accountDatabase.updateLastLoginTimestamp(updatedUser))
    }

    @Test
    fun `Update credentialProviderId works`() {
        val credentialProviderIdUser1 = UUID.randomUUID().toString()
        val credentialProviderIdUser2 = UUID.randomUUID().toString()
        val updatedCredentialProviderId = UUID.randomUUID().toString()

        val createdUser1 = accountDatabase.createUser(TestUser.generateTestUser(), credentialProviderIdUser1)
        accountDatabase.createUser(TestUser.generateTestUser(), credentialProviderIdUser2)

        val updated = accountDatabase.updateExternalProviderId(createdUser1, updatedCredentialProviderId)

        assertNotNull(updated)
        assertEquals(createdUser1, updated, "Update user externalProviderId changed more user fields than expected")
        assertEquals(updatedCredentialProviderId, accountDatabase.getExternalProviderId(updated))

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.updateExternalProviderId(updated, credentialProviderIdUser2)
        }.apply {
            assertEquals(
                "The user externalProviderId cannot be updated because it is already taken by another user. $credentialProviderIdUser2. $updated",
                message,
            )
        }
    }

    @Test
    fun `Update user works`() {
        val memberGroup = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val ownedGroup = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        val created =
            accountDatabase.createUser(
                TestUser.generateTestUser(
                    permissions = setOf(Permission.ADMIN),
                    groups =
                    setOf(
                        accountDatabase.createGroup(
                            TestGroup.generateTestGroup(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                    ownedGroups =
                    setOf(
                        accountDatabase.createGroup(
                            TestGroup.generateTestGroup(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                ),
                UUID.randomUUID().toString(),
            )
        val secondUser =
            accountDatabase.createUser(
                TestUser.generateTestUser(permissions = setOf(Permission.ADMIN)),
                UUID.randomUUID().toString(),
            )
        val updatedUser =
            created.copy(
                username = UUID.randomUUID().toString(),
                displayName = UUID.randomUUID().toString(),
                firstname = UUID.randomUUID().toString(),
                surname = UUID.randomUUID().toString(),
                mail = UUID.randomUUID().toString() + "@example.org",
                registeredSince = Instant.now().truncatedTo(ChronoUnit.MINUTES).minusSeconds(180),
                permissions = setOf(Permission.USER),
                lastLoginAt = Instant.now().truncatedTo(ChronoUnit.MINUTES).minusSeconds(180),
                disabled = true,
                groups = setOf(memberGroup.id),
                ownedGroups = setOf(ownedGroup.id),
            )
        val updatedSavedUser = accountDatabase.updateUser(updatedUser)
        val freshUser = accountDatabase.getUserById(updatedSavedUser.id)

        assertEquals(
            updatedUser.copy(lastLoginAt = created.lastLoginAt, groups = setOf(memberGroup.id, ownedGroup.id)),
            updatedSavedUser,
        )
        assertEquals(freshUser, updatedSavedUser)

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.updateUser(updatedSavedUser.copy(username = secondUser.username))
        }.apply {
            assertEquals(
                "The user cannot be updated because the username or email is already taken. ${
                    updatedSavedUser.copy(
                        username = secondUser.username,
                    )
                }",
                message,
            )
        }

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.updateUser(updatedSavedUser.copy(mail = secondUser.mail))
        }.apply {
            assertEquals(
                "The user cannot be updated because the username or email is already taken. ${
                    updatedSavedUser.copy(
                        mail = secondUser.mail,
                    )
                }",
                message,
            )
        }
    }

    @Test
    fun `Update non existing user throws`() {
        val created = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())
        accountDatabase.deleteUser(created)
        assertFailsWith<IllegalArgumentException> {
            accountDatabase.updateUser(
                created.copy(
                    displayName = UUID.randomUUID().toString(),
                ),
            )
        }.apply {
            assertEquals("User with id ${created.id} does not exist and cannot be updated.", message)
        }
    }

    @Test
    fun `Update user photo works`() {
        val externalProviderId = UUID.randomUUID().toString()
        val created = accountDatabase.createUser(TestUser.generateTestUser(), externalProviderId)
        val photo: ByteArray = Random.Default.nextBytes(10)

        assertNull(accountDatabase.getPhotoForUser(created))
        accountDatabase.updatePhotoForUser(created, photo)
        assertContentEquals(photo, accountDatabase.getPhotoForUser(created))
    }

    @Test
    fun `Delete user photo works`() {
        val externalProviderId = UUID.randomUUID().toString()
        val user = accountDatabase.createUser(TestUser.generateTestUser(), externalProviderId)
        val photo: ByteArray = Random.Default.nextBytes(10)

        assertNull(accountDatabase.getPhotoForUser(user))
        accountDatabase.updatePhotoForUser(user, photo)
        assertContentEquals(photo, accountDatabase.getPhotoForUser(user))

        accountDatabase.deletePhotoForUser(user)
        assertNull(accountDatabase.getPhotoForUser(user))
    }

    @Test
    fun `Test create group`() {
        val user1 = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())
        val user2 = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())

        val ownerGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val ownedGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        val memberGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val parentGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        val toCreate =
            TestGroup.generateTestGroup(
                userMembers = setOf(user1.id),
                owners = setOf(user2.id),
                ownerGroups = setOf(ownerGroup.id),
                ownedGroups = setOf(ownedGroup.id),
                groupMembers = setOf(memberGroup.id),
                parentGroups = setOf(parentGroup.id),
            )

        val created = accountDatabase.createGroup(toCreate, UUID.randomUUID().toString())
        val fresh = accountDatabase.getGroup(created.id)

        // Every group a user/group is admin, the entity is also member
        val expectedUser =
            toCreate.copy(
                id = created.id,
                userMembers = setOf(user1.id, user2.id),
                groupMembers = setOf(memberGroup.id, ownerGroup.id),
                parentGroups = setOf(parentGroup.id, ownedGroup.id),
            )

        assert(created.id != 0)
        assertEquals(expectedUser, created)
        assertEquals(created, fresh)
    }

    @Test
    fun `Test update group`() {
        val created =
            accountDatabase.createGroup(
                TestGroup.generateTestGroup(
                    userMembers =
                    setOf(
                        accountDatabase.createUser(
                            TestUser.generateTestUser(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                    owners =
                    setOf(
                        accountDatabase.createUser(
                            TestUser.generateTestUser(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                    ownerGroups =
                    setOf(
                        accountDatabase.createGroup(
                            TestGroup.generateTestGroup(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                    ownedGroups =
                    setOf(
                        accountDatabase.createGroup(
                            TestGroup.generateTestGroup(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                    groupMembers =
                    setOf(
                        accountDatabase.createGroup(
                            TestGroup.generateTestGroup(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                    parentGroups =
                    setOf(
                        accountDatabase.createGroup(
                            TestGroup.generateTestGroup(),
                            UUID.randomUUID().toString(),
                        ).id,
                    ),
                ),
                UUID.randomUUID().toString(),
            )

        val second = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        val name = UUID.randomUUID().toString()
        val description = UUID.randomUUID().toString()

        val user1 = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())
        val user2 = accountDatabase.createUser(TestUser.generateTestUser(), UUID.randomUUID().toString())

        val ownerGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val ownedGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        val memberGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val parentGroup =
            accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        val toUpdate =
            Group(
                id = created.id,
                name = name,
                description = description,
                owners = setOf(user2.id),
                ownerGroups = setOf(ownerGroup.id),
                ownedGroups = setOf(ownedGroup.id),
                userMembers = setOf(user1.id, user2.id),
                groupMembers = setOf(memberGroup.id, ownerGroup.id),
                parentGroups = setOf(parentGroup.id, ownedGroup.id),
            )
        val updated = accountDatabase.updateGroup(toUpdate)
        val fresh = accountDatabase.getGroup(created.id)

        assertEquals(toUpdate, updated)
        assertEquals(updated, fresh)

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.updateGroup(updated.copy(name = second.name))
        }.apply {
            assertEquals(
                "The group cannot be updated because the name is already taken. ${
                    updated.copy(
                        name = second.name,
                    )
                }",
                message,
            )
        }
    }

    @Test
    fun `Get group by name works`() {
        val created = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val read = accountDatabase.getGroupByName(created.name)

        assertEquals(created, read)
        assertNull(accountDatabase.getGroupByName("not_existing"))
    }

    @Test
    fun `Test create group with self loops`() {
        val groupWithSelfMember = TestGroup.generateTestGroup(groupMembers = setOf(0))
        val groupWithSelfOwner = TestGroup.generateTestGroup(ownerGroups = setOf(0))
        val groupWithSelfOwned = TestGroup.generateTestGroup(ownedGroups = setOf(0))
        val groupWithSelfChild = TestGroup.generateTestGroup(parentGroups = setOf(0))

        setOf(groupWithSelfMember, groupWithSelfOwner, groupWithSelfOwned, groupWithSelfChild).forEach { group ->
            assertFailsWith<IllegalArgumentException> {
                accountDatabase.createGroup(group, UUID.randomUUID().toString())
            }.also { assertEquals("No self loops in group memberships allowed", it.message) }
        }
    }

    @Test
    fun `Test create groups with triangle loops`() {
        val group1 = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val group2 =
            accountDatabase.createGroup(
                TestGroup.generateTestGroup(parentGroups = setOf(group1.id)),
                UUID.randomUUID().toString(),
            )
        val group3 =
            TestGroup.generateTestGroup(parentGroups = setOf(group2.id), ownerGroups = setOf(group1.id))

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createGroup(group3, UUID.randomUUID().toString())
        }.also {
            assertEquals(
                "Group memberships are cyclic, which is not allowed. Cycles found: [[${group1.name}, ${group2.name}, ${group3.name}]]",
                it.message,
            )
        }

        val group4 = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        val group5 =
            accountDatabase.createGroup(
                TestGroup.generateTestGroup(ownedGroups = setOf(group4.id)),
                UUID.randomUUID().toString(),
            )
        val group6 =
            TestGroup.generateTestGroup(ownedGroups = setOf(group5.id), groupMembers = setOf(group4.id))

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createGroup(group6, UUID.randomUUID().toString())
        }.also {
            assertEquals(
                "Group memberships are cyclic, which is not allowed. Cycles found: [[${group4.name}, ${group5.name}, ${group6.name}]]",
                it.message,
            )
        }
    }

    @Test
    fun `Create group with set id fails`() {
        val existing = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createGroup(existing, UUID.randomUUID().toString())
        }.apply {
            assertEquals("Could not create group. Id already set. $existing", message)
        }
    }

    @Test
    fun `Create group with same name or externalProviderId fails`() {
        val existingExternalId = UUID.randomUUID().toString()
        val existing = accountDatabase.createGroup(TestGroup.generateTestGroup(), existingExternalId)

        val nameTakenGroup = TestGroup.generateTestGroup(name = existing.name)
        val externalProviderIdTakenGroup = TestGroup.generateTestGroup()

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createGroup(nameTakenGroup, UUID.randomUUID().toString())
        }.apply {
            assertEquals(
                "The group has a name or externalProviderId which is already taken. $nameTakenGroup",
                message,
            )
        }

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.createGroup(externalProviderIdTakenGroup, existingExternalId)
        }.apply {
            assertEquals(
                "The group has a name or externalProviderId which is already taken. $externalProviderIdTakenGroup",
                message,
            )
        }
    }

    @Test
    fun `Get group by externalProviderId works`() {
        val externalId = UUID.randomUUID().toString()
        val created = accountDatabase.createGroup(TestGroup.generateTestGroup(), externalId)
        val read = accountDatabase.getGroupByExternalId(externalId)

        assertEquals(created, read)
        assertNull(accountDatabase.getGroupByExternalId("not_existing"))
    }

    @Test
    fun `Get group externalProviderId works`() {
        val externalProviderId = UUID.randomUUID().toString()
        val created = accountDatabase.createGroup(TestGroup.generateTestGroup(), externalProviderId)

        assertEquals(externalProviderId, accountDatabase.getExternalProviderId(created))
        accountDatabase.deleteGroup(created)
        assertNull(accountDatabase.getExternalProviderId(created))
    }

    @Test
    fun `Delete group works`() {
        val created = accountDatabase.createGroup(TestGroup.generateTestGroup(), UUID.randomUUID().toString())
        accountDatabase.deleteGroup(created)
        assertNull(accountDatabase.getGroup(created.id))
    }

    @Test
    fun `Update group credentialProviderId works`() {
        val credentialProviderIdGroup1 = UUID.randomUUID().toString()
        val credentialProviderIdGroup2 = UUID.randomUUID().toString()
        val updatedCredentialProviderId = UUID.randomUUID().toString()

        val createdUser1 = accountDatabase.createGroup(TestGroup.generateTestGroup(), credentialProviderIdGroup1)
        accountDatabase.createGroup(TestGroup.generateTestGroup(), credentialProviderIdGroup2)

        val updated = accountDatabase.updateExternalProviderId(createdUser1, updatedCredentialProviderId)

        assertNotNull(updated)
        assertEquals(createdUser1, updated, "Update user externalProviderId changed more user fields than expected")
        assertEquals(updatedCredentialProviderId, accountDatabase.getExternalProviderId(updated))

        assertFailsWith<IllegalArgumentException> {
            accountDatabase.updateExternalProviderId(updated, credentialProviderIdGroup2)
        }.apply {
            assertEquals(
                "The group externalProviderId cannot be updated because it is already taken by another group. $credentialProviderIdGroup2. $updated",
                message,
            )
        }
    }

    @Test
    fun `Get{User,Group}ByX() delivers same result as get{User,Group}ByY() or getAll{User,Group}s()`() {
        // Create all entities for this test
        val user1ExternalId = UUID.randomUUID().toString()
        val user2ExternalId = UUID.randomUUID().toString()

        val user1 =
            accountDatabase.createUser(
                TestUser.generateTestUser(disabled = false, permissions = setOf(Permission.ADMIN)),
                user1ExternalId,
            )
        val user2 =
            accountDatabase.createUser(
                TestUser.generateTestUser(disabled = true, permissions = setOf(Permission.INVITE)),
                user2ExternalId,
            )

        val group1ExternalId = UUID.randomUUID().toString()
        val group2ExternalId = UUID.randomUUID().toString()

        val group1 = accountDatabase.createGroup(TestGroup.generateTestGroup(), group1ExternalId)
        val group2 = accountDatabase.createGroup(TestGroup.generateTestGroup(), group2ExternalId)

        // Setup group memberships
        accountDatabase.updateGroup(
            group1.copy(userMembers = setOf(user1.id, user2.id), ownerGroups = setOf(group2.id), groupMembers = setOf(group2.id)),
        )
        accountDatabase.updateGroup(
            group2.copy(userMembers = setOf(user1.id), ownedGroups = setOf(group1.id), parentGroups = setOf(group1.id)),
        )
        // The two lines above influences all users and groups defined
        // So we don't compare with the above definitions of users
        // This is done in specific tests
        val freshUser1 = requireNotNull(accountDatabase.getUserById(user1.id))
        val freshUser2 = requireNotNull(accountDatabase.getUserById(user2.id))

        assertEquals(freshUser1, accountDatabase.getUserByExternalId(user1ExternalId))
        assertEquals(freshUser1, accountDatabase.getUserByMail(user1.mail))
        assertEquals(freshUser1, accountDatabase.getUserByUsername(user1.username))

        assertEquals(freshUser2, accountDatabase.getUserByExternalId(user2ExternalId))
        assertEquals(freshUser2, accountDatabase.getUserByMail(user2.mail))
        assertEquals(freshUser2, accountDatabase.getUserByUsername(user2.username))

        assertEquals(setOf(freshUser1, freshUser2), accountDatabase.getUsers().filter { it.id == user1.id || it.id == user2.id }.toSet())

        val freshGroup1 = requireNotNull(accountDatabase.getGroup(group1.id))
        val freshGroup2 = requireNotNull(accountDatabase.getGroup(group2.id))

        assertEquals(freshGroup1, accountDatabase.getGroupByName(freshGroup1.name))
        assertEquals(freshGroup1, accountDatabase.getGroupByExternalId(group1ExternalId))

        assertEquals(freshGroup2, accountDatabase.getGroupByName(freshGroup2.name))
        assertEquals(freshGroup2, accountDatabase.getGroupByExternalId(group2ExternalId))

        assertEquals(
            setOf(freshGroup1, freshGroup2),
            accountDatabase.getGroups().filter { it.id == group1.id || it.id == group2.id }.toSet(),
        )
    }
}
