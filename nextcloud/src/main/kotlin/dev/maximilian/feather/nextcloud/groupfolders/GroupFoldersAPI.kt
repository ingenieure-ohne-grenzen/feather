/*
 *
 *  *    Copyright [2021] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.groupfolders

import dev.maximilian.feather.nextcloud.Ocs
import dev.maximilian.feather.nextcloud.SuccessResponse
import dev.maximilian.feather.nextcloud.TransformEmptyArrayAsObjectSerializer
import dev.maximilian.feather.nextcloud.groupfolders.entities.GroupFolderDetails
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer

internal object GroupFoldersAnswerEntitySerializer :
    TransformEmptyArrayAsObjectSerializer<Map<String, GroupFolderDetails>>(
        MapSerializer(
            String.serializer(),
            GroupFolderDetails.serializer(),
        ),
    )

@Serializable(with = GroupFoldersAnswerEntitySerializer::class)
internal sealed interface GroupFoldersAnswerEntity : Map<String, GroupFolderDetails>

internal class GroupFoldersAPI(
    private val client: HttpClient,
    baseUrl: String,
) : IGroupFoldersAPI {
    private val basePath = "apps/groupfolders"
    private val groupfoldersPath = "$baseUrl/$basePath/folders"

    override suspend fun getAllGroupFolders(): Map<String, GroupFolderDetails> =
        client.get(groupfoldersPath).body<Ocs<GroupFoldersAnswerEntity>>().ocs.data

    override suspend fun getGroupFolder(id: Int): GroupFolderDetails =
        client.get("$groupfoldersPath/$id").body<Ocs<GroupFolderDetails>>().ocs.data

    override suspend fun createGroupFolder(mountpoint: String): Int =
        client.post(groupfoldersPath) {
            setBody(NewGroupFolderRequest(mountpoint))
        }.body<Ocs<NewGroupFolderId>>().ocs.data.id

    // newQuota is as string encoding the number of bytes or "-3" for no quota
    override suspend fun changeQuota(
        id: Int,
        newQuota: String,
    ): Boolean =
        client.post("$groupfoldersPath/$id/quota") {
            setBody(ChangeQuotaRequest(newQuota))
        }.body<Ocs<SuccessResponse>>().ocs.data.success

    override suspend fun changeMointpoint(
        id: Int,
        newMountpoint: String,
    ): Boolean =
        client.post("$groupfoldersPath/$id/mountpoint") {
            setBody(ChangeMointPointRequest(newMountpoint))
        }.body<Ocs<SuccessResponse>>().ocs.data.success

    override suspend fun changeAclFlag(
        id: Int,
        aclFlag: Boolean,
    ): Boolean =
        client.post("$groupfoldersPath/$id/acl") {
            setBody(ChangeAclFlagRequest(aclFlag))
        }.body<Ocs<SuccessResponse>>().ocs.data.success

    // mappingType is either 'user' or 'group' and mappingId the id of the user or group
    override suspend fun changeAclManager(
        id: Int,
        mappingType: String,
        mappingId: String,
        manageAcl: Boolean,
    ): Boolean =
        client.post("$groupfoldersPath/$id/manageACL") {
            setBody(ChangeAclManagerRequest(mappingType, mappingId, manageAcl))
        }.body<Ocs<SuccessResponse>>().ocs.data.success

    override suspend fun deleteGroupFolder(id: Int) {
        client.delete("$groupfoldersPath/$id").body<Unit>()
    }
}

@Serializable
private data class NewGroupFolderRequest(
    @SerialName("mountpoint")
    val mountPoint: String,
)

@Serializable
private data class NewGroupFolderId(
    val id: Int,
)

@Serializable
private data class ChangeQuotaRequest(
    val quota: String,
)

@Serializable
private data class ChangeMointPointRequest(
    @SerialName("mountpoint")
    val mountPoint: String,
)

@Serializable
private data class ChangeAclFlagRequest(
    val acl: Boolean,
)

@Serializable
private data class ChangeAclManagerRequest(
    val mappingType: String,
    val mappingId: String,
    val manageAcl: Boolean,
)
