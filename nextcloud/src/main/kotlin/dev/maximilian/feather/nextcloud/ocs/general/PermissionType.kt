/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs.general

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlin.math.pow

public enum class PermissionType {
    Read,
    Update,
    Create,
    Delete,
    Share,
    ;

    public companion object {
        public fun toIntValue(permissions: Set<PermissionType>): Int = permissions.sumOf { 2.0.pow(it.ordinal.toDouble()).toInt() }

        public fun fromIntValue(value: Int): Set<PermissionType> {
            val enumValues = enumValues<PermissionType>()

            return enumValues.filterIndexed { index, _ ->
                value and (1 shl index) != 0
            }.toSet()
        }
    }
}

internal object PermissionTypeSetSerializer : KSerializer<Set<PermissionType>> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("permissionType", PrimitiveKind.INT)

    override fun serialize(
        encoder: Encoder,
        value: Set<PermissionType>,
    ) {
        encoder.encodeInt(PermissionType.toIntValue(value))
    }

    override fun deserialize(decoder: Decoder): Set<PermissionType> = PermissionType.fromIntValue(decoder.decodeInt())
}
