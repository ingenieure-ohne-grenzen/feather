/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.nextcloud.Ocs
import dev.maximilian.feather.nextcloud.getOrNull
import dev.maximilian.feather.nextcloud.ocs.entities.CreateShareResponseSubEntity
import dev.maximilian.feather.nextcloud.ocs.entities.general.ShareDetailEntity
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.get
import io.ktor.http.HttpMethod
import io.ktor.http.parameters

internal class ShareAPI(
    private val client: HttpClient,
    baseUrl: String,
) : IShareAPI {
    private val basePath = "ocs/v2.php/apps/files_sharing/api/v1"
    private val sharePath = "$baseUrl/$basePath/shares"

    override suspend fun createShare(
        path: String,
        type: ShareType,
        userOrGroupIDtoShareWith: String,
        publicUpload: Boolean,
        permissions: Set<PermissionType>,
        expireDate: String?,
    ): CreateShareResponseSubEntity =
        client.submitForm(
            url = sharePath,
            formParameters =
            parameters {
                append("path", path)
                append("shareType", "${type.q}")
                append("shareWith", userOrGroupIDtoShareWith)
                append("permissions", "${PermissionType.toIntValue(permissions)}")
                append("publicUpload", "$publicUpload")

                if (expireDate != null) {
                    append("expireDate", expireDate)
                }
            },
        ).body<Ocs<CreateShareResponseSubEntity>>().ocs.data

    override suspend fun updateShare(
        shareID: Int,
        publicUpload: Boolean,
        permissions: Set<PermissionType>,
        expireDate: String?,
    ): ShareDetailEntity =
        client.submitForm(
            url = "$sharePath/$shareID",
            formParameters =
            parameters {
                append("permissions", "${PermissionType.toIntValue(permissions)}")
                append("publicUpload", "$publicUpload")

                if (expireDate != null) {
                    append("expireDate", expireDate)
                }
            },
        ) {
            method = HttpMethod.Put
        }.body<Ocs<ShareDetailEntity>>().ocs.data

    override suspend fun deleteShare(shareID: Int) {
        client.delete("$sharePath/$shareID").body<Unit>()
    }

    override suspend fun getSpecificShareEntity(shareID: Int): ShareDetailEntity? =
        client.getOrNull<Ocs<List<ShareDetailEntity>>>("$sharePath/$shareID")?.ocs?.data?.first()

    override suspend fun getAllShares(): List<ShareDetailEntity> = client.get(sharePath).body<Ocs<List<ShareDetailEntity>>>().ocs.data
}
