package dev.maximilian.feather.nextcloud

import com.github.sardine.SardineFactory
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.WebdavReal
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.plugins.auth.Auth
import io.ktor.client.plugins.auth.providers.BasicAuthCredentials
import io.ktor.client.plugins.auth.providers.basic
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import mu.KLogging

public class NextcloudFactory {
    private companion object : KLogging()

    private val debug = System.getenv("DEBUG") == "true"

    public fun create(
        baseUrl: String,
        username: String,
        password: String,
    ): Nextcloud {
        require(baseUrl.isNotBlank()) { "Nextcloud baseUrl may not be empty" }
        require(username.isNotBlank()) { "Nextcloud user may not be empty" }
        require(password.isNotBlank()) { "Nextcloud password may not be empty" }

        val webdavURL = baseUrl.removeSuffix("/") + "/remote.php/dav/files/$username/"
        val realWebdav = WebdavReal(SardineFactory.begin(username, password))

        logger.info { "Creating Nextcloud WebDav client for user $username (password length: ${password.length})" }
        return Nextcloud(apiClient = createApiHttpClient(username, password), webdavURL, realWebdav, baseUrl)
    }

    private fun HttpClientConfig<*>.configureLogging() =
        Logging {
            logger =
                object : Logger {
                    override fun log(message: String) {
                        NextcloudFactory.logger.info { message }
                    }
                }
            level = if (debug) LogLevel.ALL else LogLevel.INFO
        }

    private fun HttpClientConfig<*>.configureJson() =
        install(ContentNegotiation) {
            json(
                Json {
                    ignoreUnknownKeys = true
                },
            )
        }

    private fun createApiHttpClient(
        authUser: String,
        authPassword: String,
    ): HttpClient =
        HttpClient {
            expectSuccess = true

            configureLogging()
            configureJson()

            defaultRequest {
                contentType(ContentType.Application.Json)
                header("OCS-APIRequest", "true")
            }

            Auth {
                basic {
                    sendWithoutRequest { true }
                    credentials {
                        BasicAuthCredentials(authUser, authPassword)
                    }
                }
            }
        }
}
