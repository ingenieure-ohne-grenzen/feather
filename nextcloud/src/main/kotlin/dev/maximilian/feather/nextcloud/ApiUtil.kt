package dev.maximilian.feather.nextcloud

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ResponseException
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.http.HttpStatusCode

internal suspend inline fun <reified T> HttpClient.getOrNull(
    urlString: String,
    block: HttpRequestBuilder.() -> Unit = {},
): T? =
    try {
        get(urlString, block).body()
    } catch (e: ResponseException) {
        if (e.response.status == HttpStatusCode.NotFound) {
            null
        } else {
            throw e
        }
    }
