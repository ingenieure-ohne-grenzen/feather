/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import dev.maximilian.feather.nextcloud.ocs.NextcloudShare
import dev.maximilian.feather.nextcloud.ocs.NextcloudUser
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import dev.maximilian.feather.testutils.FakeData
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

internal fun getEnv(
    name: String,
    default: String? = null,
): String = System.getenv(name) ?: default ?: throw IllegalArgumentException("Environment variable $name not found")

object TestUtil {
    val baseUrl = getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082")
    val user = getEnv("NEXTCLOUD_USER", "ncadmin")
    val password = getEnv("NEXTCLOUD_PWD", "ncadminsecret")

    const val GROUP_TO_SHARE_WITH: String = "testgroup"

    val dateTimeFormatter: DateTimeFormatter =
        DateTimeFormatter.ISO_LOCAL_DATE
            .withLocale(Locale.US)
            .withZone(ZoneId.of("UTC"))

    internal val nextcloud =
        NextcloudFactory().create(
            baseUrl,
            user,
            password,
        )
}

internal fun generateTestUser(
    displayName: String = "display" + UUID.randomUUID().toString().replace("-", "").substring(4),
    email: String = FakeData.generateMailAddress(),
    userid: String = UUID.randomUUID().toString().replace("-", "").substring(4).lowercase(Locale.getDefault()),
    password: String = UUID.randomUUID().toString().replace("-", ""),
    groups: List<String> = emptyList(),
    subadmin: List<String> = emptyList(),
    quota: Long = 0,
    language: String = "",
) = NextcloudUser(
    displayName,
    email,
    userid,
    password,
    groups,
    subadmin,
    quota,
    language,
)

internal fun generateTestPath(parentPath: String = "/"): Pair<String, String> {
    val subPath = UUID.randomUUID().toString()
    val fullPath =
        if (parentPath.endsWith("/")) {
            parentPath + subPath
        } else {
            "$parentPath/$subPath"
        }
    return subPath to fullPath
}

internal fun generateTestShare(
    path: String = "/" + UUID.randomUUID().toString(),
    type: ShareType = ShareType.Group,
    userOrGroupIDtoShareWith: String = TestUtil.GROUP_TO_SHARE_WITH,
    publicUpload: Boolean = false,
    permissions: Set<PermissionType> = PermissionType.values().toSet(),
    expireDate: Instant = Instant.now().plus(3, ChronoUnit.DAYS),
) = NextcloudShare(
    path,
    type,
    userOrGroupIDtoShareWith,
    publicUpload,
    permissions,
    TestUtil.dateTimeFormatter.format(expireDate),
)
