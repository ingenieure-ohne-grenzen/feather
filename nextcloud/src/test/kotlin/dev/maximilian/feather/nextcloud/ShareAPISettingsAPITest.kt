/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import dev.maximilian.feather.nextcloud.ocs.ShareAPI
import dev.maximilian.feather.nextcloud.ocs.ShareAPISettingsAPI
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import kotlinx.coroutines.runBlocking
import java.lang.Exception
import kotlin.test.Test
import kotlin.test.assertFailsWith

class ShareAPISettingsAPITest {
    companion object {
        private val shareAPI = ShareAPI(TestUtil.nextcloud.apiClient, TestUtil.baseUrl)
        private val shareSettingsAPI =
            ShareAPISettingsAPI(TestUtil.nextcloud.apiClient, TestUtil.baseUrl)
    }

    @Test
    fun `Test get and set enforcement of expiration date`() {
        runBlocking {
            val t1 = shareSettingsAPI.setEnforceExpireDate(false)
            check(t1)

            check(shareSettingsAPI.setDefaultExpireDate(true))

            var shares = shareAPI.getAllShares()
            shares.forEach {
                shareAPI.deleteShare(it.id)
            }
            shares = shareAPI.getAllShares()
            check(shares.isEmpty())

            // test with public URL
            val shareUrlID =
                shareAPI.createShare(
                    "/Documents",
                    ShareType.PublicLink,
                    "",
                    false,
                    setOf(PermissionType.Create),
                ).id

            checkNotNull(shareUrlID > 0)
            // remove expiration date by explicitely setting it to ""
            shareAPI.updateShare(shareUrlID, true, setOf(PermissionType.Create), "")
            val shareDetails = shareAPI.getSpecificShareEntity(shareUrlID)
            requireNotNull(shareDetails)
            check(shareDetails.expiration == null)

            val t2 = shareSettingsAPI.getEnforceExpireDate()
            check(!t2)

            val t3 = shareSettingsAPI.setEnforceExpireDate(true)
            check(t3)

            // test with public URL
            val shareUrlID2 =
                shareAPI.createShare(
                    "/Documents",
                    ShareType.PublicLink,
                    "",
                    false,
                    setOf(PermissionType.Create),
                ).id
            // try to remove expiration date by explicitely setting it to "" => has to fail!
            checkNotNull(shareUrlID2 > 0)
            assertFailsWith<Exception> {
                shareAPI.updateShare(shareUrlID2, true, setOf(PermissionType.Create), "")
            }
            val shareDetails2 = shareAPI.getSpecificShareEntity(shareUrlID2)
            requireNotNull(shareDetails2)
            checkNotNull(shareDetails2.expiration)

            val t4 = shareSettingsAPI.getEnforceExpireDate()
            check(t4)
        }
    }

    @Test
    fun `Test get and set internal enforcement of expiration date`() {
        runBlocking {
            val t1 = shareSettingsAPI.setEnforceInternalExpireDate(false)
            check(t1)

            val t2 = shareSettingsAPI.getEnforceInternalExpireDate()
            check(!t2)

            val t3 = shareSettingsAPI.setEnforceInternalExpireDate(true)
            check(t3)

            val t4 = shareSettingsAPI.getEnforceInternalExpireDate()
            check(t4)
        }
    }
}
